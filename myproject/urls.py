"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from myproject.views import index,dashboard,building,milestonedash,userlogout,masterMenu

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$',index, name= 'index'),
    url(r'^userlogout/',userlogout, name= 'userlogout'),
    url(r'^dashboard/',dashboard, name= 'dashboard'),
    url(r'^masterMenu/',masterMenu, name= 'masterMenu'),
	# url(r'^building/',building, name= 'building'),
	url(r'^milestone/',milestonedash, name= 'milestonedash'),
	url(r'^userlogout/',userlogout, name= 'userlogout'),
    url(r'masters/',include(("master.urls","masters"), namespace='masters')),
    url(r'project/',include(("projects.urls","project"), namespace='project')),
    # url(r'api/',include(("mobileApi.urls","mobileApi"), namespace='mobileApi')),
    url(r'userManagement/',include(("userManagements.urls","userManagement"), namespace='userManagement')),
]
