# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
import json
from django.contrib.auth import logout,authenticate, login
from django.contrib.auth import models
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
import string
# from apollo.settings import DEBUG
# from rest_framework_jwt.utils import jwt_payload_handler
# from rest_framework_jwt.utils import jwt_response_payload_handler
# from rest_framework_jwt.utils import jwt_encode_handler
import random
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core import serializers
from django.apps import apps
import re
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login
from rest_framework.generics import CreateAPIView,ListAPIView,RetrieveAPIView
from base64 import b64decode
from django.core.files.base import ContentFile
from rest_framework.settings import api_settings
from django.contrib.auth import authenticate
from django.conf import settings
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import django
import datetime
import ast
from datetime import datetime,timedelta
from django.contrib import messages
import os
from django.utils.crypto import get_random_string
from email.header  import Header
from getpass  import getpass
import smtplib
import copy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.db.models import Q
from django.db.models import Sum
import calendar
from master.models import ProjectStatus,ProjectType,country,State,City
from projects.models import Project,ProjectMembers,ProjectMilestoneTaskLogs,ProjectMilestoneTask
from django.contrib.auth.models import User,Group
from userManagements.models import UserMapping
@csrf_exempt
def index(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		checkin = authenticate(request, username=username, password=password)
		if checkin is not None:
			user = User.objects.get(username__iexact=username)
			login(request, user, backend='django.contrib.auth.backends.ModelBackend')
			um = UserMapping.objects.filter(user_id = user.id,isActive=True)[0]
			if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
				request.session["isMaster"] = "yes"
			else:
				request.session["isMaster"] = "no"
			
			# return render(request, 'admin_templates/dashboard.html')
			return redirect('dashboard')
		else:
			messages.error(request, 'Invalid credentials')
			return render(request,'admin_templates/wfm-new-login.html')
	elif request.user.is_authenticated:
		
		return redirect('dashboard')
	else:
		return render(request,'admin_templates/wfm-new-login.html')
	# return HttpResponse('success')
		
@api_view(['GET'])
def userlogout(request):
	try:
		logout(request)
		return redirect("index")
	except Exception as e:
		return HttpResponse(json.dumps({"status": "success","responsecode":"500","message":str(e)}), content_type="application/json")
	
@csrf_exempt
@login_required
def dashboard(request):
	ps = ProjectStatus.objects.filter(isActive=True)
	pmt = ProjectMilestoneTask.objects.all()
	pt = ProjectType.objects.filter(isActive=True)
	users = User.objects.filter(is_active=True)
	projects = Project.objects.filter(isActive=True).order_by('-id')
	pmtl = ProjectMilestoneTaskLogs.objects.all().order_by('-timestamp')[:10]
	user = request.user
	um = UserMapping.objects.filter(user_id = user.id,isActive=True)[0]
	if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
		flag = True
	else:
		flag = False
	data = {"pmt":pmt,"projects":projects}
	# data = {"flag":flag,"ps":ps,"pt":pt,"users":users,"projects":projects,"pmtl":pmtl}
	return render(request,'admin_templates/dashboard-wfm1.html',{"data":data})
	
def building(request):
	return render(request,'admin_templates/building.html')

def milestonedash(request):
	return render(request,'admin_templates/milestone/milestone-dash.html')
	
def masterMenu(request):
	return render(request,'admin_templates/masters/masterDash.html')