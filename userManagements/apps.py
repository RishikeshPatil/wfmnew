from django.apps import AppConfig


class UsermanagementsConfig(AppConfig):
    name = 'userManagements'
