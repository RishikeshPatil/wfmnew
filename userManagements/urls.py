from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings

from userManagements.views import (
	userManagement,
	userManagementApi,
	userPermission,
	userPermissionAPI,
	forgotPassword,
	changePassword,
)

urlpatterns = [
url(r'^userManagement/$',userManagement, name= 'userManagement'),
url(r'^userManagementApi/$',userManagementApi, name= 'userManagementApi'),
url(r'^userPermission/$',userPermission, name= 'userPermission'),
  url(r'^userPermissionAPI/$',userPermissionAPI, name= 'userPermissionAPI'),
  url(r'^forgotPassword/$',forgotPassword, name= 'forgotPassword'),
  url(r'^changePassword/$',changePassword, name= 'changePassword'),
]
