from django.contrib import admin
from userManagements.models import UserMapping,Modules,UserPermission
# Register your models here.
admin.site.register(UserMapping)
admin.site.register(Modules)
admin.site.register(UserPermission)