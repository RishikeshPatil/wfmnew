from django.db import models
from master.models import role,TypeOfTask
from django.contrib.auth.models import User,Group
# from project.models import BusinessUnit

class UserMapping(models.Model):
	user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True)
	role = models.ForeignKey(role,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	# department = models.ForeignKey(department,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	# bunit = models.ForeignKey(BusinessUnit,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.user.username
		
class ModulesUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getModulesUUID():
	try:
		uuid = ModulesUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = ModulesUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getmodId = 'MID'+ str(uuid.uuidNumber)
	return getmodId
	
class Modules(models.Model):
	uId = models.TextField(default=getModulesUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True, blank=True)
	def __str__(self):
		return self.name
		
#--------------Permissions----------------------------------------------------#
class UserPermission(models.Model):
	role = models.ForeignKey(role,to_field="uId",on_delete=  models.SET_NULL, null=True, blank=True)
	modules = models.ForeignKey(TypeOfTask,to_field="uId",on_delete=  models.SET_NULL,null=True, blank=True)
	# view = models.BooleanField(default=False)
	create = models.BooleanField(default=False)
	edit = models.BooleanField(default=False)
	delete = models.BooleanField(default=False)
	isActive = models.BooleanField(default=True)
	def __unicode__(self):
		return self.role.name