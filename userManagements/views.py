# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
import json
from django.contrib.auth import logout,authenticate, login
from django.contrib.auth import models
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
import string
# from apollo.settings import DEBUG
import random
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core import serializers
from django.apps import apps
import re
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login
from rest_framework.generics import CreateAPIView,ListAPIView,RetrieveAPIView
from base64 import b64decode
from django.core.files.base import ContentFile
from rest_framework.settings import api_settings
from django.contrib.auth import authenticate
from django.conf import settings
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import django
import datetime
import ast
from datetime import datetime,timedelta
from django.contrib import messages
import os
from django.utils.crypto import get_random_string
from email.header  import Header
from getpass  import getpass
import smtplib
import copy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.db.models import Q
from django.db.models import Sum
import calendar
from master.models import role
from userManagements.models import UserMapping,Modules,UserPermission
from email.header  import Header
from getpass  import getpass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
import smtplib
import asyncio
loop = asyncio.get_event_loop()
import string 
import random
from django.contrib.auth import authenticate, login
from master.views import sendMailFunctionPassword
from master.models import TypeOfTask

def sendRegistrationEmail(name,emailId,password):
	receiver_email = [emailId]
	smtpHost = settings.SMTP_HOST
	smtpPort = settings.SMTP_PORT
	subject= 'Project Management Tool Credentials' 
	msg = MIMEMultipart('alternative')
	msg['From'] = settings.SENDER_EMAIL
	msg['To'] =  ','.join(receiver_email)
	msg['Subject'] = subject
	html = 'Hi,<br><br>'+settings.REGISTRATION_MESSAGE+"<br><br>Username: "+emailId+"<br><br>Password: "+password
	part = MIMEText(html, 'html')
	msg.attach(part)
	smtpconf = smtpHost+":"+smtpPort
	mailServer = smtplib.SMTP(smtpconf)
	mailServer.starttls()
	mailServer.login(settings.SENDER_EMAIL,settings.SENDER_PASSWORD)
	mailServer.sendmail(settings.SENDER_EMAIL,receiver_email, msg.as_string())
	mailServer.close()
	
#-------------------load business unit page----------------------------
@csrf_exempt
def userManagement(request):
	dept=""
	# dept = department.objects.filter(isActive = True)
	roles = role.objects.filter(isActive = True)
	data = {"dept":dept,"roles":roles}
	return render(request,'admin_templates/userManagement/userManagement.html',{"data":data})
	
#---------------Business unit Api---------
@api_view(['POST'])
def userManagementApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = User.objects.get(id=data["uId"])
				detailsmapping = UserMapping.objects.get(user_id=data["uId"])
				oldUsername = details.username
				if(oldUsername==data["email"]):
					pass
				else:
					cnt = User.objects.filter(username=data["email"]).count()
					if(cnt>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Username already exist"}),content_type="application/json")
				# if(data["password"]!="" and data["password"]!="Null" and data["password"]!="null" and data["password"]!=None):
					# details.password = data["password"]
					# details.set_password(details.password)
			except:
				cnt = User.objects.filter(username=data["email"]).count()
				if(cnt>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Username already exist"}),content_type="application/json")
				details = User()
				detailsmapping = UserMapping()
				details.password = data["password"]
				details.set_password(details.password)
			details.first_name = data["first_name"]
			details.last_name = data["last_name"]
			details.email = data["email"]
			details.username = data["email"]
			details.save()
			detailsmapping.role_id = data["role"]
			# detailsmapping.department_id = data["department"]
			detailsmapping.user_id = details.id
			detailsmapping.save()
			if(data["action"]=="create"):
				name = data["first_name"]+" "+data["last_name"]
				loop.run_in_executor(None,sendRegistrationEmail,name,data["email"],data["password"])
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","id":details.id,"first_name":details.first_name,"last_name":details.last_name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = User.objects.get(id=uId)
			um = UserMapping.objects.get(user_id=uId)
			obj = {"uId":p.id,"first_name":p.first_name,"last_name":p.last_name,"email":p.email,"role":um.role_id,"department":um.department_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = User.objects.filter(is_active=True).count()
			ListDetails = User.objects.filter(is_active=True).order_by('-id')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(username__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				detailsmapping = UserMapping.objects.filter(user_id=item.id)[0]
				try:
					role = detailsmapping.role.name
				except:
					role = ""
				try:
					department = detailsmapping.department.name
				except:
					department = ""
				
				list.append({"uId":item.id,"first_name":item.first_name,"last_name":item.last_name,"role":role,"department":department,"email":item.email})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = User.objects.get(id=uId)
			um = UserMapping.objects.get(user_id=uId)
			p.is_active = False
			p.save()
			um.isActive = False
			um.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
@api_view(['POST'])
def forgotPassword(request):
	try:
		data = request.data
		ucount = User.objects.filter(username = data["email"]).count()
		if(ucount == 0):
			return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Email id not found"}),content_type="application/json")
		else:
			details = User.objects.get(username=data["email"])
			res = ''.join(random.choices(string.ascii_uppercase +string.digits, k = 10))
			password = str(res)
			details.password = password
			details.set_password(details.password)
			details.save()
			name = details.first_name+" "+details.last_name
			loop.run_in_executor(None,sendMailFunctionPassword,"Forgot Password",details.id,password)
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","name":name,"password":password}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
@api_view(['POST'])
def changePassword(request):
	try:
		data = request.data

		# user = request.user.id
		user = data["user"]

		print(data["user"])
		details = User.objects.get(id = user)
		print(details)
		password = data["password"]
		details.password = password
		details.set_password(details.password)
		details.save()
		login(request, details, backend='django.contrib.auth.backends.ModelBackend')
		# name = details.first_name+" "+details.last_name
		# loop.run_in_executor(None,sendForgotPasswordEmail,name,data["email"],password)
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
def sendForgotPasswordEmail(name,emailId,password):
	receiver_email = [emailId]
	smtpHost = settings.SMTP_HOST
	smtpPort = settings.SMTP_PORT
	subject= 'Project Management Tool Credentials' 
	msg = MIMEMultipart('alternative')
	msg['From'] = settings.SENDER_EMAIL
	msg['To'] =  ','.join(receiver_email)
	msg['Subject'] = subject
	html = 'Hi,<br><br>'+"Your new Password is: "+password
	part = MIMEText(html, 'html')
	msg.attach(part)
	smtpconf = smtpHost+":"+smtpPort
	mailServer = smtplib.SMTP(smtpconf)
	mailServer.starttls()
	mailServer.login(settings.SENDER_EMAIL,settings.SENDER_PASSWORD)
	mailServer.sendmail(settings.SENDER_EMAIL,receiver_email, msg.as_string())
	mailServer.close()
	
#-------------------load user permission----------------------------
@csrf_exempt
def userPermission(request):
	roles = role.objects.filter(isActive = True)
	data = {"roles":roles}
	return render(request,'admin_templates/userManagement/userPermission.html',{"data":data})

@api_view(['POST'])
@csrf_exempt
# @login_required(login_url='index')
def userPermissionAPI(request):
	try:
		data = request.data
		if(data["action"]=="getPermissions"):

			AllTransaction = TypeOfTask.objects.filter(isActive=True)
			role = data["role"]
			userPermissionList=[]
			
			for transaction in AllTransaction:
				try:
					UserPermissionData = UserPermission.objects.get(isActive=True,role_id=role,modules_id=transaction.uId)
					print(UserPermissionData)
				except:
					UserPermissionData = UserPermission()
					UserPermissionData.role_id = role
					UserPermissionData.modules_id = transaction.uId
					# UserPermissionData.view = False
					UserPermissionData.create = False
					UserPermissionData.edit = False
					UserPermissionData.delete = False
					UserPermissionData.save()
				obj = {"transactionId":UserPermissionData.modules_id,"transactionName":UserPermissionData.modules.name,"create":UserPermissionData.create,"edit":UserPermissionData.edit,"delete":UserPermissionData.delete}
				userPermissionList.append(obj)
			return HttpResponse(json.dumps({"Status" : "Success", "response_code":"200", "userPermissionList":userPermissionList, "roleId":role}), content_type="application/json")

		elif(request.POST["action"]=="savePermissions"):
			role = data["role"]
			permissionItems = json.loads(request.POST["permissionItems"])
			userPermissionList=[]
			for item in permissionItems:
				try:
					userPermission = UserPermission.objects.get(isActive=True,role_id = role,modules_id=item["transactionId"])
				except:
					userPermission = UserPermission()
					userPermission.role_id = role
					userPermission.modules_id=item["transactionId"]
				# if (item["view"]==True):
					# userPermission.view = True
				# else:
					# userPermission.view = False
				if (item["create"]==True):
					userPermission.create = True
				else:
					userPermission.create = False
				if (item["edit"]==True):
					userPermission.edit = True
				else:
					userPermission.edit = False
				if (item["delete"]==True):
					userPermission.delete = True
				else:
					userPermission.delete = False
				userPermission.save()
				obj = {"transactionId":userPermission.modules_id,"transactionName":userPermission.modules.name,"create":userPermission.create,"edit":userPermission.edit,"delete":userPermission.delete}
				userPermissionList.append(obj)
			return HttpResponse(json.dumps({"Status" : "Success", "response_code":"200", "roleId":role, "userPermissionList":userPermissionList}), content_type="application/json")
	except Exception as e:
		# print(e)
		return HttpResponse(json.dumps({"response_code":"600","Status": "Fail","error":str(e)}), content_type="application/json")