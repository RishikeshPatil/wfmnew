from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings
from projects.views import(
	ProjectApi,
	addProject,
	editProject,
	milestoneDetails,
	milestoneProjectApi,
	projectMilestoneTaskApi,
	taskDetails,
	addTask,
	editTask,
	project,
	projectDetail
	
)
urlpatterns = [
	url(r'^projectDetail/(?P<uId>[\w{}.-]{1,100})/$',projectDetail, name= 'projectDetail'),
	url(r'^$',project, name= 'project'),
	url(r'^addProject/',addProject, name= 'addProject'),
	url(r'^editProject/(?P<uId>.*)/$',editProject, name= 'editProject'),
	url(r'^ProjectApi/$',ProjectApi, name= 'ProjectApi'),
	url(r'^milestoneDetails/(?P<uId>.*)/$',milestoneDetails, name= 'milestoneDetails'),
	url(r'^taskDetails/(?P<uId>.*)/$',taskDetails, name= 'taskDetails'),
	url(r'^addTask/(?P<uId>.*)/$',addTask, name= 'addTask'),
	url(r'^editTask/(?P<uId>.*)/$',editTask, name= 'editTask'),
	url(r'^milestoneProjectApi/',milestoneProjectApi, name= 'milestoneProjectApi'),
	url(r'^projectMilestoneTaskApi/',projectMilestoneTaskApi, name= 'projectMilestoneTaskApi'),
	# url(r'^ProjectBudgetApi/',ProjectBudgetApi, name= 'ProjectBudgetApi'),
	]