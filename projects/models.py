from django.db import models
# from masters.models import Employee,country,State,City,ProjectStatus,ProjectType,role,building,subbuilding,UOM,department,UtilitiesSystemCategory,UtilitiesSystem,ElectricalSystemCategory,ElectricalSystem,StatutoryCategory,Equipment,EquipmentSubCategory,EquipmentCategory,TypeOfWork,SeverityMaster,FieldTypes,TaskCategory
from master.models import Employee,country,State,City,ProjectStatus,ProjectType,role,UOM,TaskCategory,TeamFunction,ProjectManagementMode,Privacy,Priority,TaskStatus
from django.contrib.auth.models import User,Group
from django.conf import settings
import os.path
import datetime
		
#-------------------Project-------------------------
class projectUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getProjectUUID():
	try:
		uuid = projectUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = projectUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'ProId-'+ str(uuid.uuidNumber)
	return getUUIDId

class Project(models.Model):
	uId = models.CharField(default=getProjectUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	key = models.TextField(null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	projectType = models.ForeignKey(ProjectType,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	projectStatus = models.ForeignKey(ProjectStatus,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	actualstartDate = models.DateField(null=True, blank=True)
	planstartDate = models.DateField(null=True, blank=True)
	actualendDate = models.DateField(null=True, blank=True)
	planendDate = models.DateField(null=True, blank=True)
	completiondate = models.DateField(null=True, blank=True)
	duration = models.IntegerField(default=0,null=True, blank=True)
	privacy = models.ForeignKey(Privacy,on_delete=models.SET_NULL,null=True, blank=True)
	priority = models.ForeignKey(Priority,on_delete=models.SET_NULL,null=True, blank=True)
	projectManager = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True,related_name="projectManager")
	projectCoordinator = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True,related_name="projectCoordinator")
	projectHead = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True,related_name="projectHead")
	isActive = models.BooleanField(default=True,null=True,blank=True)
	teamfunction = models.ForeignKey(TeamFunction,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	projectmanagementmode = models.ForeignKey(ProjectManagementMode,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	inviteyourteam = models.ForeignKey(Employee,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	
	def __str__(self):
		return self.name
		
class ProjectMembers(models.Model):
	user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True)
	project = models.ForeignKey(Project,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
		
		
#-------------------Milestone Master-------------------------
class milestoneMasterUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getMilestoneMasterUUID():
	try:
		uuid = milestoneMasterUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = milestoneMasterUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'MSId-'+ str(uuid.uuidNumber)
	return getUUIDId

class MilestoneMaster(models.Model):
	uId = models.CharField(default=getMilestoneMasterUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isDefault = models.BooleanField(default=True,null=True,blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	seq = models.IntegerField(null=True,blank=True)
	def __str__(self):
		return self.name

class Milestonestatus(models.Model):
	uId = models.IntegerField()
	name =models.TextField(null=True, blank=True)
	def __str__(self):
		return self.name
		
		
#-------------------Task Master-------------------------
class taskMasterUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getTaskMasterUUID():
	try:
		uuid = taskMasterUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = taskMasterUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'TSId-'+ str(uuid.uuidNumber)
	return getUUIDId

class TaskMaster(models.Model):
	uId = models.CharField(default=getTaskMasterUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
		
#-------------------Milestone and Task Mapping Master-------------------------
class taskMilestoneMasterUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getMilestoneTaskMasterUUID():
	try:
		uuid = taskMilestoneMasterUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = taskMilestoneMasterUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'MTId-'+ str(uuid.uuidNumber)
	return getUUIDId

class MilestoneTaskMapping(models.Model):
	uId = models.CharField(default=getMilestoneTaskMasterUUID, max_length=100, unique=True)
	milestone = models.ForeignKey(MilestoneMaster,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	task = models.ForeignKey(TaskMaster,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __unicode__(self):
		return self.milestone
		
#-------------------Project Milestone-------------------------
class projectMilestoneUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getProjectMilestoneUUID():
	try:
		uuid = projectMilestoneUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = projectMilestoneUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PMId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectMilestone(models.Model):
	uId = models.CharField(default=getProjectMilestoneUUID, max_length=100, unique=True)
	project = models.ForeignKey(Project,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	milestone = models.ForeignKey(MilestoneMaster,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	milestoneName = models.TextField(null=True, blank=True)
	milestonestatus = models.ForeignKey(Milestonestatus,on_delete=models.SET_NULL,null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	actualstartDate = models.DateField(null=True, blank=True)
	planstartDate = models.DateField(null=True, blank=True)
	actualendDate = models.DateField(null=True, blank=True)
	planendDate = models.DateField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	completionPercentage = models.FloatField(default=0,null=True, blank=True)
	seq = models.IntegerField(null=True,blank=True)
	def __str__(self):
		return self.milestoneName
				

def getUploadFilepath(self, filename):
	extlist = filename.rsplit(".")
	ext = extlist[len(extlist)-1]
	# currentdatetime = (datetime.now())
	filename = extlist[0]+"."+ext
	try:
		directory =settings.MEDIA_ROOT+"/"+self.pmt.uId
	except:
		directory =settings.MEDIA_ROOT+"/"+self.projectId.uId
	if not os.path.exists(directory):
		os.makedirs(directory)
	full_path = str(directory)+"/%s" %(filename)
	self.attachmentname = filename
	return full_path	

	
#-------------------Project Milestone and Task-------------------------
class projectMilestoneTaskUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getprojectMilestoneTaskUUID():
	try:
		uuid = projectMilestoneTaskUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = projectMilestoneTaskUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PMTId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectMilestoneTask(models.Model):
	uId = models.CharField(default=getprojectMilestoneTaskUUID, max_length=100, unique=True)
	project = models.ForeignKey(Project,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	milestone = models.ForeignKey(ProjectMilestone,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	milestonename = models.TextField(null=True, blank=True)
	task = models.ForeignKey(TaskMaster,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	taskstatus = models.ForeignKey(TaskStatus,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	taskname = models.TextField(null=True, blank=True)
	plannedStartDate = models.DateField(null=True, blank=True)
	plannedEndDate = models.DateField(null=True, blank=True)
	actualStartDate = models.DateField(null=True, blank=True)
	poDate = models.DateField(null=True, blank=True)
	actualEndDate = models.DateField(null=True, blank=True)
	completionPercentage = models.FloatField(default=0,null=True, blank=True)
	remark = models.TextField(null=True, blank=True)
	uom = models.ForeignKey(UOM,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	postedDate = models.DateField(null=True, blank=True)
	closedDate = models.DateField(null=True, blank=True)
	status = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	plannedDuration = models.IntegerField(default=0,null=True,blank=True)
	actualDuration = models.IntegerField(default=0,null=True,blank=True)
	taskCategory = models.ForeignKey(TaskCategory,to_field = "uId",on_delete = models.SET_NULL,null=True,blank=True)
	taskCloseMailStatus = models.BooleanField(default=False,null=True,blank=True)
	isDefault = models.BooleanField(default = False,null=True,blank=True)
	def __str__(self):
		return self.taskname
		
class PMTAttachments(models.Model):
	attachment = models.FileField(upload_to=getUploadFilepath,null=True, blank=True)
	attachmentname = models.TextField(null=True, blank=True)
	remark = models.TextField(null=True,blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	pushToGallary = models.BooleanField(default=False,null=True,blank=True)
	date = models.DateField(null=True, blank=True)
	pmt = models.ForeignKey(ProjectMilestoneTask,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	projectId = models.ForeignKey(Project,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)
	# pmId = models.ForeignKey(ProjectMilestone,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)

class ProjectMilestoneTaskLogs(models.Model):
	user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True, blank=True)
	timestamp = models.DateTimeField(auto_now=True,null=True,blank=True)
	pmt = models.ForeignKey(ProjectMilestoneTask,to_field='uId',on_delete=models.SET_NULL,null=True, blank=True)

Color_CHOICES =( 
    ("red", "red"), 
    ("amber", "amber"), 
    ("yellow", "yellow"), 
    ("green", "green")
)
		
class MilestoneTaskCategoryMapping(models.Model):
	milestone = models.ForeignKey(ProjectMilestone,to_field = "uId",on_delete = models.SET_NULL,null=True,blank=True)
	category = models.ForeignKey(TaskCategory,to_field = "uId",on_delete = models.SET_NULL,null=True,blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)

class projectTeamUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getprojectTeamUUID():
	try:
		uuid = projectTeamUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = projectTeamUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PTId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectTeam(models.Model):
	uId = models.CharField(default=getprojectTeamUUID, max_length=100, unique=True)
	project = models.ForeignKey(Project,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	empId = models.ForeignKey(Employee,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	projectstatus = models.ForeignKey(ProjectStatus,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	projectJoinedDate = models.DateField(null=True, blank=True)
	projectManager = models.BooleanField(default=True,null=True,blank=True)
	read = models.BooleanField(default=True,null=True,blank=True)
	write = models.BooleanField(default=True,null=True,blank=True)
	edit = models.BooleanField(default=True,null=True,blank=True)
	delete = models.BooleanField(default=True,null=True,blank=True)

	def __str__(self):
		return self.project.name

class TimeSheet(models.Model):
	uId = models.CharField(max_length=100, unique=True)
	project = models.ForeignKey(Project,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	empId = models.ForeignKey(Employee,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	taskId = models.ForeignKey(TaskMaster,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	taskname = models.CharField(max_length=100, unique=True)
	timesheetDate = models.DateField(null=True, blank=True)
	# currentdatetime = datetime.now()
	hourspent = models.CharField(max_length=100)

	def __str__(self):
		return self.taskname

class TaskUpdates(models.Model):
	uId = models.CharField(max_length=100, unique=True)
	taskId = models.ForeignKey(TaskMaster,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	taskname = models.CharField(max_length=100, unique=True)
	updateDate = models.DateField(null=True, blank=True)
	description = models.CharField(max_length=100, unique=True)

	def __str__(self):
		return self.taskname

class TimesheetFileAttachment(models.Model):
	uId = models.CharField(max_length=100, unique=True)
	TimeSheetId = models.ForeignKey(TimeSheet,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	empId = models.ForeignKey(Employee,to_field="uId",on_delete=models.SET_NULL,null=True, blank=True)
	filename = models.CharField(max_length=100, unique=True)
	fileurl = models.URLField(max_length=200)
	description = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.filename