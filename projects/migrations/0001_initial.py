# Generated by Django 2.2.11 on 2020-04-10 10:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import projects.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MilestoneMaster',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getMilestoneMasterUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isDefault', models.BooleanField(blank=True, default=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('seq', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='milestoneMasterUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getProjectUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('key', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('actualstartDate', models.DateField(blank=True, null=True)),
                ('planstartDate', models.DateField(blank=True, null=True)),
                ('actualendDate', models.DateField(blank=True, null=True)),
                ('planendDate', models.DateField(blank=True, null=True)),
                ('duration', models.IntegerField(blank=True, default=0, null=True)),
                ('privacy', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('projectCoordinator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='projectCoordinator', to=settings.AUTH_USER_MODEL)),
                ('projectHead', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='projectHead', to=settings.AUTH_USER_MODEL)),
                ('projectManager', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='projectManager', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectMilestone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getProjectMilestoneUUID, max_length=100, unique=True)),
                ('milestoneName', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('actualstartDate', models.DateField(blank=True, null=True)),
                ('planstartDate', models.DateField(blank=True, null=True)),
                ('actualendDate', models.DateField(blank=True, null=True)),
                ('planendDate', models.DateField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('completionPercentage', models.FloatField(blank=True, default=0, null=True)),
                ('seq', models.IntegerField(blank=True, null=True)),
                ('milestone', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.MilestoneMaster', to_field='uId')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectMilestoneTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getprojectMilestoneTaskUUID, max_length=100, unique=True)),
                ('milestonename', models.TextField(blank=True, null=True)),
                ('taskname', models.TextField(blank=True, null=True)),
                ('plannedStartDate', models.DateField(blank=True, null=True)),
                ('plannedEndDate', models.DateField(blank=True, null=True)),
                ('actualStartDate', models.DateField(blank=True, null=True)),
                ('poDate', models.DateField(blank=True, null=True)),
                ('actualEndDate', models.DateField(blank=True, null=True)),
                ('completionPercentage', models.FloatField(blank=True, default=0, null=True)),
                ('remark', models.TextField(blank=True, null=True)),
                ('postedDate', models.DateField(blank=True, null=True)),
                ('closedDate', models.DateField(blank=True, null=True)),
                ('status', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('plannedDuration', models.IntegerField(blank=True, default=0, null=True)),
                ('actualDuration', models.IntegerField(blank=True, default=0, null=True)),
                ('taskCloseMailStatus', models.BooleanField(blank=True, default=False, null=True)),
                ('isDefault', models.BooleanField(blank=True, default=False, null=True)),
                ('milestone', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectMilestone', to_field='uId')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='projectMilestoneTaskUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='projectMilestoneUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='projectTeamUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='projectUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TaskMaster',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getTaskMasterUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='taskMasterUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='taskMilestoneMasterUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TimeSheet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(max_length=100, unique=True)),
                ('taskname', models.CharField(max_length=100, unique=True)),
                ('timesheetDate', models.DateField(blank=True, null=True)),
                ('hourspent', models.CharField(max_length=100)),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
                ('taskId', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.TaskMaster', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='TimesheetFileAttachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(max_length=100, unique=True)),
                ('filename', models.CharField(max_length=100, unique=True)),
                ('fileurl', models.URLField()),
                ('description', models.TextField(blank=True, null=True)),
                ('TimeSheetId', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.TimeSheet', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='TaskUpdates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(max_length=100, unique=True)),
                ('taskname', models.CharField(max_length=100, unique=True)),
                ('updateDate', models.DateField(blank=True, null=True)),
                ('description', models.CharField(max_length=100, unique=True)),
                ('taskId', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.TaskMaster', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectTeam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getprojectTeamUUID, max_length=100, unique=True)),
                ('projectJoinedDate', models.DateField(blank=True, null=True)),
                ('projectManager', models.BooleanField(blank=True, default=True, null=True)),
                ('read', models.BooleanField(blank=True, default=True, null=True)),
                ('write', models.BooleanField(blank=True, default=True, null=True)),
                ('edit', models.BooleanField(blank=True, default=True, null=True)),
                ('delete', models.BooleanField(blank=True, default=True, null=True)),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectMilestoneTaskLogs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now=True, null=True)),
                ('pmt', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectMilestoneTask', to_field='uId')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='projectmilestonetask',
            name='task',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.TaskMaster', to_field='uId'),
        ),
        migrations.CreateModel(
            name='ProjectMembers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PMTAttachments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attachment', models.FileField(blank=True, null=True, upload_to=projects.models.getUploadFilepath)),
                ('attachmentname', models.TextField(blank=True, null=True)),
                ('remark', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('pushToGallary', models.BooleanField(blank=True, default=False, null=True)),
                ('date', models.DateField(blank=True, null=True)),
                ('pmt', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectMilestoneTask', to_field='uId')),
                ('projectId', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.Project', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='MilestoneTaskMapping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=projects.models.getMilestoneTaskMasterUUID, max_length=100, unique=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('milestone', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.MilestoneMaster', to_field='uId')),
                ('task', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.TaskMaster', to_field='uId')),
            ],
        ),
        migrations.CreateModel(
            name='MilestoneTaskCategoryMapping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
                ('milestone', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectMilestone', to_field='uId')),
            ],
        ),
    ]
