from django.contrib import admin
from projects.models import Project,ProjectMilestone,ProjectMembers,MilestoneMaster,TaskMaster,ProjectMilestoneTaskLogs,ProjectMilestoneTask,Milestonestatus,MilestoneTaskMapping
# Register your models here.
admin.site.register(Project)
admin.site.register(ProjectMembers)
admin.site.register(MilestoneMaster)
admin.site.register(TaskMaster)
admin.site.register(ProjectMilestoneTaskLogs)
admin.site.register(ProjectMilestoneTask)
admin.site.register(ProjectMilestone)
admin.site.register(Milestonestatus)
admin.site.register(MilestoneTaskMapping)