# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
import json
from django.contrib.auth import models
from django.contrib.auth.models import User, Group
import string
# from apollodev.settings import DEBUG
import random
from rest_framework import status
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.conf import settings
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import django
import datetime
from datetime import datetime,timedelta,date
from django.contrib import messages
import os
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.db.models import Sum
import calendar
# from masters.models import Currency,ShipmentLocation,SeverityMaster,StatutoryCategory,UtilitiesSystemCategory,Equipment,UOM,building,TypeOfWork,ProjectStatus,ProjectType,country,State,City,department,role,FieldTypes,subbuilding,UtilitiesSystem,Holiday,TaskCategory,EmailTemplateType,EmailTemplate,EmailTemplateRoleMapping
from master.models import UOM,ProjectStatus,ProjectType,country,State,City,role,TaskCategory,Party,Privacy,TeamFunction,ProjectManagementMode,Employee,ProjectType,Priority,TaskStatus
from projects.models import Project,ProjectMembers,ProjectMilestone,MilestoneMaster,TaskMaster,MilestoneTaskMapping,ProjectMilestone,ProjectMilestoneTask,PMTAttachments,ProjectMilestoneTaskLogs,Milestonestatus
# from project.models import BudgetSystem,ProjectBudget,BusinessUnit,Project,ProjectMembers,ProjectMilestone,MilestoneMaster,TaskMaster,MilestoneTaskMapping,ProjectMilestone,ProjectMilestoneTask,milestoneFieldMapping,taskCustomFields,PMTAttachments,ProjectMilestoneTaskLogs,GlobalRAGThreshold,TaskHistory,BuildingProjectMapping,MilestoneTaskCategoryMapping
from django.contrib.auth.models import User,Group
import base64
# from dateutil import relativedelta
from django.db.models import Sum
from userManagements.models import UserMapping,UserPermission
from email.header  import Header
from getpass  import getpass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
import smtplib
import asyncio
from master.views import sendMailFunction
loop = asyncio.get_event_loop()

#--------------------Convert date in DB format------------------#
def getDatefromDateString(dateString):
	date = None
	if(dateString != ""):
		date = datetime.strptime(dateString, "%d-%m-%Y")
		return date
	elif(dateString == 'undefined'):
		return date
	else:
		return date
		
def mailSendingFunction(request):
	try:
		pass
	except:
		pass
#---------------------Convert date in UI format----------------------#
def getDateStringfromDate(dateString):
	if(dateString == "" or dateString == None or dateString == "Null"):
		return ""
	else:
		date = dateString.strftime("%d/%m/%Y")
		return date
def getDateStringfromDateFullMonth(dateString):
	if(dateString == "" or dateString == None or dateString == "Null"):
		return ""
	else:
		date = dateString.strftime("%d-%m-%Y")
		return date
def getPermission(request,action):
	permission = []
	user = request.user.id
	um = UserMapping.objects.get(user_id=user)
	umc = UserMapping.objects.filter(id=user).count()
	if(action == "create"):
		up = UserPermission.objects.filter(role_id = um.role_id,create = True)
	elif(action == "edit"):
		up = UserPermission.objects.filter(role_id = um.role_id,edit = True) 
	elif(action == "delete"):
		up = UserPermission.objects.filter(role_id = um.role_id,delete = True) 
	for u in up:
		permission.append(u.modules_id)
	return permission
	# return HttpResponse(json.dumps({"um":um.id,"user":request.user.id,"role":um.role_id,"umc":umc,"upc1":upc1,"status":"Success","responsecode":"200","permission":permission}),content_type="application/json")


############################################

@login_required(login_url='index')
def projectDetail(request,uId):
	project = Project.objects.get(uId=uId)
	projectmilestone = ProjectMilestone.objects.filter(project_id=uId)
	milestonestatus = Milestonestatus.objects.all()
	taskstatus = TaskStatus.objects.all()
	employee = Employee.objects.filter(isActive=True)
	# backlogList = SubWorkType.objects.filter(isActive = True,client_id = request.session["clientId"],worktype_id=project.uId)

	# user = request.user
	# employeeUserMap = EmployeeUserMap.objects.get(userId_id=user.id,client_id = request.session["clientId"])
	# loggedInEmployee = Employee.objects.get(uId = employeeUserMap.empId_id,client_id = request.session["clientId"])
	# reportingEmpList = []
	# reportingEmpList.append(loggedInEmployee.uId)
	# reportingToLoggedInEmployee = Employee.objects.filter(isActive=True,client_id = request.session["clientId"])
	# for repEmp in reportingToLoggedInEmployee:
	# 	if(repEmp.reporting != None):
	# 		result = [x.strip() for x in repEmp.reporting.split(',')]
	# 		if(loggedInEmployee.uId in result):
	# 			reportingEmpList.append(repEmp.uId)
	#employeeList = Employee.objects.filter(isActive = True,client_id = request.session["clientId"])
	# employeeList = Employee.objects.filter(isActive = True,client_id = request.session["clientId"],uId__in=reportingEmpList)

	data = {
		"project":project,
		"projectmilestone":projectmilestone,
		"milestonestatus":milestonestatus,
		"taskstatus":taskstatus,
		"employee":employee,
		# "backlogList":backlogList,
		# "employeeList":employeeList,
		# "loggedInEmployee":loggedInEmployee,
	}
	return render(request, 'admin_templates/project/projectDetail.html',{"data":data})

############################################



@login_required(login_url='index')
def project(request):
	privacy = Privacy.objects.all()
	priority = Priority.objects.all()
	projectStatus = ProjectStatus.objects.filter(isActive=True)
	teamfunction = TeamFunction.objects.filter(isActive=True)
	projectmanagementmode = ProjectManagementMode.objects.filter(isActive=True)
	employee = Employee.objects.filter(isActive=True)
	projecttype = ProjectType.objects.filter(isActive=True)
	partyList = Party.objects.filter(isActive = True)
	data = {
		"projectStatus":projectStatus,
		"privacy":privacy,
		"teamfunction":teamfunction,
		"projectmanagementmode":projectmanagementmode,
		"employee":employee,
		"projecttype":projecttype,
		"priority":priority,
		# "partyList":partyList,
	}
	return render(request, 'admin_templates/project/project.html',{"data":data})

def addProject(request):
	ps = ProjectStatus.objects.filter(isActive=True)
	pt = ProjectType.objects.filter(isActive=True)
	users = User.objects.filter(is_active=True)
	bunit = BusinessUnit.objects.filter(isActive=True)
	projects = Project.objects.filter(isActive=True)
	con = country.objects.filter(id=100)
	state = State.objects.filter(country_id=100)
	dept = department.objects.filter(isActive = True)
	roles = role.objects.filter(isActive = True)
	pmtl = ProjectMilestoneTaskLogs.objects.all().order_by('-timestamp')[:10]
	data = {"pmtl":pmtl,"dept":dept,"roles":roles,"ps":ps,"pt":pt,"users":users,"bunit":bunit,"projects":projects,"con":con,"state":state}
	return render(request,'admin_templates/project/addProject.html',{"data":data})
	
def editProject(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	# return HttpResponse(uId1)
	ps = ProjectStatus.objects.filter(isActive=True)
	pt = ProjectType.objects.filter(isActive=True)
	users = User.objects.filter(is_active=True)
	bunit = BusinessUnit.objects.filter(isActive=True)
	projects = Project.objects.get(isActive=True,uId=uId1)
	members = ProjectMembers.objects.filter(isActive=True,project_id=uId1)
	con = country.objects.filter(id=100)
	state = State.objects.filter(country_id=100)
	dept = department.objects.filter(isActive = True)
	roles = role.objects.filter(isActive = True)
	memList = []
	for m in members:
		memList.append(m.user_id)
	pmtl = ProjectMilestoneTaskLogs.objects.all().order_by('-timestamp')[:10]
	data = {"pmtl":pmtl,"dept":dept,"roles":roles,"ps":ps,"pt":pt,"users":users,"bunit":bunit,"projects":projects,"memList":memList,"con":con,"state":state}
	return render(request,'admin_templates/project/editProject.html',{"data":data})

#---------------Project Api---------
@login_required(login_url='index')
@api_view(['POST'])
def ProjectApi(request):
	try:
		data = request.POST
		if(data["action"]=="save"):
			if(data["uId"]==""):
				alreadyExistData = Project.objects.filter(name__icontains=data["name"])
				if(alreadyExistData):
					return HttpResponse(json.dumps({"responsecode":"500","Status": "Fail","error":"Project already exists"}), content_type="application/json")
				details = Project()
			elif(data["uId"] != ""):
				alreadyExistData = Project.objects.filter(name__icontains=data["name"])
				if(alreadyExistData):
					return HttpResponse(json.dumps({"responsecode":"500","Status": "Fail","error":"Project already exists"}), content_type="application/json")
				
				else:
					details = Project.objects.get(uId=data["uId"])
			# details.planstartdate = getDatefromDateString(data["planstartDate"])
			# details.actualstartdate = getDatefromDateString(data["actualstartDate"])
			# details.plandate = getDatefromDateString(data["planendDate"])
			# details.actualenddate = getDatefromDateString(data["actualendDate"])
			details.name = data["name"]
			details.key = data["key"]
			details.teamfunction_id = data["teamfunction"]
			details.projectmanagementmode_id = data["projectmanagementmode"]
			details.projectStatus_id = data["projectStatus"]
			details.privacy_id = data["privacy"]
			details.priority_id = data["priority"]
			details.inviteyourteam_id = data["inviteyourteam"]
			details.completiondate = getDatefromDateString(data["completiondate"])
			details.projectType_id = data["projecttype"]
			details.planendDate = getDatefromDateString(data["planenddate"])
			details.planstartDate = getDatefromDateString(data["planstartdate"])
			# details.client_id = request.session["clientId"]
			details.save()
			return HttpResponse(json.dumps({"Status" : "Success", "responsecode":"200","projectId":details.uId ,"name":details.name}), content_type="application/json")
		
		elif(data["action"]=="list"):
			draw = data['draw']
			ListItems = []
			start = int(data['start'])
			length = int(data['length'])
			end = start + length
			searchVal = data['search[value]']
			# totalCount = WorkType.objects.filter(isActive = True,client_id = request.session["clientId"],isProject = True).count()
			totalCount = Project.objects.all().count()
			if(searchVal==""):
				# ListDetails = WorkType.objects.filter(isActive = True,client_id = request.session["clientId"],isProject = True).order_by("-id")[start:end]
				# count = WorkType.objects.filter(isActive = True,client_id = request.session["clientId"],isProject = True).order_by("-id").count()
				ListDetails = Project.objects.all().order_by("-uId")[start:end]
				count = Project.objects.all().order_by("-uId").count()
			else:
				ListDetails = Project.objects.filter(name__icontains=searchVal).order_by("-uId")[start:end]
				count = Project.objects.filter(name__icontains=searchVal).order_by("-uId").count()
				# ListDetails = WorkType.objects.filter(isActive = True,client_id = request.session["clientId"],name__icontains=searchVal).order_by("-id")[start:end]
				# count = WorkType.objects.filter(isActive = True,client_id = request.session["clientId"],name__icontains=searchVal).order_by("-id").count()
			for item in ListDetails:
				# try:
				# 	partyName = item.party.name
				# except Exception as e:
				# 	partyName = ""
				# ListItems.append({"name":item.name,"projectId":item.uId,"client":partyName,"actualstartdate":getDateStringfromDate(item.actualstartdate),"actualenddate":getDateStringfromDate(item.actualenddate),"planstartdate":getDateStringfromDate(item.planstartdate),"planenddate":getDateStringfromDate(item.plandate)})
				ListItems.append({"name":item.name,"uId":item.uId,"privacy":item.priority.name,'owner':item.inviteyourteam.name,"actualstartdate":getDateStringfromDate(item.actualstartDate),"actualenddate":getDateStringfromDate(item.actualendDate),"planstartdate":getDateStringfromDate(item.planstartDate),"planenddate":getDateStringfromDate(item.planendDate)})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : ListItems}), content_type="application/json")
		
		elif(data["action"]=="viewById"):
			item = Project.objects.get(uId=data["uId"])
			# try:
			# 	partyName = item.party.name
			# except Exception as e:
			# 	partyName = ""
			itemObj = {"uId":item.uId,"actualstartdate":getDateStringfromDate(item.actualstartDate),"actualenddate":getDateStringfromDate(item.actualendDate),"name":item.name,"partyId":item.party_id,"planstartdate":getDateStringfromDate(item.planstartDate),"planenddate":getDateStringfromDate(item.planendDate)}
			return HttpResponse(json.dumps({"Status" : "Success", "responsecode":"200", "itemObj" : itemObj}), content_type="application/json")

		elif(data["action"]=="delete"):
			item = Project.objects.get(uId=data["uId"])
			# if(SubWorkType.objects.filter(isActive=True,client_id = request.session["clientId"], worktype_id=item.uId).count()>0):
				# return HttpResponse(json.dumps({"Status" : "Fail", "responsecode":"500", "error" : "Cannot delete as sprint/activity is created against it"}), content_type="application/json")
			# elif(ManualWorkLineItems.objects.filter(isActive=True,client_id = request.session["clientId"], work_id=item.uId).count()>0):
				# return HttpResponse(json.dumps({"Status" : "Fail", "responsecode":"500", "error" : "Cannot delete as it is used in manual work"}), content_type="application/json")
			# elif(ProposalLineItems.objects.filter(isActive=True,client_id = request.session["clientId"], work_id=item.uId).count()>0):
				# return HttpResponse(json.dumps({"Status" : "Fail", "responsecode":"500", "error" : "Cannot delete as it is used in proposal"}), content_type="application/json")
			# else:
			item.delete()
				# item.save()
			return HttpResponse(json.dumps({"Status" : "Success", "responsecode":"200", "name":item.name}), content_type="application/json")

		
		elif(data["action"]=="search"):
			# bunit = data["bunit"]
			# search = data["search"]
			# try:
			# 	user = request.user
			# 	um = UserMapping.objects.filter(user_id = user.id,isActive=True)[0]
			# 	if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
			# 		flag = True
			# 	else:
			# 		flag = False
			# except Exception as e:
			# 	flag = False
			# projects = Project.objects.all()
			cnames = [c.name for c in Project.objects.all()]
			
			# if(bunit!=""):
			# 	projects = projects.filter(businessUnit_id=bunit)
			# if(search!=""):
			# 	projects = projects.filter(name__icontains=search)
			# proList = []
			
			# for p in projects:
			# 	milestoneList = []
			# 	miles = ProjectMilestone.objects.filter(project_id = p.uId,isActive=True,pushToDashboard=True).order_by("milestone__seq")
			# 	per = 0
			# 	for m in miles:
			# 		totalTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True).count()
			# 		openTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True,completionPercentage__lt=100).count()
			# 		closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True).aggregate(Sum('completionPercentage'))
			# 		compper = closeTaskSum["completionPercentage__sum"]
			# 		totalper = totalTask*100
			# 		closeTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True,completionPercentage__gte=100).count()
			# 		try:
			# 			completionPercentage = (compper/totalper)*100
			# 			completionPercentage = round(completionPercentage)
			# 		except Exception as e:
			# 			completionPercentage = 0
			# 		per = completionPercentage
			# 		milestoneList.append({"name":m.milestoneName,"percentage":per})
					
			# 	try:
			# 		projectHead = p.projectHead.first_name+" "+p.projectHead.last_name
			# 	except Exception as e:
			# 		projectHead = ""
			# 	try:
			# 		projectCoordinator = p.projectCoordinator.first_name+" "+p.projectCoordinator.last_name
			# 	except Exception as e:
			# 		projectCoordinator = ""
			# 	try:
			# 		leadCivil = p.leadCivil.first_name+" "+p.leadCivil.last_name
			# 	except:
			# 		leadCivil = ""
			# 	try:
			# 		leadElectrical = p.leadElectrical.first_name+" "+p.leadElectrical.last_name
			# 	except:
			# 		leadElectrical = ""
			# 	try:
			# 		leadUtility = p.leadUtility.first_name+" "+p.leadUtility.last_name
			# 	except:
			# 		leadUtility = ""
				
			# 	try:
			# 		projectManager = p.projectManager.first_name+" "+p.projectManager.last_name
			# 	except:
			# 		projectManager = ""
			# 	try:
			# 		leadMechanical = p.leadMechanical.first_name+" "+p.leadMechanical.last_name
			# 	except:
			# 		leadMechanical = ""
			# 	urlSafeEncodedBytes = base64.urlsafe_b64encode(str(p.uId).encode("utf-8"))
			# 	urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
			# 	totalTask = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True).exclude(milestone__milestoneName="Critical Issues Brochure").count()
			# 	openTask = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True,completionPercentage__lt=100).exclude(milestone__milestoneName="Critical Issues Brochure").count()
			# 	# closeTask = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True,completionPercentage=100).exclude(milestone__milestoneName="Critical Issues Brochure").count()
				
			# 	totalIssues = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True,milestone__milestoneName="Critical Issues Brochure").count()
			# 	openIsuues = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True,completionPercentage__lt=100,milestone__milestoneName="Critical Issues Brochure").count()
			# 	# closeTask = ProjectMilestoneTask.objects.filter(project_id=p.uId,isActive = True,completionPercentage=100,milestone__milestonename="Critical Issues Brochure").count()
			# 	firstTyreRollout = ""
			# 	if(p.firstTyreRollout!=None and p.firstTyreRollout!="" and p.firstTyreRollout!="Null"):
			# 		today = date.today()
			# 		firstTyredate = p.firstTyreRollout
			# 		delta = firstTyredate - today
			# 		firstTyreRollout = delta.days
			# 		if(firstTyreRollout<0):
			# 			firstTyreRollout = 0
			# 	if(flag == False):
			# 		u = request.user
			# 		ucnt = ProjectMembers.objects.filter(user_id = u.id,project_id=p.uId,isActive=True).count()
			# 		if(ucnt>0 or p.projectHead_id == u.id or p.leadCivil_id == u.id or p.leadElectrical_id == u.id or p.leadUtility_id == u.id):
			# 			proList.append({"projectCoordinator":projectCoordinator,"firstTyreRolloutDate":getDateStringfromDateFullMonth(p.firstTyreRollout),"firstTyreRollout":firstTyreRollout,"leadMechanical":leadMechanical,"projectManager":projectManager,"openIsuues":openIsuues,"totalIssues":totalIssues,"totalTask":totalTask,"openTask":openTask,"milestoneList":milestoneList,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"leadUtility":leadUtility,"leadElectrical":leadElectrical,"leadCivil":leadCivil,"projectHead":projectHead,"name":p.name,"status":p.projectStatus.name})
			# 		else:
			# 			pass
				# else:
				# proList.append({"projectCoordinator":projectCoordinator,"firstTyreRolloutDate":getDateStringfromDateFullMonth(p.firstTyreRollout),"firstTyreRollout":firstTyreRollout,"leadMechanical":leadMechanical,"projectManager":projectManager,"openIsuues":openIsuues,"totalIssues":totalIssues,"totalTask":totalTask,"openTask":openTask,"milestoneList":milestoneList,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"leadUtility":leadUtility,"leadElectrical":leadElectrical,"leadCivil":leadCivil,"projectHead":projectHead,"name":p.name,"status":p.projectStatus.name})
			# proList.append({"projects":projects.name})
			# return HttpResponse(json.dumps({"usetr":request.user.id,"flag":flag,"status":"Success","responsecode":"200","proList":proList}),content_type="application/json")
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","proList":cnames}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"responsecode":"500","Status": "Fail","error":str(e)}), content_type="application/json")
		
def milestoneDetails(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	milestones = MilestoneMaster.objects.filter(isActive = True)
	pmtl = ProjectMilestoneTaskLogs.objects.filter(pmt__project_id=uId1).order_by('-timestamp')[:10]
	tc = TaskCategory.objects.filter(isActive = True)
	user = request.user
	um = UserMapping.objects.filter(user_id = user.id,isActive=True)[0]
	if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
		flag = True
	else:
		flag = False
	data = {"flag":flag,"tc":tc,"project":project,"milestones":milestones,"pmtl":pmtl,"uId":uId}
	return render(request,'admin_templates/milestone/milestoneDetails.html',{"data":data})
	
@api_view(['POST'])
def getProcurementStatusList(request):
	uId = request.POST["uId"]
	draw = request.POST['draw']
	list = []
	start = int(request.POST['start'])
	length = int(request.POST['length'])
	end = start + length
	searchVal = request.POST['search[value]']
	totalCount = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).count()
	ListDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).order_by('-id')
	if(searchVal != ""):
		ListDetails = ListDetails.filter(taskname__icontains=searchVal)
	count = len(ListDetails)
	ListDetails = ListDetails[start:end]
	for p in ListDetails:
		if(p.overrideHealthCheck==False):
			rag = calculateRag(p)
			p.healthCheck = rag
			p.save()
		rag = p.healthCheck
		try:
			workType = p.workType.name
		except Exception as e:
			workType = ""
		try:
			building = p.building.name
		except Exception as e:
			building = ""
		try:
			subbuilding = p.subbuilding.name
		except Exception as e:
			subbuilding = ""
		obj = {
			"rag":rag,
			"plannedStartDate":getDateStringfromDate(p.plannedStartDate),
			"completionPercentage":p.completionPercentage,
			"actualEndDate":getDateStringfromDate(p.actualEndDate),
			"actualStartDate":getDateStringfromDate(p.actualStartDate),
			"plannedEndDate":getDateStringfromDate(p.plannedEndDate),
			"uId":p.uId,
			"milestone":p.milestone_id,
			"task":p.taskname,
			"workType":workType,
			"building":building,
			"subbuilding":subbuilding,
			"poNumber":p.poNumber,
			"prNumber":p.prNumber
		}
		list.append(obj)
	return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
	
	
@api_view(['POST'])
def getProcurementStatusListNew(request):
	uId = request.POST["uId"]
	draw = request.POST['draw']
	list = []
	start = int(request.POST['start'])
	length = int(request.POST['length'])
	end = start + length
	searchVal = request.POST['search[value]']
	totalCount = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).count()
	# ListDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).order_by('-id')
	if(searchVal!=""):
		equipmentDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True,equipment__name__icontains=searchVal).distinct('equipment_id')
	else:
		equipmentDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).distinct('equipment_id')
	buildingDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True).distinct('building_id')
	for e in equipmentDetails:
		for b in buildingDetails:
			poRelease = "N"
			dispatchClearance = "N"
			dispatch = "N"
			deliveryAtSite = "N"
			ycount = 0
			try:
				potaskdetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True,equipment_id=e.equipment_id,building_id=b.building_id,taskname="PO Status")
				potaskdetailscount = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True,equipment_id=e.equipment_id,building_id=b.building_id,taskname="PO Status").count()
				if(potaskdetailscount>0):
					if(potaskdetails[0].poNumber!="" and potaskdetails[0].poNumber!="Null" and potaskdetails[0].poNumber!=None):
						poRelease = "Y"
						ycount = ycount+1
						edasdetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True,equipment_id=e.equipment_id,building_id=b.building_id,taskname="Equipment - Delivery at site")
						edasdetailscount = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Mechanical Readiness",isActive=True,equipment_id=e.equipment_id,building_id=b.building_id,taskname="Equipment - Delivery at site").count()
						if(edasdetailscount>0):
							if(edasdetails[0].equipmentDeliveryAtSite!="" and edasdetails[0].equipmentDeliveryAtSite!="Null" and edasdetails[0].equipmentDeliveryAtSite!=None):
								deliveryAtSite = "Y"
								ycount = ycount+1
							if(edasdetails[0].actualDespatchClearanceDate!="" and edasdetails[0].actualDespatchClearanceDate!="Null" and edasdetails[0].actualDespatchClearanceDate!=None):
								dispatchClearance = "Y"
								ycount = ycount+1
								dispatch = "Y"
								ycount = ycount+1
			except:				
				poRelease = "N"
				dispatchClearance = "N"
				dispatch = "N"
				deliveryAtSite = "N"
			if(ycount>3):
				healthCheck = "green"
			elif(ycount>=2):
				healthCheck = "amber"
			elif(ycount>=1):
				healthCheck = "yellow"
			else:
				healthCheck = "red"
			try:
				list.append({"equipment":e.equipment.name,"building":b.building.code,"poRelease":poRelease,"dispatchClearance":dispatchClearance,"dispatch":dispatch,"deliveryAtSite":deliveryAtSite,"healthCheck":healthCheck})
			except:
				pass
	# totalCount = len(list)
	count = len(list)
	list = list[start:end]
	return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")	
	
@api_view(['POST'])
def getStatutoryApprovalList(request):
	uId = request.POST["uId"]
	draw = request.POST['draw']
	list = []
	start = int(request.POST['start'])
	length = int(request.POST['length'])
	end = start + length
	searchVal = request.POST['search[value]']
	totalCount = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Statutory Approval",isActive=True).count()
	ListDetails = ProjectMilestoneTask.objects.filter(project_id = uId,milestone__milestoneName="Statutory Approval",isActive=True).order_by('-id')
	if(searchVal != ""):
		ListDetails = ListDetails.filter(taskname__icontains=searchVal)
	count = len(ListDetails)
	ListDetails = ListDetails[start:end]
	for p in ListDetails:
		if(p.overrideHealthCheck==False):
			rag = calculateRag(p)
			p.healthCheck = rag
			p.save()
		rag = p.healthCheck
		try:
			complianceCategoryName = p.complianceCategory.name
		except Exception as e:
			complianceCategoryName = ""
		obj = {
			"rag":rag,
			"plannedStartDate":getDateStringfromDate(p.plannedStartDate),
			"completionPercentage":p.completionPercentage,
			"actualEndDate":getDateStringfromDate(p.actualEndDate),
			"actualStartDate":getDateStringfromDate(p.actualStartDate),
			"plannedEndDate":getDateStringfromDate(p.plannedEndDate),
			"uId":p.uId,
			"milestone":p.milestone_id,
			"complianceName":p.complianceName,
			"complianceCategory":complianceCategoryName,
			"remark":p.remark
		}
		list.append(obj)
	return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")

@api_view(['POST'])
def milestoneProjectApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = ProjectMilestone.objects.get(uId=data["uId"])
			except:
				details = ProjectMilestone()
			details.project_id = data["project"]
			try:
				m = MilestoneMaster.objects.get(name = data["milestoneName"])
				details.milestone_id = m.uId
			except:
				pass
			# if(data["percentageOverrideFlag"]=="true" or data["percentageOverrideFlag"]==True):
			# 	details.percentageOverrideFlag = True
			# else:
			# 	details.percentageOverrideFlag = False
			# try:
			# 	details.completionPercentage = float(data["completionPercentage"])
			# except:
			# 	pass
			# details.healthCheck = data["healthCheck"]
			details.milestoneName = data["milestoneName"]
			details.description = data["description"]
			details.actualstartDate = getDatefromDateString(data["actualstartDate"])
			details.actualendDate = getDatefromDateString(data["actualendDate"])
			details.planstartDate = getDatefromDateString(data["planstartdate"])
			details.planendDate = getDatefromDateString(data["planenddate"])
			details.milestonestatus_id = data["milestonestatus"]
			# if(data["postToDashboard"]=="true" or data["postToDashboard"]==True):
			# 	details.pushToDashboard	= True
			# else:
			# 	details.pushToDashboard = False
			details.save()
			# category = json.loads(data["category"])
			# mtc = MilestoneTaskCategoryMapping.objects.filter(milestone_id = details.uId,isActive=True)
			# for c in mtc:
				# c.isActive = False
				# c.save()
			# for c in category:
				# try:
					# mt = MilestoneTaskCategoryMapping.objects.get(milestone_id = details.uId,category_id = c)
				# except:
					# mt = MilestoneTaskCategoryMapping()
				# mt.category_id = c
				# mt.milestone_id = details.uId
				# mt.isActive = True
				# mt.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = ProjectMilestone.objects.get(uId=uId)
			startDate = getDateStringfromDate(p.startDate) 
			endDate = getDateStringfromDate(p.endDate)
			# category = MilestoneTaskCategoryMapping.objects.filter(milestone_id = uId,isActive=True)
			# categoryList = []
			# for c in category:
				# categoryList.append(c.category_id)
			obj = {"percentageOverrideFlag":p.percentageOverrideFlag,"completionPercentage":p.completionPercentage,"healthCheck":p.healthCheck,"project":p.project_id,"pushToDashboard":p.pushToDashboard,"description":p.description,"uId":p.uId,"milestoneName":p.milestoneName,"endDate":endDate,"startDate":startDate}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = ProjectMilestone.objects.get(uId=uId)
			p.isActive = False
			p.save()
			pmt = ProjectMilestoneTask.objects.filter(milestone_id = uId)
			for pm in pmt:
				pm.isActive = False
				pm.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		elif(data["action"]=="search"):
			search = data["search"]
			project = data["project"]
			milestone = ProjectMilestone.objects.filter(isActive=True,project_id=project).order_by('milestone__seq','id')
			if(search!=""):
				milestone = ProjectMilestone.filter(name__icontains=search,project_id=project,isActive=True)
			proList = []
			# permission = getPermission(request,"create")
			for p in milestone:
				urlSafeEncodedBytes = base64.urlsafe_b64encode(str(p.uId).encode("utf-8"))
				urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
				totalTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,isActive = True).count()
				openTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,isActive = True,completionPercentage__lt=100).count()
				closeTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,isActive = True,completionPercentage__gte=100).count()
				closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,isActive = True).aggregate(Sum('completionPercentage'))
				compper = closeTaskSum["completionPercentage__sum"]
				totalper = totalTask*100
				completionPercentage = 0
				if(p.percentageOverrideFlag==True):
					try:
						completionPercentage = (compper/totalper)*100
						completionPercentage = round(completionPercentage)
					except:
						completionPercentage = 0
					if(completionPercentage>100):
						completionPercentage = 100
					if(completionPercentage<100):
						p.mailSentFlag = False
					p.completionPercentage = completionPercentage
					p.save()
				else:
					completionPercentage = p.completionPercentage
				if(p.mailSentFlag == False and completionPercentage == 100):
					loop.run_in_executor(None,sendMailFunction,"Milestone Closed",p.uId)
					p.mailSentFlag = True
					p.save()
				proList.append({"totalTask":totalTask,"closeTask":closeTask,"openTask":openTask,"completionPercentage":completionPercentage,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"milestoneName":p.milestoneName,"completionPercentage":p.completionPercentage,"healthCheck":p.healthCheck})
				# if(totalTask>0):
					# # permissionCount = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,isActive = True,workType_id__in=permission).count()
					# # if(permissionCount>0):
					# proList.append({"totalTask":totalTask,"closeTask":closeTask,"openTask":openTask,"completionPercentage":completionPercentage,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"milestoneName":p.milestoneName})
				# else:
					# proList.append({"totalTask":totalTask,"closeTask":closeTask,"openTask":openTask,"completionPercentage":completionPercentage,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"milestoneName":p.milestoneName})
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","proList":proList}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
def taskDetails(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	milestone = ProjectMilestone.objects.get(isActive = True,uId=uId1)
	# buildings = building.objects.filter(isActive=True)
	buildings = BuildingProjectMapping.objects.filter(project_id = milestone.project_id,isActive=True).distinct('building_id')
	totalTask = ProjectMilestoneTask.objects.filter(milestone_id=uId1,project_id = milestone.project_id,isActive = True).count()
	openTask = ProjectMilestoneTask.objects.filter(milestone_id=uId1,project_id = milestone.project_id,isActive = True,completionPercentage__lt=100).count()
	closeTask = ProjectMilestoneTask.objects.filter(milestone_id=uId1,project_id = milestone.project_id,isActive = True,completionPercentage__gte=100).count()
	closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=uId1,isActive = True).aggregate(Sum('completionPercentage'))
	compper = closeTaskSum["completionPercentage__sum"]
	totalper = totalTask*100
	try:
		completionPercentage = (compper/totalper)*100
		completionPercentage = round(completionPercentage)
	except:
		completionPercentage = 0
	createpermission = getPermission(request,"create")
	# return createpermission
	if(len(createpermission)>0):
		createFlag = True
	else:
		createFlag = False
	editpermission = getPermission(request,"edit")
	deletepermission = getPermission(request,"delete")
	equipment = Equipment.objects.filter(isActive=True)
	flag = False
	try:
		um = UserMapping.objects.filter(user_id = request.user.id,isActive=True)[0]
		if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
			flag = True
		else:
			flag = False
	except:
		pass
	data = {"isAdmin":flag,"equipment":equipment,"deletepermission":deletepermission,"editpermission":editpermission,"createFlag":createFlag,"buildings":buildings,"completionPercentage":completionPercentage,"milestone":milestone,"encodeduId":uId,"totalTask":totalTask,"openTask":openTask,"closeTask":closeTask}
	# return HttpResponse(json.dumps({"status":"Success","responsecode":"200","createFlag":createFlag}),content_type="application/json")
	return render(request,'admin_templates/task/taskDetails.html',{"data":data})
	

def gallary(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	# milestones = MilestoneMaster.objects.filter(isActive = True)
	pmtl = PMTAttachments.objects.filter(pmt__project_id=uId1,pushToGallary=True,isActive=True).order_by('-date')
	bpm = BuildingProjectMapping.objects.filter(project_id = uId1,isActive=True).distinct('building_id')
	attachmentList = []
	milestoneslist = ProjectMilestoneTask.objects.filter(project_id = uId1).distinct('milestone_id')
	for aa in pmtl:
		if(aa.attachment=="" or aa.attachment=="NULL" or aa.attachment==None or aa.attachment=="undefined"):
			pass
		else:
			fileu = aa.attachment.url
			filepath=fileu.replace(settings.MEDIA_ROOT,"")
			fileurl=settings.SITENAME+filepath
			date = getDateStringfromDate(aa.date)
			attachmentList.append({"date":date,"remark":aa.remark,"fileurl":fileurl,"name":aa.attachmentname,"milestone":aa.pmt.milestone.milestoneName,"task":aa.pmt.taskname})
	data = {"milestoneslist":milestoneslist,"bpm":bpm,"uId":uId,"project":project,"attachmentList":attachmentList}
	return render(request,'admin_templates/project/gallary.html',{"data":data})
	
def buildingGallary(request,b,p,m):
	bb = base64.b64decode(b).decode('utf-8')
	pp = base64.b64decode(p).decode('utf-8')
	if(m == "abcpqr"):
		mm = ""
	else:
		mm = base64.b64decode(m).decode('utf-8')
	project = Project.objects.get(uId=pp)
	pmtl = PMTAttachments.objects.filter(pmt__building_id=bb,pmt__project_id=pp,pushToGallary=True,isActive=True).order_by('-date')
	if(mm!=""):
		pmtl = pmtl.filter(pmt__milestone_id = mm)
	bpm = BuildingProjectMapping.objects.filter(building_id=bb,project_id = pp,isActive=True).distinct('building_id')
	milestoneslist = ProjectMilestoneTask.objects.filter(project_id = pp).distinct('milestone_id')
	attachmentList = []
	for aa in pmtl:
		if(aa.attachment=="" or aa.attachment=="NULL" or aa.attachment==None or aa.attachment=="undefined"):
			pass
		else:
			fileu = aa.attachment.url
			filepath=fileu.replace(settings.MEDIA_ROOT,"")
			fileurl=settings.SITENAME+filepath
			attachmentList.append({"remark":aa.remark,"fileurl":fileurl,"name":aa.attachmentname,"milestone":aa.pmt.milestone.milestoneName,"task":aa.pmt.taskname})
	data = {"mm":mm,"milestoneslist":milestoneslist,"bb":bb,"bpm":bpm,"uId":p,"project":project,"attachmentList":attachmentList}
	return render(request,'admin_templates/project/gallary.html',{"data":data})

def projectOverview(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	# milestones = MilestoneMaster.objects.filter(isActive = True)
	# pmtl = ProjectMilestoneTaskLogs.objects.all().order_by('-timestamp')[:10]
	milestoneList = []
	miles = ProjectMilestone.objects.filter(project_id = uId1,isActive=True).order_by("milestone__seq")
	per = 0
	for m in miles:
		totalTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True).count()
		openTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True,completionPercentage__lt=100).count()
		closeTask = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True,completionPercentage__gte=100).count()
		closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=m.uId,isActive = True).aggregate(Sum('completionPercentage'))
		compper = closeTaskSum["completionPercentage__sum"]
		totalper = totalTask*100
		try:
			completionPercentage = (compper/totalper)*100
			completionPercentage = round(completionPercentage)
		except Exception as e:
			completionPercentage = 0
		per = completionPercentage
		milestoneList.append({"name":m.milestoneName,"percentage":per})
	try:
		projectHead = project.projectHead.first_name+" "+project.projectHead.last_name
	except Exception as e:
		projectHead = ""
	try:
		projectManager = project.projectManager.first_name+" "+project.projectManager.last_name
	except Exception as e:
		projectManager = ""
	try:
		projectCoordinator = project.projectCoordinator.first_name+" "+project.projectCoordinator.last_name
	except Exception as e:
		projectCoordinator = ""
	try:
		leadCivil = project.leadCivil.first_name+" "+project.leadCivil.last_name
	except:
		leadCivil = ""
	try:
		leadElectrical = project.leadElectrical.first_name+" "+project.leadElectrical.last_name
	except:
		leadElectrical = ""
	try:
		leadUtility = project.leadUtility.first_name+" "+project.leadUtility.last_name
	except:
		leadUtility = ""
	try:
		leadMechanical = project.leadMechanical.first_name+" "+project.leadMechanical.last_name
	except:
		leadMechanical = ""
	totalTask = ProjectMilestoneTask.objects.filter(project_id=uId1,isActive = True).exclude(milestone__milestoneName="Critical Issues Brochure").count()
	openTask = ProjectMilestoneTask.objects.filter(project_id=uId1,isActive = True,completionPercentage__lt=100).exclude(milestone__milestoneName="Critical Issues Brochure").count()
	# closeTask = ProjectMilestoneTask.objects.filter(milestone_id=uId1,isActive = True,completionPercentage=100).exclude(milestone__milestoneName="Critical Issues Brochure").count()
	
	totalIssues = ProjectMilestoneTask.objects.filter(project_id=uId1,isActive = True,milestone__milestoneName="Critical Issues Brochure").count()
	openIsuues = ProjectMilestoneTask.objects.filter(project_id=uId1,isActive = True,completionPercentage__lt=100,milestone__milestoneName="Critical Issues Brochure").count()
	# closeTask = ProjectMilestoneTask.objects.filter(milestone_id=uId1,isActive = True,completionPercentage=100,milestone__milestonename="Critical Issues Brochure").count()
		
	data = {"projectCoordinator":projectCoordinator,"projectManager":projectManager,"openIsuues":openIsuues,"totalIssues":totalIssues,"totalTask":totalTask,"openTask":openTask,"milestoneList":milestoneList,"leadUtility":leadUtility,"leadMechanical":leadMechanical,"leadElectrical":leadElectrical,"leadCivil":leadCivil,"projectHead":projectHead,"uId":uId,"project":project}
	return render(request,'admin_templates/project/projectOverview.html',{"data":data})
	
def buildingLayout1(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	buildingList = building.objects.filter(isActive = True)
	bpm = BuildingProjectMapping.objects.filter(project_id = uId1,isActive=True).distinct('building_id')
	for b in bpm:
		urlSafeEncodedBytes = base64.urlsafe_b64encode(str(b.building_id).encode("utf-8"))
		urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
		b.encodedBuildingId = urlSafeEncodedStr
		totalCount = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True).count()
		openTask = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__lt=100).count()
		closeTask = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gte=100).count()
		closeTaskSum = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
		compper = closeTaskSum["completionPercentage__sum"]
		totalper = totalCount*100
		try:
			per = (compper/totalper)*100
			percentage = round(per)
		except:
			percentage = 0
		b.openTask = openTask
		b.closeTask = closeTask
		b.totalTask = totalCount
		b.percentage = percentage
		
		pmilestone = ProjectMilestone.objects.filter(project_id = uId1,isActive=True).distinct('milestoneName')
		milestoneList = []
		for p in pmilestone:
			totalCount = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True).count()
			openTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__lt=100).count()
			closeTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__gte=100).count()
			closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
			compper = closeTaskSum["completionPercentage__sum"]
			totalper = totalCount*100
			try:
				per = (compper/totalper)*100
				percentage = round(per)
			except:
				percentage = 0
			milestoneList.append({"name":p.milestoneName,"percentage":percentage})
		b.milestoneList = milestoneList
	data = {"uId":uId,"project":project,"bpm":bpm,"buildingList":buildingList}
	return render(request,'admin_templates/project/buildingLayout.html',{"data":data})
	
def buildingLayout(request,uId,pm):
	uId1 = base64.b64decode(uId).decode('utf-8')
	if(pm == "abcpqr"):
		pm1 = ""
	else:
		pm1 = base64.b64decode(pm).decode('utf-8')
	
	project = Project.objects.get(uId=uId1)
	buildingList = building.objects.filter(isActive = True)
	dataList = []
	bpm = BuildingProjectMapping.objects.filter(project_id = uId1,isActive=True).distinct('building_id')
	for b in bpm:
		urlSafeEncodedBytes = base64.urlsafe_b64encode(str(b.building_id).encode("utf-8"))
		urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
		b.encodedBuildingId = urlSafeEncodedStr
		if(pm1!=""):
			totalCount = ProjectMilestoneTask.objects.filter(milestone_id=pm1,project_id = b.project_id,building_id = b.building_id,isActive=True).count()
		else:
			totalCount = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True).count()
		if(pm1!=""):
			openTask = ProjectMilestoneTask.objects.filter(milestone_id=pm1,project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__lt=100).count()
		else:
			openTask = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__lt=100).count()
		if(pm1!=""):
			closeTask = ProjectMilestoneTask.objects.filter(milestone_id=pm1,project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gte=100).count()
		else:
			closeTask = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gte=100).count()
		if(pm1!=""):
			closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=pm1,project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
		else:
			closeTaskSum = ProjectMilestoneTask.objects.filter(project_id = b.project_id,building_id = b.building_id,isActive=True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
		
		compper = closeTaskSum["completionPercentage__sum"]
		totalper = totalCount*100
		try:
			per = (compper/totalper)*100
			percentage = round(per)
		except:
			percentage = 0
		b.openTask = openTask
		b.closeTask = closeTask
		b.totalTask = totalCount
		b.percentage = percentage
		
		pmilestone = ProjectMilestone.objects.filter(project_id = uId1,isActive=True).distinct('milestoneName')
		milestoneList = []
		for p in pmilestone:
			totalCount = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True).count()
			openTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__lt=100).count()
			closeTask = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__gte=100).count()
			closeTaskSum = ProjectMilestoneTask.objects.filter(milestone_id=p.uId,building_id=b.building_id,isActive=True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
			compper = closeTaskSum["completionPercentage__sum"]
			totalper = totalCount*100
			try:
				per = (compper/totalper)*100
				percentage = round(per)
			except:
				percentage = 0
			milestoneList.append({"name":p.milestoneName,"percentage":percentage})
		b.milestoneList = milestoneList
		b.chartData = json.dumps(getDataForActualVsPlannedNosLineChart(uId1,pm1,b.building_id))
		# dataList.append({"cnt":getDataForActualVsPlannedNosLineChart(uId1,pm1,b.building_id),"pm1":pm1,"b":b.building_id})
	# return HttpResponse(dataList)
	milestones = ProjectMilestone.objects.filter(project_id=uId1)
	data = {"pm1":pm1,"uId":uId,"project":project,"bpm":bpm,"buildingList":buildingList,"pm":pm,"milestones":milestones}
	return render(request,'admin_templates/project/buildingLayout.html',{"data":data})
	
def buildingTask(request,b,p,m):
	bb = base64.b64decode(b).decode('utf-8')
	pp = base64.b64decode(p).decode('utf-8')
	milestone = ""
	if(m == "abcpqr"):
		mm = ""
		milestone = ""
	else:
		mm = base64.b64decode(m).decode('utf-8')
		milestone = ProjectMilestone.objects.get(uId=mm)
	milestones = ProjectMilestone.objects.filter(project_id=pp)
	equipment = Equipment.objects.filter(isActive=True)
	createpermission = getPermission(request,"create")
	if(len(createpermission)>0):
		createFlag = True
	else:
		createFlag = False
	editpermission = getPermission(request,"edit")
	deletepermission = getPermission(request,"delete")
	flag = False
	try:
		um = UserMapping.objects.filter(user_id = request.user.id,isActive=True)[0]
		if(um.role.name == "CXO" or um.role.name == "PMO" or um.role.name == "admin"):
			flag = True
		else:
			flag = False
	except:
		pass
	data = {"isAdmin":flag,"equipment":equipment,"milestone":milestone,"deletepermission":deletepermission,"editpermission":editpermission,"createFlag":createFlag,"b":bb,"p":pp,"m":mm,"encodedMilestoneId":m,"milestones":milestones}
	return render(request,'admin_templates/project/buildingTask.html',{"data":data})
	
def addTask(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	user = request.user.id
	permission = getPermission(request,"create")
	tow = TypeOfWork.objects.filter(isActive=True,uId__in=permission)
	milestones = ProjectMilestone.objects.get(isActive = True,uId=uId1)
	buildings = BuildingProjectMapping.objects.filter(project_id = milestones.project_id,isActive=True).distinct('building_id')
	uoms = UOM.objects.filter(isActive=True)
	equipments = Equipment.objects.filter(isActive=True).order_by('name')
	systems = UtilitiesSystemCategory.objects.filter(isActive=True)
	shipmentLocation = ShipmentLocation.objects.filter(isActive=True)
	categorys = StatutoryCategory.objects.filter(isActive = True)
	severitys = SeverityMaster.objects.filter(isActive = True)
	tasks = TaskMaster.objects.filter(isActive = True)
	fieldtypeslist = FieldTypes.objects.filter(isActive = True)
	taskCategory = MilestoneTaskCategoryMapping.objects.filter(milestone_id=uId1,isActive=True)
	fieldList = []
	try:
		milestoneMasterDetails = MilestoneMaster.objects.get(uId = milestones.milestone_id,isDefault=True,isActive=True)
		taskfields = milestoneFieldMapping.objects.filter(milestone_id = milestones.milestone_id)
		for t in taskfields:
			fieldList.append(t.taskfield)
	except Exception as e:
		# fieldList.append(str(e))
		pass
	if(len(fieldList)==0):
		fieldList = ["task","workType","building","subbuilding","plannedStartDate","plannedEndDate","actualStartDate","actualEndDate","completionPercentage","prNumber","poNumber","attachment","remark","plannedNos","actualNos","buildingDescription","healthCheck","RAGReason","uom","postToDashboard","equipment","system","subsystem","ShowToGraph"]
	data = {"shipmentLocation":shipmentLocation,"taskCategory":taskCategory,"fieldtypeslist":fieldtypeslist,"fieldList":fieldList,"tasks":tasks,"severitys":severitys,"categorys":categorys,"systems":systems,"milestones":milestones,"encodeduId":uId,"tow":tow,"buildings":buildings,"uoms":uoms,"equipments":equipments}
	return render(request,'admin_templates/task/addTask.html',{"data":data})
	
def editTask(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	user = request.user.id
	permission = getPermission(request,"edit")
	tow = TypeOfWork.objects.filter(isActive=True,uId__in=permission)
	# tow = TypeOfWork.objects.filter(isActive=True)
	pmtask = ProjectMilestoneTask.objects.get(uId=uId1)
	buildings = BuildingProjectMapping.objects.filter(project_id = pmtask.project_id,isActive=True).distinct('building_id')
	uoms = UOM.objects.filter(isActive=True)
	equipments = Equipment.objects.filter(isActive=True).order_by('name')
	systems = UtilitiesSystemCategory.objects.filter(isActive=True)
	categorys = StatutoryCategory.objects.filter(isActive = True)
	severitys = SeverityMaster.objects.filter(isActive = True)
	tasks = TaskMaster.objects.filter(isActive = True)
	milestones = ProjectMilestone.objects.get(isActive = True,uId=pmtask.milestone_id)
	fieldtypeslist = FieldTypes.objects.filter(isActive = True)
	customFields = taskCustomFields.objects.filter(task_id = uId1)
	taskCategory = MilestoneTaskCategoryMapping.objects.filter(milestone_id=pmtask.milestone_id,isActive=True)
	shipmentLocation = ShipmentLocation.objects.filter(isActive=True)
	fieldList = []
	try:
		milestoneMasterDetails = MilestoneMaster.objects.get(uId = milestones.milestone_id,isDefault=True,isActive=True)
		taskfields = milestoneFieldMapping.objects.filter(milestone_id = milestones.milestone_id)
		for t in taskfields:
			fieldList.append(t.taskfield)
	except:
		pass
	if(len(fieldList)==0):
		fieldList = ["task","workType","building","subbuilding","plannedStartDate","plannedEndDate","actualStartDate","actualEndDate","completionPercentage","prNumber","poNumber","attachment","remark","plannedNos","actualNos","buildingDescription","healthCheck","RAGReason","uom","postToDashboard","equipment","system","subsystem","ShowToGraph"]
	subbuildinglist = []
	listdetailssub = subbuilding.objects.filter(building_id = pmtask.building_id)
	for s in listdetailssub:
		subbuildinglist.append({"uId":s.uId,"name":s.name})
	subsystemlist = []
	listdetailsuti = UtilitiesSystem.objects.filter(category_id = pmtask.system_id)
	for s in listdetailsuti:
		subsystemlist.append({"uId":s.uId,"name":s.name})
	fileurl=""
	fileList = []
	attach = PMTAttachments.objects.filter(pmt_id = uId1,isActive=True)
	for aa in attach:
		if(aa.attachment=="" or aa.attachment=="NULL" or aa.attachment==None or aa.attachment=="undefined"):
			pass
		else:
			fileu = aa.attachment.url
			filepath=fileu.replace(settings.MEDIA_ROOT,"")
			fileurl=settings.SITENAME+filepath
			if(aa.pushToGallary==True):
				push = "Y"
			else:
				push = "N"
			date = getDateStringfromDate(aa.date)
			fileList.append({"attachmentRemark":aa.remark,"date":date,"fileUrl":fileurl,"id":aa.id,"name":aa.attachmentname,"pushToGallary":push})
	data = {"shipmentLocation":shipmentLocation,"taskCategory":taskCategory,"fileList":fileList,"subsystemlist":subsystemlist,"subbuildinglist":subbuildinglist,"pmtask":pmtask,"customFields":customFields,"fieldtypeslist":fieldtypeslist,"fieldList":fieldList,"tasks":tasks,"severitys":severitys,"categorys":categorys,"systems":systems,"milestones":milestones,"tow":tow,"buildings":buildings,"uoms":uoms,"equipments":equipments}
	return render(request,'admin_templates/task/editTask.html',{"data":data})
	
	
def updatePMTTask(p,u):
	pmtl = ProjectMilestoneTaskLogs()
	pmtl.user_id = u
	pmtl.pmt_id = p
	pmtl.save()
	pm = TaskHistory()
	pm.user_id = u
	pm.field = "Task Added"
	pm.pmt_id = p
	pm.save()
	
@api_view(['POST'])
def projectMilestoneTaskApi(request):
	# try:
		data = request.data
		if(data["action"] == "create"):
			try:
				pmt = ProjectMilestoneTask.objects.get(uId = data["uId"])
			except:
				pmt = ProjectMilestoneTask()
			
			pmt.project_id = data["project"]
			pmt.milestone_id = data["milestoneId"]
			try:
				milestoneMapping = MilestoneTaskMapping.objects.filter(milestone_id=data["milestoneId"],task__name=data["task"])[0]
				pmt.task_id = milestoneMapping.task_id
			except:
				pass
			try:
				pmt.taskname = data["task"]
				try:
					addToMaster = data["addToMaster"]
					if(addToMaster=="true" or addToMaster==True):
						try:
							tm = TaskMaster.objects.get(name = data["task"])
						except:
							tm = TaskMaster()
						tm.name = data["task"]
						tm.save()
					else:
						pass
				except:
					pass
			except:
				pass
			
			# try:
			# 	pmt.workType_id = data["workType"]
			# except:
			# 	pass
			# try:
			# 	pmt.building_id = data["building"]
			# except:
			# 	pass
			# try:
			# 	pmt.subbuilding_id = data["subbuilding"]
			# except:
			# 	pass
			try:
				pmt.plannedStartDate = getDatefromDateString(data["planstartdate"])
			except:
				pass
			# try:
			# 	pmt.poDate = getDatefromDateString(data["poDate"])
			# except:
			# 	pass
			try:
				pmt.plannedEndDate = getDatefromDateString(data["planenddate"])
			except:
				pass
			try:
				pmt.actualStartDate = getDatefromDateString(data["actualstartdate"])
			except:
				pass
			try:
				pmt.actualEndDate = getDatefromDateString(data["actualenddate"])
			except:
				pass
			# try:
			# 	pmt.completionPercentage = data["completionPercentage"]
			# 	if(pmt.completionPercentage<100):
			# 		pmt.taskCloseMailStatus = False
			# 		pmt.save()
			# 	if(pmt.taskCloseMailStatus==False and pmt.completionPercentage==100):
			# 		loop.run_in_executor(None,sendMailFunction,"Task Closed",pmt.uId)
			# 		pmt.taskCloseMailStatus = True
			# 		pmt.save()
			# except:
			# 	pass
			# try:
			# 	pmt.prNumber = data["prNumber"]
			# except:
			# 	pass
			# try:
			# 	pmt.poNumber = data["poNumber"]
			# except:
			# 	pass
			try:
				pmt.remark = data["remark"]
			except:
				pass
			# try:
			# 	pmt.plannedNos = data["plannedNos"]
			# except:
			# 	pass
			# try:
			# 	pmt.actualNos = data["actualNos"]
			# except:
			# 	pass
			# try:
			# 	pmt.buildingDescription = data["buildingDescription"]
			# except:
			# 	pass
			# try:
			# 	if(data["healthCheck"]!="white"):
			# 		pmt.overrideHealthCheck = True
			# 	pmt.healthCheck = data["healthCheck"]
			# except:
			# 	pass
			# try:
			# 	pmt.RAGReason = data["RAGReason"]
			# except:
			# 	pass
			# try:
			# 	pmt.uom_id = data["uom"]
			# except:
			# 	pass
			# try:
			# 	if(data["postToDashboard"]=="true" or data["postToDashboard"]==True):
			# 		pmt.postToDashboard = True
			# 	else:
			# 		pmt.postToDashboard = False
			# except:
			# 	pass
			# try:
			# 	if(data["ShowToGraph"]=="true" or data["ShowToGraph"]==True):
			# 		pmt.ShowToGraph = True
			# 	else:
			# 		pmt.ShowToGraph = False
			# except:
			# 	pass
			# try:
			# 	if(data["showNumbers"]=="true" or data["showNumbers"]==True):
			# 		pmt.showNumbers = True
			# 	else:
			# 		pmt.showNumbers = False
			# except:
			# 	pass
			# try:
			# 	if(data["measureByDate"]=="true" or data["measureByDate"]==True):
			# 		pmt.measureByDate = True
			# 	else:
			# 		pmt.measureByDate = False
			# except:
			# 	pass
			# try:
			# 	if(data["measureByNumbers"]=="true" or data["measureByNumbers"]==True):
			# 		pmt.measureByNumbers = True
			# 	else:
			# 		pmt.measureByNumbers = False
			# except:
			# 	pass
			# try:
			# 	pmt.equipment_id = data["equipment"]
			# except:
			# 	pass
			# try:
			# 	pmt.system_id = data["system"]
			# except:
			# 	pass
			# try:
			# 	pmt.subsystem_id = data["subsystem"]
			# except:
			# 	pass
			# try:
			# 	pmt.complianceName = data["complianceName"]
			# except:
			# 	pass
			# try:
			# 	pmt.complianceCategory_id = data["complianceCategory"]
			# except:
			# 	pass
			# try:
			# 	pmt.cib = data["cib"]
			# except:
			# 	pass
			# try:
			# 	pmt.responsibility = data["responsibility"]
			# except:
			# 	pass
			# try:
			# 	pmt.area = data["area"]
			# except:
			# 	pass
			# try:
			# 	pmt.severity_id = data["severity"]
			# except:
			# 	pass
			# try:
			# 	pmt.postedDate = getDatefromDateString(data["postedDate"])
			# except:
			# 	pass
			# try:
			# 	pmt.closedDate = getDatefromDateString(data["closedDate"])
			# except:
			# 	pass
			# try:
			# 	pmt.status = data["status"]
			# except:
			# 	pass
			# try:
			# 	pmt.risk = data["risk"]
			# except:
			# 	pass
			# try:
			# 	pmt.migrationPlan = data["migrationPlan"]
			# except:
			# 	pass
			# try:
			# 	pmt.plannedDuration = data["plannedDuration"]
			# except:
			# 	pass
			# try:
			# 	pmt.actualDuration = data["actualDuration"]
			# except:
			# 	pass
			# try:
			# 	pmt.prerequisites = data["prerequisites"]
			# except:
			# 	pass
			# try:
			# 	pmt.approvalauthority = data["approvalauthority"]
			# except:
			# 	pass
			# try:
			# 	pmt.taskCategory_id = data["taskCategory"]
			# except:
			# 	pass
			# try:
			# 	pmt.manufacturingLeadTime = data["manufacturingLeadTime"]
			# except:
			# 	pass
			# try:
			# 	pmt.packingAndShipment = data["packingAndShipment"]
			# except:
			# 	pass
			# try:
			# 	pmt.shipmentDuration = data["shipmentDuration"]
			# except:
			# 	pass
			# try:
			# 	pmt.customsClearances = data["customsClearances"]
			# except:
			# 	pass
			# try:
			# 	pmt.landTransportation = data["landTransportation"]
			# except:
			# 	pass
			# try:
			# 	pmt.shipmentLocation_id = data["shipmentLocation"]
			# except:
			# 	pass
			# try:
			# 	pmt.plannedDespatchClearanceDate = getDatefromDateString(data["plannedDespatchClearanceDate"])
			# except:
			# 	pass
			# try:
			# 	pmt.actualDespatchClearanceDate = getDatefromDateString(data["actualDespatchClearanceDate"])
			# except:
			# 	pass
			try:
				pmt.taskstatus_id = data["taskstatus"]
			except:
				pass
			pmt.save()
			res =""
			# try:
			# 	ccount = int(data["attachmentCount"])
			# 	while ccount>0:
			# 		pmtatt = PMTAttachments()
			# 		cnt = str(ccount)
			# 		pmtatt.attachment = data["file"+cnt]
			# 		if(data["pushToGallary"+cnt]=="true" or data["pushToGallary"+cnt]==True):
			# 			pmtatt.pushToGallary = True
			# 		else:
			# 			pmtatt.pushToGallary = False
			# 		pmtatt.pmt_id = pmt.uId
			# 		pmtatt.remark = data["attachmentRemark"+cnt]
			# 		pmtatt.date = getDatefromDateString(data["date"+cnt])
			# 		pmtatt.save()
			# 		ccount = ccount-1
			# 		t = TaskHistory()
			# 		t.user_id = request.user.id
			# 		t.pmt_id = data["uId"]
			# 		t.field =  "Attachment Added"
			# 		# t.oldValue = pmt.migrationPlan
			# 		t.newValue = pmtatt.attachmentname
			# 		t.save()
			# except Exception as e:
			# 	res = str(e)
			# 	pass
			# for cf in json.loads(data["customFields"]):
			# 	try:
			# 		tcf = taskCustomFields.objects.get(id = cf["tcfId"])
			# 	except:
			# 		tcf = taskCustomFields()
			# 	tcf.field_id = cf["field"]
			# 	tcf.task_id = pmt.uId
			# 	tcf.value = cf["value"]
			# 	tcf.options = cf["options"]
			# 	tcf.fieldname = cf["fieldname"]
			# 	tcf.save()
			# 	t = TaskHistory()
			# 	t.user_id = request.user.id
			# 	t.pmt_id = pmt.uId
			# 	t.field =  "Custom Field Added: "+cf["fieldname"]
			# 	# t.oldValue = pmt.migrationPlan
			# 	t.newValue = cf["value"]
			# 	t.save()
			# updatePMTTask(pmt.uId,request.user.id)
			# loop.run_in_executor(None,sendMailFunction,"Task Creation",pmt.uId)
			return HttpResponse(json.dumps({"res":res,"status":"Success","responsecode":"200","taskname":pmt.taskname}),content_type="application/json")
		
		if(data["action"] == "edit"):
			try:
				pmt = ProjectMilestoneTask.objects.get(uId = data["uId"])
			except:
				pmt = ProjectMilestoneTask()
			
			pmt.project_id = data["project"]
			pmt.milestone_id = data["projectMilestoneId"]
			try:
				milestoneMapping = MilestoneTaskMapping.objects.filter(milestone_id=data["milestoneId"],task__name=data["task"])[0]
				pmt.task_id = milestoneMapping.task_id
			except:
				pass
			try:
				if(pmt.taskname != data["task"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Task Name"
					t.oldValue = pmt.taskname
					t.newValue = data["task"]
					t.save()
				pmt.taskname = data["task"]
				try:
					addToMaster = data["addToMaster"]
					if(addToMaster=="true" or addToMaster==True):
						try:
							tm = TaskMaster.objects.get(name = data["task"])
						except:
							tm = TaskMaster()
						tm.name = data["task"]
						tm.save()
					else:
						pass
				except:
					pass
			except:
				pass
			
			try:
				if(pmt.workType_id != data["workType"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Work Type"
					try:
						t.oldValue = pmt.workType.name
					except:
						t.oldValue = ""
					t.save()
					pmt.workType_id = data["workType"]
					t.newValue = pmt.workType.name
					t.save()
			except:
				pass
			try:
				if(pmt.building_id != data["building"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Building"
					try:
						t.oldValue = pmt.building.name
					except:
						t.oldValue = ""
					t.save()
					pmt.building_id = data["building"]
					t.newValue = pmt.building.name
					t.save()
			except:
				pass
			try:
				if(pmt.subbuilding_id != data["subbuilding"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Sub Building"
					try:
						t.oldValue = pmt.subbuilding.name
					except:
						t.oldValue = ""
					t.save()
					pmt.subbuilding_id = data["subbuilding"]
					t.newValue = pmt.subbuilding.name
					t.save()
				
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.plannedStartDate) != (data["plannedStartDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Planned Start Date"
					t.oldValue = getDateStringfromDate(pmt.plannedStartDate)
					t.newValue = data["plannedStartDate"]
					t.save()
				pmt.plannedStartDate = getDatefromDateString(data["plannedStartDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.poDate) != (data["poDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "PO Date"
					t.oldValue = getDateStringfromDate(pmt.poDate)
					t.newValue = data["poDate"]
					t.save()
				pmt.poDate = getDatefromDateString(data["poDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.plannedEndDate) != (data["plannedEndDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Planned End Date"
					t.oldValue = getDateStringfromDate(pmt.plannedEndDate)
					t.newValue = data["plannedEndDate"]
					t.save()
				pmt.plannedEndDate = getDatefromDateString(data["plannedEndDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.actualStartDate) != (data["actualStartDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Actual Start Date"
					t.oldValue = getDateStringfromDate(pmt.actualStartDate)
					t.newValue = data["actualStartDate"]
					t.save()
				pmt.actualStartDate = getDatefromDateString(data["actualStartDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.actualEndDate) != (data["actualEndDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Actual End Date"
					t.oldValue = getDateStringfromDate(pmt.actualEndDate)
					t.newValue = data["actualEndDate"]
					t.save()
				pmt.actualEndDate = getDatefromDateString(data["actualEndDate"])
			except:
				pass
			try:
				if(pmt.completionPercentage != float(data["completionPercentage"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Completion Percentage"
					t.oldValue = pmt.completionPercentage
					t.newValue = data["completionPercentage"]
					t.save()
				pmt.completionPercentage = data["completionPercentage"]
				if(pmt.completionPercentage<100):
					pmt.taskCloseMailStatus = False
					pmt.save()
				if(pmt.taskCloseMailStatus==False and pmt.completionPercentage==100):
					loop.run_in_executor(None,sendMailFunction,"Task Closed",pmt.uId)
					pmt.taskCloseMailStatus = True
					pmt.save()
			except:
				pass
			try:
				if(pmt.prNumber != data["prNumber"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "PR Number"
					t.oldValue = pmt.prNumber
					t.newValue = data["prNumber"]
					t.save()
				pmt.prNumber = data["prNumber"]
			except:
				pass
			try:
				if(pmt.poNumber != data["poNumber"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "PO Number"
					t.oldValue = pmt.poNumber
					t.newValue = data["poNumber"]
					t.save()
				pmt.poNumber = data["poNumber"]
			except:
				pass
			try:
				if(pmt.remark != data["remark"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Remark"
					t.oldValue = pmt.remark
					t.newValue = data["remark"]
					t.save()
				pmt.remark = data["remark"]
			except:
				pass
			try:
				if(pmt.plannedNos != float(data["plannedNos"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Planned Nos"
					t.oldValue = pmt.plannedNos
					t.newValue = data["plannedNos"]
					t.save()
				pmt.plannedNos = data["plannedNos"]
			except:
				pass
			try:
				if(pmt.actualNos != float(data["actualNos"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Actual Nos"
					t.oldValue = pmt.actualNos
					t.newValue = data["actualNos"]
					t.save()
				pmt.actualNos = data["actualNos"]
			except:
				pass
			try:
				if(pmt.buildingDescription != data["buildingDescription"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Building Description"
					t.oldValue = pmt.buildingDescription
					t.newValue = data["buildingDescription"]
					t.save()
				pmt.buildingDescription = data["buildingDescription"]
			except:
				pass
			try:
				if(pmt.healthCheck != data["healthCheck"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Health Check"
					t.oldValue = pmt.healthCheck
					t.newValue = data["healthCheck"]
					t.save()
					pmt.overrideHealthCheck = True
				pmt.healthCheck = data["healthCheck"]
			except:
				pass
			try:
				if(pmt.RAGReason != data["RAGReason"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "RAG Reason"
					t.oldValue = pmt.RAGReason
					t.newValue = data["RAGReason"]
					t.save()
				pmt.RAGReason = data["RAGReason"]
			except:
				pass
			try:
				if(pmt.uom_id != data["uom"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "UOM"
					try:
						t.oldValue = pmt.uom.name
					except:
						t.oldValue = ""
					t.save()
					pmt.uom_id = data["uom"]
					t.newValue = pmt.uom.name
					t.save()
			except:
				pass
			try:
				if(data["postToDashboard"]=="true" or data["postToDashboard"]==True):
					pmt.postToDashboard = True
				else:
					pmt.postToDashboard = False
			except:
				pass
			try:
				if(data["ShowToGraph"]=="true" or data["ShowToGraph"]==True):
					if(pmt.ShowToGraph!=True):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "ShowToGraph"
						t.oldValue = False
						t.save()
						t.newValue = True
						t.save()
						pmt.ShowToGraph = True
				else:
					if(pmt.ShowToGraph!=False):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "ShowToGraph"
						t.oldValue = True
						t.save()
						t.newValue = False
						t.save()
						pmt.ShowToGraph = False
			except:
				pass
			try:
				if(data["showNumbers"]=="true" or data["showNumbers"]==True):
					pmt.showNumbers = True
				else:
					pmt.showNumbers = False
			except:
				pass
			try:
				if(data["measureByDate"]=="true" or data["measureByDate"]==True):
					pmt.measureByDate = True
				else:
					pmt.measureByDate = False
			except:
				pass
			try:
				if(data["measureByNumbers"]=="true" or data["measureByNumbers"]==True):
					pmt.measureByNumbers = True
				else:
					pmt.measureByNumbers = False
			except:
				pass
			try:
				if(pmt.equipment_id != data["equipment"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Equipment"
					try:
						t.oldValue = pmt.equipment.name
					except:
						t.oldValue = ""
					t.save()
					pmt.equipment_id = data["equipment"]
					t.newValue = pmt.equipment.name
					t.save()
			except:
				pass
			try:
				if(pmt.system_id != data["system"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "System"
					try:
						t.oldValue = pmt.system.name
					except:
						t.oldValue = ""
					t.save()
					pmt.system_id = data["system"]
					t.newValue = pmt.system.name
					t.save()
			except:
				pass
			try:
				
				if(pmt.taskCategory_id != data["taskCategory"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Task Category"
					try:
						t.oldValue = pmt.taskCategory.name
					except:
						t.oldValue = ""
					t.save()
					pmt.taskCategory_id = data["taskCategory"]
					t.newValue = pmt.taskCategory.name
					t.save()
			except:
				pass
			try:
				if(pmt.subsystem_id != data["subsystem"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Subsystem"
					try:
						t.oldValue = pmt.subsystem.name
					except:
						t.oldValue = ""
					t.save()
					pmt.subsystem_id = data["subsystem"]
					t.newValue = pmt.subsystem.name
					t.save()
			except:
				pass
			try:
				if(pmt.complianceName != data["complianceName"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Compliance Name"
					t.oldValue = pmt.complianceName
					t.newValue = data["complianceName"]
					t.save()
				pmt.complianceName = data["complianceName"]
			except:
				pass
			try:
				if(pmt.complianceCategory_id != data["complianceCategory"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Compliance Category"
					try:
						t.oldValue = pmt.complianceCategory.name
					except:
						t.oldValue = ""
					t.save()
					pmt.complianceCategory_id = data["complianceCategory"]
					t.newValue = pmt.complianceCategory.name
					t.save()
			except:
				pass
			try:
				if(pmt.shipmentLocation_id != data["shipmentLocation"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Shipment Location"
					try:
						t.oldValue = pmt.shipmentLocation.name
					except:
						t.oldValue = ""
					t.save()
					pmt.shipmentLocation_id = data["shipmentLocation"]
					t.newValue = pmt.shipmentLocation.name
					t.save()
			except:
				pass
			try:
				if(pmt.cib != data["cib"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "CIB"
					t.oldValue = pmt.cib
					t.newValue = data["cib"]
					t.save()
				pmt.cib = data["cib"]
			except:
				pass
			try:
				if(pmt.responsibility != data["responsibility"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "responsibility"
					t.oldValue = pmt.responsibility
					t.newValue = data["responsibility"]
					t.save()
				pmt.responsibility = data["responsibility"]
			except:
				pass
			try:
				if(pmt.area != data["area"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "area"
					t.oldValue = pmt.area
					t.newValue = data["area"]
					t.save()
				pmt.area = data["area"]
			except:
				pass
			try:
				if(pmt.severity_id != data["severity"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Severity"
					try:
						t.oldValue = pmt.severity.name
					except:
						t.oldValue = ""
					t.save()
					pmt.severity_id = data["severity"]
					t.newValue = pmt.severity.name
					t.save()
				pmt.severity_id = data["severity"]
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.postedDate) != (data["postedDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Posted Date"
					t.oldValue = getDateStringfromDate(pmt.postedDate)
					t.newValue = data["postedDate"]
					t.save()
				pmt.postedDate = getDatefromDateString(data["postedDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.plannedDespatchClearanceDate) != (data["plannedDespatchClearanceDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "planned Despatch Clearance Date"
					t.oldValue = getDateStringfromDate(pmt.plannedDespatchClearanceDate)
					t.newValue = data["plannedDespatchClearanceDate"]
					t.save()
				pmt.plannedDespatchClearanceDate = getDatefromDateString(data["plannedDespatchClearanceDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.actualDespatchClearanceDate) != (data["actualDespatchClearanceDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "actual Despatch Clearance Date"
					t.oldValue = getDateStringfromDate(pmt.actualDespatchClearanceDate)
					t.newValue = data["actualDespatchClearanceDate"]
					t.save()
				pmt.actualDespatchClearanceDate = getDatefromDateString(data["actualDespatchClearanceDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.equipmentDeliveryAtSite) != (data["equipmentDeliveryAtSite"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "equipment Delivery At Site"
					t.oldValue = getDateStringfromDate(pmt.equipmentDeliveryAtSite)
					t.newValue = data["equipmentDeliveryAtSite"]
					t.save()
				pmt.equipmentDeliveryAtSite = getDatefromDateString(data["equipmentDeliveryAtSite"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.closedDate) != (data["closedDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Closed Date"
					t.oldValue = getDateStringfromDate(pmt.closedDate)
					t.newValue = data["closedDate"]
					t.save()
				pmt.closedDate = getDatefromDateString(data["closedDate"])
			except:
				pass
			try:
				if(pmt.status != data["status"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Status"
					t.oldValue = pmt.status
					t.newValue = data["status"]
					t.save()
				pmt.status = data["status"]
			except:
				pass
			try:
				if(pmt.manufacturingLeadTime != data["manufacturingLeadTime"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Manufacturing Lead Time"
					t.oldValue = pmt.manufacturingLeadTime
					t.newValue = data["manufacturingLeadTime"]
					t.save()
				pmt.manufacturingLeadTime = data["manufacturingLeadTime"]
			except:
				pass
			try:
				if(pmt.packingAndShipment != data["packingAndShipment"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Packing And Shipment"
					t.oldValue = pmt.packingAndShipment
					t.newValue = data["packingAndShipment"]
					t.save()
				pmt.packingAndShipment = data["packingAndShipment"]
			except:
				pass
			try:
				if(pmt.shipmentDuration != data["shipmentDuration"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "shipmentDuration"
					t.oldValue = pmt.shipmentDuration
					t.newValue = data["shipmentDuration"]
					t.save()
				pmt.shipmentDuration = data["shipmentDuration"]
			except:
				pass
			try:
				if(pmt.customsClearances != data["customsClearances"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Customs Clearances"
					t.oldValue = pmt.customsClearances
					t.newValue = data["customsClearances"]
					t.save()
				pmt.customsClearances = data["customsClearances"]
			except:
				pass
			try:
				if(pmt.landTransportation != data["landTransportation"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Land Transportation"
					t.oldValue = pmt.landTransportation
					t.newValue = data["landTransportation"]
					t.save()
				pmt.landTransportation = data["landTransportation"]
			except:
				pass
			try:
				if(pmt.risk != data["risk"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Risk"
					t.oldValue = pmt.risk
					t.newValue = data["risk"]
					t.save()
				pmt.risk = data["risk"]
			except:
				pass
			try:
				if(pmt.migrationPlan != data["migrationPlan"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "migrationPlan"
					t.oldValue = pmt.migrationPlan
					t.newValue = data["migrationPlan"]
					t.save()
				pmt.migrationPlan = data["migrationPlan"]
			except:
				pass
			try:
				pmt.plannedDuration = data["plannedDuration"]
			except:
				pass
			try:
				pmt.actualDuration = data["actualDuration"]
			except:
				pass
			try:
				if(pmt.prerequisites != data["prerequisites"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "prerequisites"
					t.oldValue = pmt.prerequisites
					t.newValue = data["prerequisites"]
					t.save()
				pmt.prerequisites = data["prerequisites"]
			except:
				pass
			try:
				if(pmt.approvalauthority != data["approvalauthority"]):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "approvalauthority"
					t.oldValue = pmt.approvalauthority
					t.newValue = data["approvalauthority"]
					t.save()
				pmt.approvalauthority = data["approvalauthority"]
			except:
				pass
			pmt.save()
			try:
				ccount = int(data["attachmentCount"])
				while ccount>0:
					pmtatt = PMTAttachments()
					cnt = str(ccount)
					pmtatt.attachment = data["file"+cnt]
					if(data["pushToGallary"+cnt]=="true" or data["pushToGallary"+cnt]==True):
						pmtatt.pushToGallary = True
					else:
						pmtatt.pushToGallary = False
					pmtatt.pmt_id = pmt.uId
					pmtatt.remark = data["attachmentRemark"+cnt]
					pmtatt.date = getDatefromDateString(data["date"+cnt])
					pmtatt.save()
					ccount = ccount-1
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Added Attachment"
					# t.oldValue = pmt.migrationPlan
					t.newValue = pmtatt.attachmentname
					t.save()
			except Exception as e:
				pass
			for cf in json.loads(data["customFields"]):
				try:
					tcf = taskCustomFields.objects.get(id = cf["tcfId"])
				except:
					tcf = taskCustomFields()
				tcf.field_id = cf["field"]
				tcf.task_id = pmt.uId
				tcf.value = cf["value"]
				tcf.options = cf["options"]
				tcf.fieldname = cf["fieldname"]
				tcf.save()
				t = TaskHistory()
				t.user_id = request.user.id
				t.pmt_id = data["uId"]
				t.field =  "Custom Field Added: "+cf["fieldname"]
				# t.oldValue = pmt.migrationPlan
				t.newValue = cf["value"]
				t.save()
			loop.run_in_executor(None,sendMailFunction,"Task Edit",pmt.uId)
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		
		elif(data["action"]=="deleteAttachment"):
			pmta = PMTAttachments.objects.get(id = data["id"])
			pmta.isActive = False
			pmta.save()
			t = TaskHistory()
			t.user_id = request.user.id
			t.pmt_id = pmta.pmt_id
			t.field =  "Attachment Deleted"
			# t.oldValue = pmt.migrationPlan
			t.newValue = pmta.attachmentname
			t.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		
		elif(data["action"]=="editAttachment"):
			pmta = PMTAttachments.objects.get(id = data["id"])
			if(pmta.pushToGallary == True):
				oldval = "Y"
			else:
				oldval = "N"
			if(data["pushToGallary"]=="true" or data["pushToGallary"]==True):
				pmta.pushToGallary = True
				val = "Y"
			else:
				pmta.pushToGallary = False
				val = "N"
			pmta.save()
			if(oldval!=val):
				t = TaskHistory()
				t.user_id = request.user.id
				t.pmt_id = pmta.pmt_id
				t.field =  "Attachment: pushToGallary"
				t.oldValue = oldval
				t.newValue = val
				t.save()
			ndate = data["date"]
			pdate = pmta.date
			if(getDateStringfromDate(pdate) != ndate):
				t = TaskHistory()
				t.user_id = request.user.id
				t.pmt_id = pmta.pmt_id
				t.field = "Attachment Date"
				t.oldValue = getDateStringfromDate(pdate)
				t.newValue = ndate
				t.save()
			pmta.date = getDatefromDateString(ndate)
			pmta.save()
			if(pmta.remark != data["attachmentRemark"]):
				t = TaskHistory()
				t.user_id = request.user.id
				t.pmt_id = pmta.pmt_id
				t.field = "Attachment Remark"
				t.oldValue = pmta.remark
				t.newValue = data["attachmentRemark"]
				t.save()
			pmta.remark = data["attachmentRemark"]
			pmta.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		
		elif(data["action"]=="viewById"):
			projectmilestone = ProjectMilestone.objects.filter(project_id = data['projectId'],isActive = True).order_by("id")
			milestonelist = []
			statuslist = []
			tstatus = TaskStatus.objects.all()

			for y in tstatus:
				statuslist.append({'uId':y.uId,'name':y.name})

			for x in projectmilestone:
				milestonelist.append({'uId':x.uId,'milestone':x.milestoneName})
			pmt = ProjectMilestoneTask.objects.get(uId = data["uId"])
			obj = {
				"project":pmt.project_id,
				"milestone":pmt.milestone_id,
				"taskname" : pmt.taskname,
				"uId":pmt.uId,
				# "workType" : pmt.workType_id,
				# "building" : pmt.building_id,
				# "subbuilding" : pmt.subbuilding_id,
				# "taskstatus":pmt.taskstatus_id
				"planstartdate" : getDateStringfromDate(pmt.plannedStartDate),
				"planenddate" : getDateStringfromDate(pmt.plannedEndDate),
				"actualstartdate" : getDateStringfromDate(pmt.actualStartDate),
				"actualenddate" : getDateStringfromDate(pmt.actualEndDate),
				"completionPercentage": pmt.completionPercentage,
				# "prNumber" : pmt.prNumber,
				# "poNumber" : pmt.poNumber,
				# "attachment" : pmt.attachment,
				"remark" : pmt.remark,
				# "plannedNos" : pmt.plannedNos,
				# "actualNos" : pmt.actualNos,
				# "buildingDescription" : pmt.buildingDescription,
				# "healthCheck" : pmt.healthCheck,
				# "RAGReason" : pmt.RAGReason,
				"uom" : pmt.uom_id,
				# "postToDashboard" : pmt.postToDashboard,
				# "equipment" : pmt.equipment_id,
				# "system" : pmt.system_id,
				# "subsystem" : pmt.subsystem_id,
				# "complianceName" : pmt.complianceName,
				# "complianceCategory" : pmt.complianceCategory_id,
				# "cib" : pmt.cib,
				# "severity" : pmt.severity_id,
				"plannedDuration" : pmt.plannedDuration,
				"actualDuration" : pmt.actualDuration,
				"postedDate" : getDateStringfromDate(pmt.postedDate),
				"closedDate" : getDateStringfromDate(pmt.closedDate),
				"taskstatus" : pmt.taskstatus_id,
				# "taskowner" : pmt.taskowner_id,
				# "risk" : pmt.risk,
				# "migrationPlan" : pmt.migrationPlan
			}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj,"milestone":milestonelist,"tstatus":statuslist}),content_type="application/json")
		if(data["action"]=="search"):
			# uId = data["uId"]
			# draw = request.POST['draw']
			# list = []
			# start = int(request.POST['start'])
			# length = int(request.POST['length'])
			# end = start + length
			# searchVal = request.POST['search[value]']
			# building = request.POST['building']
			# equipment = request.POST['equipment']
			# orderBy = request.POST['orderBy']
			# totalCount = ProjectMilestoneTask.objects.filter(milestone_id = uId,isActive=True).count()
			# ListDetails = ProjectMilestoneTask.objects.filter(milestone_id = uId,isActive=True).order_by(orderBy)
			# if(searchVal != ""):
			# 	ListDetails = ListDetails.filter(taskname__icontains=searchVal)
			# if(building!=""):
			# 	ListDetails = ListDetails.filter(building_id=building)
			# if(equipment!=""):
			# 	ListDetails = ListDetails.filter(equipment_id=equipment)
			# count = len(ListDetails)
			# totalTask = ListDetails.filter(isActive = True).count()
			# openTask = ListDetails.filter(isActive = True,completionPercentage__lt=100).count()
			# closeTask = ListDetails.filter(isActive = True,completionPercentage=100).count()
			# closeTaskSum = ListDetails.filter(isActive = True,completionPercentage__gt=0).aggregate(Sum('completionPercentage'))
			# compper = closeTaskSum["completionPercentage__sum"]
			# totalper = totalTask*100
			# try:
			# 	completionPercentage = (compper/totalper)*100
			# 	completionPercentage = round(completionPercentage)
			# except:
			# 	completionPercentage = 0
			# ListDetails = ListDetails[start:end]
			# for p in ListDetails:
			# 	urlSafeEncodedBytes = base64.urlsafe_b64encode(str(p.uId).encode("utf-8"))
			# 	urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
			# 	if(p.overrideHealthCheck==False):
			# 		rag = calculateRag(p)
			# 		p.healthCheck = rag
			# 		p.save()
			# 	rag = p.healthCheck
			# 	if(p.milestone.milestoneName == "Statutory Approval"):
			# 		taskname = p.complianceName
			# 		plannedStartDate = getDateStringfromDate(p.plannedStartDate)
			# 		actualEndDate = getDateStringfromDate(p.actualEndDate)
			# 		actualStartDate = getDateStringfromDate(p.actualStartDate)
			# 		plannedEndDate = getDateStringfromDate(p.plannedEndDate)
			# 	elif(p.milestone.milestoneName == "Critical Issues Brochure"):
			# 		taskname = p.cib
			# 		actualStartDate = getDateStringfromDate(p.postedDate)
			# 		actualEndDate = getDateStringfromDate(p.closedDate)
			# 		plannedStartDate = ""
			# 		plannedEndDate = ""
			# 	else:
			# 		taskname = p.taskname
			# 		plannedStartDate = getDateStringfromDate(p.plannedStartDate)
			# 		actualEndDate = getDateStringfromDate(p.actualEndDate)
			# 		actualStartDate = getDateStringfromDate(p.actualStartDate)
			# 		plannedEndDate = getDateStringfromDate(p.plannedEndDate)
			# 	try:
			# 		equipment = p.equipment.name
			# 	except:
			# 		equipment = ""
			# 	try:
			# 		complianceCategory = p.complianceCategory.name
			# 	except:
			# 		complianceCategory = ""
			# 	if(p.remark == None):
			# 		remark = ""
			# 	else:
			# 		remark = p.remark
			# 	try:
			# 		severity = p.severity.name
			# 	except:
			# 		severity = ""
			# 	jsonObj = {
			# 		"complianceCategory":complianceCategory,
			# 		"isDefault":p.isDefault,
			# 		"remark":remark,
			# 		"equipment":equipment,
			# 		"workType":p.workType_id,
			# 		"actualDuration":p.actualDuration,
			# 		"plannedDuration":p.plannedDuration,
			# 		"rag":rag,
			# 		"plannedStartDate":plannedStartDate,
			# 		"completionPercentage":p.completionPercentage,
			# 		"actualEndDate":actualEndDate,
			# 		"actualStartDate":actualStartDate,
			# 		"plannedEndDate":plannedEndDate,
			# 		"encodedUid":urlSafeEncodedStr,
			# 		"uId":p.uId,
			# 		"milestone":p.milestone_id,
			# 		"task":taskname,
			# 		"severity":severity,
			# 		"area":p.area,
			# 		"responsibility":p.responsibility,
			# 	}#,"workType":p.workType.name,"building":p.building.name,"subbuilding":p.subbuilding.name})
			# 	list.append(jsonObj)
			list =  ProjectMilestoneTask.objects.all().order_by('-id')
			listitem = []
			for item in list:
				listitem.append({"uId":item.uId,"taskname":item.taskname})


			# return HttpResponse(json.dumps({"completionPercentage":completionPercentage,"closeTask":closeTask,"openTask":openTask,"totalTask":totalTask,"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","proList":listitem}),content_type="application/json")
		if(data["action"]=="searchByBuilding"):
			p = data["p"]
			b = data["b"]
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			milestone = request.POST['milestone']
			equipment = request.POST['equipment']
			orderBy = request.POST['orderBy']
			totalCount = ProjectMilestoneTask.objects.filter(project_id = p,building_id = b,isActive=True).count()
			ListDetails = ProjectMilestoneTask.objects.filter(project_id = p,building_id = b,isActive=True).order_by(orderBy)
			if(searchVal != ""):
				ListDetails = ListDetails.filter(taskname__icontains=searchVal)
			if(milestone!=""):
				ListDetails = ListDetails.filter(milestone_id=milestone)
			if(equipment!=""):
				ListDetails = ListDetails.filter(equipment_id=equipment)
			count = len(ListDetails)
			totalTask = ListDetails.filter(isActive = True).count()
			openTask = ListDetails.filter(isActive = True,completionPercentage__lt=100).count()
			closeTask = ListDetails.filter(isActive = True,completionPercentage=100).count()
			closeTaskSum = ListDetails.filter(isActive = True).aggregate(Sum('completionPercentage'))
			compper = closeTaskSum["completionPercentage__sum"]
			totalper = totalTask*100
			try:
				completionPercentage = (compper/totalper)*100
				completionPercentage = round(completionPercentage)
			except:
				completionPercentage = 0
			ListDetails = ListDetails[start:end]
			for p in ListDetails:
				urlSafeEncodedBytes = base64.urlsafe_b64encode(str(p.uId).encode("utf-8"))
				urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
				if(p.overrideHealthCheck==False):
					rag = calculateRag(p)
					p.healthCheck = rag
					p.save()
				rag = p.healthCheck
				if(p.milestone.milestoneName == "Statutory Approval"):
					taskname = p.complianceName
					plannedStartDate = getDateStringfromDate(p.plannedStartDate)
					actualEndDate = getDateStringfromDate(p.actualEndDate)
					actualStartDate = getDateStringfromDate(p.actualStartDate)
					plannedEndDate = getDateStringfromDate(p.plannedEndDate)
				elif(p.milestone.milestoneName == "Critical Issues Brochure"):
					taskname = p.cib
					actualStartDate = getDateStringfromDate(p.postedDate)
					actualEndDate = getDateStringfromDate(p.closedDate)
					plannedStartDate = ""
					plannedEndDate = ""
				else:
					taskname = p.taskname
					plannedStartDate = getDateStringfromDate(p.plannedStartDate)
					actualEndDate = getDateStringfromDate(p.actualEndDate)
					actualStartDate = getDateStringfromDate(p.actualStartDate)
					plannedEndDate = getDateStringfromDate(p.plannedEndDate)
				try:
					equipment = p.equipment.name
				except:
					equipment = ""
				list.append({"isDefault":p.isDefault,"equipment":equipment,"workType":p.workType_id,"actualDuration":p.actualDuration,"plannedDuration":p.plannedDuration,"rag":rag,"project":p.project.name,"building":p.building.code,"milestoneName":p.milestone.milestoneName,"plannedStartDate":plannedStartDate,"completionPercentage":p.completionPercentage,"actualEndDate":actualEndDate,"actualStartDate":actualStartDate,"plannedEndDate":plannedEndDate,"encodedUid":urlSafeEncodedStr,"uId":p.uId,"milestone":p.milestone_id,"task":taskname})#,"workType":p.workType.name,"building":p.building.name,"subbuilding":p.subbuilding.name})
			return HttpResponse(json.dumps({"completionPercentage":completionPercentage,"closeTask":closeTask,"openTask":openTask,"totalTask":totalTask,"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		if(data["action"] == "editFromList"):
			pmt = ProjectMilestoneTask.objects.get(uId = data["uId"])
			a = ""
			
			try:
				if(getDateStringfromDate(pmt.plannedStartDate) != (data["plannedStartDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Planned Start Date"
					t.oldValue = getDateStringfromDate(pmt.plannedStartDate)
					t.newValue = data["plannedStartDate"]
					t.save()
				pmt.plannedStartDate = getDatefromDateString(data["plannedStartDate"])
				# pmt.plannedStartDate = getDatefromDateString(data["plannedStartDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.plannedEndDate) != (data["plannedEndDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Planned End Date"
					t.oldValue = getDateStringfromDate(pmt.plannedEndDate)
					t.newValue = data["plannedEndDate"]
					t.save()
				pmt.plannedEndDate = getDatefromDateString(data["plannedEndDate"])
			except:
				pass
			if(pmt.milestone.milestoneName == "Critical Issues Brochure"):
				try:
					if(getDateStringfromDate(pmt.postedDate) != (data["actualStartDate"])):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "Posted Date"
						t.oldValue = getDateStringfromDate(pmt.postedDate)
						t.newValue = data["actualStartDate"]
						t.save()
					pmt.postedDate = getDatefromDateString(data["actualStartDate"])
				except:
					pass
				try:
					if(getDateStringfromDate(pmt.closedDate) != (data["actualEndDate"])):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "Closed Date"
						t.oldValue = getDateStringfromDate(pmt.closedDate)
						t.newValue = data["actualEndDate"]
						t.save()
					pmt.closedDate = getDatefromDateString(data["actualEndDate"])
				except Exception as e:
					a = str(e)
			else:
				try:
					if(getDateStringfromDate(pmt.actualStartDate) != (data["actualStartDate"])):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "Actual Start Date"
						t.oldValue = getDateStringfromDate(pmt.actualStartDate)
						t.newValue = data["actualStartDate"]
						t.save()
					pmt.actualStartDate = getDatefromDateString(data["actualStartDate"])
				except:
					pass
				try:
					if(getDateStringfromDate(pmt.actualEndDate) != (data["actualEndDate"])):
						t = TaskHistory()
						t.user_id = request.user.id
						t.pmt_id = data["uId"]
						t.field = "Actual End Date"
						t.oldValue = getDateStringfromDate(pmt.actualEndDate)
						t.newValue = data["actualEndDate"]
						t.save()
					pmt.actualEndDate = getDatefromDateString(data["actualEndDate"])
				except Exception as e:
					a = str(e)
			try:
				if(getDateStringfromDate(pmt.postedDate) != (data["postedDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Posted Date"
					t.oldValue = getDateStringfromDate(pmt.postedDate)
					t.newValue = data["postedDate"]
					t.save()
				pmt.postedDate = getDatefromDateString(data["postedDate"])
			except:
				pass
			try:
				if(getDateStringfromDate(pmt.closedDate) != (data["closedDate"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Closed Date"
					t.oldValue = getDateStringfromDate(pmt.closedDate)
					t.newValue = data["closedDate"]
					t.save()
				pmt.closedDate = getDatefromDateString(data["closedDate"])
			except:
				pass
			try:
				if(pmt.completionPercentage != float(data["completionPercentage"])):
					t = TaskHistory()
					t.user_id = request.user.id
					t.pmt_id = data["uId"]
					t.field = "Completion Percentage"
					t.oldValue = pmt.completionPercentage
					t.newValue = data["completionPercentage"]
					t.save()
				pmt.completionPercentage = data["completionPercentage"]
				if(pmt.completionPercentage<100):
					pmt.taskCloseMailStatus = False
					pmt.save()
				if(pmt.taskCloseMailStatus==False and pmt.completionPercentage==100):
					loop.run_in_executor(None,sendMailFunction,"Task Closed",pmt.uId)
					pmt.taskCloseMailStatus = True
					pmt.save()
			except Exception as e:
				pass
			try:
				pmt.plannedDuration = float(data["plannedDuration"])
			except Exception as e:
				pass
			try:
				pmt.actualDuration = float(data["actualDuration"])
			except Exception as e:
				pass
			pmt.save()
			loop.run_in_executor(None,sendMailFunction,"Task Edit",pmt.uId)
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","a":a}),content_type="application/json")
		if(data["action"] == "delete"):
			pmt = ProjectMilestoneTask.objects.get(uId = data["uId"])
			pmt.isActive = False
			pmt.save()
			pmta = PMTAttachments.objects.filter(pmt_id = data["uId"])
			for a in pmta:
				a.isActive = False
				a.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	# except Exception as e:
		# return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
@api_view(['POST'])
def getTaskHistory(request):
	try:
		data = request.data
		list = []
		thistory = TaskHistory.objects.filter(pmt_id = data["uId"])
		for t in thistory:
			if(t.field == "Task Added"):
				action = "Create"
				field = ""
				oldValue = ""
				newValue = ""
			else:
				action = "Edit"
				field = t.field
				oldValue = t.oldValue
				newValue = t.newValue
			username = t.user.first_name+" "+t.user.last_name
			timestamp = (t.timestamp).strftime("%d %b %Y %r")
			obj = {"action":action,"user":username,"field":field,"oldValue":oldValue,"newValue":newValue,"timestamp":timestamp}
			list.append(obj)
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","list":list}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")

@api_view(['POST'])		
def filterGallary(request):
	# try:
		data = request.data
		if(data["action"]=="search"):
			building = data["building"]
			order = data["order"]
			uId = data["project"]
			milestonefilter = data["milestonefilter"]
			project = Project.objects.get(uId=uId)
			if(order == "" or order == "Descending"):
				pmtl = PMTAttachments.objects.filter(Q(pmt__project_id=uId,pushToGallary=True,isActive=True) | Q(projectId_id=uId,pushToGallary=True,isActive=True)).order_by('-date')
			else:
				pmtl = PMTAttachments.objects.filter(pmt__project_id=uId,pushToGallary=True,isActive=True).order_by('date')
			
			if(building != ""):
				pmtl = pmtl.filter(pmt__building_id = building)
			if(milestonefilter != ""):
				pmtl = pmtl.filter(pmt__milestone_id = milestonefilter)
				
			attachmentList = []
			for aa in pmtl:
				if(aa.attachment=="" or aa.attachment=="NULL" or aa.attachment==None or aa.attachment=="undefined"):
					pass
				else:
					fileu = aa.attachment.url
					filepath=fileu.replace(settings.MEDIA_ROOT,"")
					fileurl=settings.SITENAME+filepath
					if(aa.remark == None):
						remark = ""
					else:
						remark = aa.remark
					date = getDateStringfromDate(aa.date)
					try:
						projectname = aa.pmt.project.name
					except:
						projectname = aa.projectId.name
					try:
						milestoneName = aa.pmt.milestone.milestoneName
					except:
						milestoneName = ""
					try:
						taskname = aa.pmt.taskname
					except:
						taskname = ""
					attachmentList.append({"id":aa.id,"date":date,"remark":remark,"project":projectname,"fileurl":fileurl,"name":aa.attachmentname,"milestone":milestoneName,"task":taskname})
			return HttpResponse(json.dumps({"status":"Success","responsecode":200,"attachmentList":attachmentList}),content_type="application/json")
		elif(data["action"]=="addImage"):
			pmtatt = PMTAttachments()
			pmtatt.attachment = data["file"]
			pmtatt.pushToGallary = True
			pmtatt.projectId_id = data["project"]
			pmtatt.remark = data["attachmentRemark"]
			pmtatt.date = getDatefromDateString(data["date"])
			pmtatt.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":200}),content_type="application/json")
		elif(data["action"]=="editImage"):
			pmtatt = PMTAttachments.objects.get(id=data["id"])
			if(data["file"]!=None and data["file"]!="" and data["file"]!="Null"):
				pmtatt.attachment = data["file"]
			# pmtatt.pushToGallary = True
			# pmtatt.projectId_id = data["project"]
			pmtatt.remark = data["attachmentRemark"]
			pmtatt.date = getDatefromDateString(data["date"])
			pmtatt.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":200}),content_type="application/json")
		elif(data["action"]=="delete"):
			pmtatt = PMTAttachments.objects.get(id=data["id"])
			pmtatt.isActive = False
			pmtatt.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":200}),content_type="application/json")
	# except Exception as e:
		# return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		

		
		
@api_view(['POST'])
def calculateEndDate(request):
	try:
		data = request.data
		startDate = data["startDate"]
		duration = int(data["duration"])
		list = []
		if(startDate == "" or duration == "" ):
			return HttpResponse(json.dumps({"status":"Fail","responsecode":500,"error":"Start date or duration is missing"}),content_type="application/json")
		startDate = getDatefromDateString(startDate)
		sdate = startDate
		while(duration  > 0):
			list.append({"duration":duration,"date":getDateStringfromDate(sdate)})
			sdate += timedelta(days=1)
			weekday = sdate.weekday()
			if weekday == 6: # sunday = 6
				pass
			elif(Holiday.objects.filter(date=sdate,isActive=True).count()>0):
				pass
			else:
				duration = duration-1
		return HttpResponse(json.dumps({"list":list,"status":"Success","responsecode":"200","date":getDateStringfromDate(sdate)}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	
	
@api_view(['POST'])
def calculateDuration(request):
	try:
		data = request.data
		startDate = data["startDate"]
		endDate = data["endDate"]
		if(startDate == "" or endDate == "" ):
			return HttpResponse(json.dumps({"finalduration":0,"status":"Success","responsecode":200}),content_type="application/json")
		startDate = getDatefromDateString(startDate)
		endDate = getDatefromDateString(endDate)
		duration = 0
		weekends = 0
		hdays = 0
		skipHoliday = 0
		sdate = startDate
		# hdays = Holiday.objects.filter(date__range=[startDate,endDate],isActive=True).distinct('date').count()
		# holidays = Holiday.objects.filter(date__range=[startDate,endDate],isActive=True).distinct('date')
		holiday = 0
		while(endDate >= sdate ):
			skipFlag = False
			weekday = sdate.weekday()
			if weekday == 6: # sunday = 6
				weekends = weekends+1
				if(Holiday.objects.filter(date=sdate,isActive=True).count()>0):
					skipHoliday = skipHoliday+1
					skipFlag = True
			elif(Holiday.objects.filter(date=sdate,isActive=True).count()>0 and skipFlag==False):
				holiday = holiday+1
			else:
				duration = duration+1
			sdate += timedelta(days=1)
		finalduration = duration#+ skipHoliday - holiday	
		return HttpResponse(json.dumps({"finalduration":finalduration,"status":"Success","responsecode":200,"skipHoliday":skipHoliday,"weekends":weekends,"holiday":holiday,"duration":duration}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
def calculateDurationFunction(startDate,endDate):
	try:
		if(startDate == "" or endDate == "" or startDate == None or endDate == None):
			return 0
		startDate = getDatefromDateString(startDate)
		endDate = getDatefromDateString(endDate)
		duration = 0
		weekends = 0
		hdays = 0
		skipHoliday = 0
		sdate = startDate
		# hdays = Holiday.objects.filter(date__range=[startDate,endDate],isActive=True).distinct('date').count()
		# holidays = Holiday.objects.filter(date__range=[startDate,endDate],isActive=True).distinct('date')
		holiday = 0
		while(endDate >= sdate ):
			skipFlag = False
			weekday = sdate.weekday()
			if weekday == 6: # sunday = 6
				weekends = weekends+1
				if(Holiday.objects.filter(date=sdate,isActive=True).count()>0):
					skipHoliday = skipHoliday+1
					skipFlag = True
			elif(Holiday.objects.filter(date=sdate,isActive=True).count()>0 and skipFlag==False):
				holiday = holiday+1
			else:
				duration = duration+1
			sdate += timedelta(days=1)
		finalduration = duration# + skipHoliday - holiday
		return finalduration
	except Exception as e:
		return str(e)
		
def calculateRag(p):
	rag = "red"
	if(p.plannedStartDate != "" and p.plannedStartDate != "Null" and p.plannedStartDate != None):
		if(p.actualStartDate == "" or p.actualStartDate == "Null" or p.actualStartDate == None):
			today = date.today()
			taskDuration1 = today - p.plannedStartDate
			taskDuration = taskDuration1.days
		else:
			taskDuration1 = p.actualStartDate - p.plannedStartDate
			taskDuration = taskDuration1.days
	elif(p.postedDate != "" and p.postedDate != "Null" and p.postedDate != None):
		if(p.closedDate == "" or p.closedDate == "Null" or p.closedDate == None):
			today = date.today()
			taskDuration1 = today - p.postedDate
			taskDuration = taskDuration1.days
		else:
			taskDuration1 = p.closedDate - p.postedDate
			taskDuration = taskDuration1.days
	else:
		rag = "white"
		taskDuration = 0
	months = 0
	if(rag != "white"):
		if(p.project.red=="" or p.project.red == "Null" or p.project.red == None):
			difference = relativedelta.relativedelta(p.plannedEndDate, p.plannedStartDate)
			difference = relativedelta.relativedelta(p.project.endDate, p.project.startDate)
			months = difference.months
			gt = GlobalRAGThreshold.objects.filter(startMonth__lte=months,endMonth__gte=months,days__gte=taskDuration).order_by('days')
			for g in gt:
				rag = g.color
				break
		else:
			if(taskDuration <= p.project.green):
				rag = "green"
			elif(taskDuration <= p.project.yellow):
				rag = "yellow"
			elif(taskDuration <= p.project.amber):
				rag = "amber"
			else:
				rag = "red"
		if(taskDuration<0):
			rag = "white"
	# if(p.completionPercentage==100):
		# rag = "green"
	# p.healthCheck = rag
	# p.save()
	return rag
	
@api_view(['GET'])
def getTodaysDate(request):
	try:
		today = getDateStringfromDate(date.today())
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","date":today}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		

def getDataForActualVsPlannedNosLineChart(pId,pmId,bId):
	try:
		chartData = []
		pmt = TaskCategory.objects.filter(isActive=True)
		pmt = ProjectMilestoneTask.objects.filter(ShowToGraph=True,building_id =bId,milestone_id = pmId,isActive=True)
		for p in pmt:
			try:
				pmtCategory = p
				plannedDuration = pmtCategory.plannedDuration
				dbplannedNos = pmtCategory.plannedNos
				plannedNos = pmtCategory.plannedNos
				scopePerDay = plannedNos/plannedDuration
				todaysDate1 = date.today()
				todaysDate1 -= timedelta(days=1)
				todaysDate = todaysDate1.strftime("%d/%m/%Y")
				actualDuration = 0
				flag = True
				scopeDuration = 0
				if(pmtCategory.actualEndDate!="Null" and pmtCategory.actualEndDate!="" and pmtCategory.actualEndDate!="null" and pmtCategory.actualEndDate!=None):
					if(todaysDate1>pmtCategory.actualEndDate):
						plannedStartDate = getDateStringfromDate(pmtCategory.plannedStartDate)
						actualStartDate = getDateStringfromDate(pmtCategory.actualStartDate)
						actualEndDate = getDateStringfromDate(pmtCategory.actualEndDate)
						actualDuration = pmtCategory.actualDuration#calculateDurationFunction(actualStartDate,actualEndDate)
						scopeDuration = calculateDurationFunction(actualStartDate,actualEndDate)
						flag = False
					else:
						actualStartDate = getDateStringfromDate(pmtCategory.actualStartDate)
						actualDuration = calculateDurationFunction(actualStartDate,todaysDate)
				else:
					actualStartDate = getDateStringfromDate(pmtCategory.actualStartDate)
					actualDuration = calculateDurationFunction(actualStartDate,todaysDate)
				plannedStartDate = getDateStringfromDate(pmtCategory.plannedStartDate)
				if(flag==False):
					scopeDuration = scopeDuration
				else:
					scopeDuration = calculateDurationFunction(plannedStartDate,todaysDate)
				plannedNos = round(actualDuration*scopePerDay)
				actualNos = pmtCategory.actualNos
				scope = round(scopeDuration*scopePerDay)
			except Exception as e:
				scope = 0
				plannedNos = 0
				actualNos = 0
				scopeDuration = 0
			categories = []
			# if(plannedNos>0 or actualNos>0 ):
			categories.append({"Name":"Scope","Value":dbplannedNos})
			categories.append({"Name":"Planned Nos ","Value":scope})
			categories.append({"Name":"Actual Nos ","Value":actualNos})
			lineCategory = [{"Name":"Scope","Value":0}]
			try:
				name = p.taskname+"-"+p.equipment.name
			except:
				name = p.taskname
			chartData.append({"scopeDuration":scopeDuration,"task":name,"Categories":categories,"LineCategory":lineCategory})
		return chartData
	except Exception as e:
		return str(e)
		
#------------------------ get data for building wise and milestone wise chart-------------
@api_view(['POST'])	
def milestonewiseChartData(request):
	try:
		data = request.data
		projectId = data["project"]
		milestone = data["milestone"]
		project = Project.objects.get(uId=projectId)
		dataList = []
		bpm = BuildingProjectMapping.objects.filter(project_id = projectId,isActive=True).distinct('building_id')
		try:
			milestonedetails = ProjectMilestoneTask.objects.filter(milestone_id = milestone)[0]
			milestoneName = milestonedetails.milestone.milestoneName
		except:
			milestoneName = ""
		for b in bpm:
			chartData = getDataForActualVsPlannedNosLineChart(projectId,milestone,b.building_id)
			dataList.append({"buildingId":b.building_id,"building":b.building.code,"milestone":milestoneName,"chartData":chartData})
		return HttpResponse(json.dumps({"status":"success","responsecode":"200","dataList":dataList}), content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"fail","responsecode":"500","error":str(e)}), content_type='application/json')
		
	
@api_view(['POST'])
def getPONumberAndDate(request):
	try:
		data = request.data
		taskname = data["taskname"]
		equipment = data["equipment"]
		pmid = data["pmId"]
		poDate = ""
		poNumber = ""
		if(taskname == "Equipment - Delivery at site"):
			try:
				pmt = ProjectMilestoneTask.objects.filter(taskname="PO Status",equipment_id=equipment,milestone_id=pmid)[0]
				poNumber = pmt.poNumber
				poDate = getDateStringfromDate(pmt.actualEndDate)
			except:
				poDate = ""
				poNumber = ""
		return HttpResponse(json.dumps({"status":"success","responsecode":"200","poDate":poDate,"poNumber":poNumber}), content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"fail","responsecode":"500","error":str(e)}), content_type='application/json')


@api_view(['POST'])
def getDataForGanttChart(request):
	# try:
		data = request.data
		project = data["project"]
		myData = []
		milestones = ProjectMilestone.objects.filter(project_id = project,isActive=True).exclude(startDate__isnull=True).order_by('id')
		# i = 0
		colorcodes = ["ganttOrange","ganttBlue","ganttGreen","ganttRed"]
		for m in milestones:
			if(m.endDate!="" and m.endDate!="Null" and m.endDate!=None):
				endDate = m.endDate
			else:
				endDate = date.today()
			startDate = m.startDate
			
			my_time1 = datetime.min.time()
			my_time2 = datetime.max.time()
			startDate_datetime = datetime.combine(startDate, my_time1)
			endDate_datetime = datetime.combine(endDate, my_time1)
			milliseconds1 = int(datetime.timestamp(startDate_datetime))*1000
			# milliseconds1 = "/Date("+str(milliseconds1)+")/"
			milliseconds2 = int(datetime.timestamp(endDate_datetime))*1000
			# milliseconds2 = "/Date("+str(milliseconds2)+")/"
			# values = [{"from":milliseconds1,"to":milliseconds2,"label":m.milestoneName,"customClass":colorcodes[i]}]
			# i = i+1
			# if(i == len(colorcodes)):
			# 	i = 0
			myData.append([m.uId,m.milestoneName,None,milliseconds1,milliseconds2,None,m.completionPercentage,None])
			# myData.append([m.uId,m.milestoneName,m.uId,milliseconds1,milliseconds2,None,m.completionPercentage,None])
			# myData.append({"my_time1":str(my_time1),"my_time2":str(my_time2),"name":m.milestoneName,"desc":"","values":values})
		return HttpResponse(json.dumps({"status":"success","responsecode":"200","myData":myData}),content_type="application/json")
	# except Exception as e:
		# return HttpResponse(json.dumps({"status":"fail","responsecode":"500","error":str(e)}),content_type="application/json")

# @api_view(['POST'])
# def getDataForGanttChart(request):
	# # try:
		# data = request.data
		# project = data["project"]
		# myData = []
		# milestones = ProjectMilestone.objects.filter(project_id = project,isActive=True).order_by('id')
		# i = 0
		# colorcodes = ["ganttOrange","ganttBlue","ganttGreen","ganttRed"]
		
		# for m in milestones:
			# tasks = ProjectMilestoneTask.objects.filter(milestone_id = m.uId,isActive = True).exclude(actualStartDate__isnull=True)
			# flag = "First"
			# for t in tasks:
				# if(t.actualEndDate!="" and t.actualEndDate!="Null" and t.actualEndDate!=None):
					# endDate = t.actualEndDate
				# else:
					# endDate = date.today()
				# startDate = t.actualStartDate
				
				# my_time1 = datetime.min.time()
				# my_time2 = datetime.max.time()
				# startDate_datetime = datetime.combine(startDate, my_time1)
				# endDate_datetime = datetime.combine(endDate, my_time1)
				# milliseconds1 = int(datetime.timestamp(startDate_datetime))*1000
				# milliseconds1 = "/Date("+str(milliseconds1)+")/"
				# milliseconds2 = int(datetime.timestamp(endDate_datetime))*1000
				# milliseconds2 = "/Date("+str(milliseconds2)+")/"
				# values = [{"from":milliseconds1,"to":milliseconds2,"label":t.taskname,"customClass":colorcodes[i]}]
				# i = i+1
				# if(i == len(colorcodes)):
					# i = 0
				# if(flag == "First"):
					# milestonename = m.milestoneName
					# flag = "second"
				# else:
					# milestonename = ""
				# myData.append({"name":milestonename,"desc":t.taskname,"values":values})
		# return HttpResponse(json.dumps({"status":"success","responsecode":"200","myData":myData}),content_type="application/json")
	# # except Exception as e:
		# # return HttpResponse(json.dumps({"status":"fail","responsecode":"500","error":str(e)}),content_type="application/json")

		
def budgetAndCost(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	dept = department.objects.filter(isActive=True)
	cur = Currency.objects.filter(isActive=True)
	# totalAllocatedDetails = ProjectBudget.objects.filter(project_id=uId1,isActive=True).aggregate(Sum('allocated'))
	# totalActualDetails = ProjectBudget.objects.filter(project_id=uId1,isActive=True).aggregate(Sum('incurance'))
	# totalAllocated = totalAllocatedDetails["allocated__sum"]
	# totalActual = totalActualDetails["incurance__sum"]
	# symbol = ""
	# try:
		# symbol = project.currency.symbol
	# except Exception as e:
		# symbol = ""
	# totalAllocated = str(totalAllocated)+" "+symbol
	# totalActual = str(totalActual)+" "+symbol
	
	data = {"uId":uId,"project":project,"dept":dept,"cur":cur}
	return render(request,'admin_templates/project/budgetAndCost.html',{"data":data})
	
#---------------Business unit Api---------
@api_view(['POST'])
def ProjectBudgetApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = ProjectBudget.objects.get(uId=data["uId"])
			except:
				details = ProjectBudget()
			details.project_id = data["project"]
			details.dept_id = data["dept"]
			details.allocated = data["allocated"]
			details.incurance = data["incurance"]
			details.system_id = data["system"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = ProjectBudget.objects.get(uId=uId)
			systemList = []
			systems = BudgetSystem.objects.filter(dept_id = p.dept_id)
			for s in systems:
				systemList.append({"uId":s.uId,"name":s.name})
			obj = {"systemList":systemList,"uId":p.uId,"system":p.system_id,"project":p.project_id,"dept":p.dept_id,"allocated":p.allocated,"incurance":p.incurance}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="search"):
			project = data["project"]
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = 5#ProjectBudget.objects.filter(project_id=project,isActive=True).distinct('dept_id').count()
			# ListDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).distinct('dept_id')
			# if(searchVal != ""):
				# ListDetails = ListDetails.filter(dept__name__icontains=searchVal)
			count = 5#len(ListDetails)
			# ListDetails = ListDetails[start:end]
			symbol = ""
			ListDetails = ["Civil","Mechanical","Electrical","Utility"]
			for item in ListDetails:
				symbol = ""
				symboldetails = ProjectBudget.objects.filter(project_id=project,isActive=True,dept__name=item)
				allocateddetails = ProjectBudget.objects.filter(project_id=project,isActive=True,dept__name=item).aggregate(Sum('allocated'))
				incurancedetails = ProjectBudget.objects.filter(project_id=project,isActive=True,dept__name=item).aggregate(Sum('incurance'))
				if(allocateddetails!=None):
					allocated = symbol+" "+str(allocateddetails["allocated__sum"])
				else:
					allocated = symbol+str(0)
				if(allocateddetails!=None):
					incurance = symbol+" "+str(incurancedetails["incurance__sum"])
				else:
					incurance = symbol+str(0)
				if(len(symboldetails)>0):
					urlSafeEncodedBytes = base64.urlsafe_b64encode(str(symboldetails[0].dept_id).encode("utf-8"))
					urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
					try:
						symbol = symboldetails[0].project.currency.symbol
					except:
						symbol = ""
					list.append({"urlSafeEncodedStr":urlSafeEncodedStr,"uId":item,"symbol":symbol,"dept":item,"allocated":allocated,"incurance":incurance})
			
			symbol = ""
			symboldetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=ListDetails)
			allocateddetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=ListDetails).aggregate(Sum('allocated'))
			incurancedetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=ListDetails).aggregate(Sum('incurance'))
			if(allocateddetails!=None):
				allocated = symbol+" "+str(allocateddetails["allocated__sum"])
			else:
				allocated = symbol+str(0)
			if(allocateddetails!=None):
				incurance = symbol+" "+str(incurancedetails["incurance__sum"])
			else:
				incurance = symbol+str(0)
			if(len(symboldetails)>0):
				# urlSafeEncodedBytes = base64.urlsafe_b64encode(str(symboldetails[0].dept_id).encode("utf-8"))
				# urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
				try:
					symbol = symboldetails[0].project.currency.symbol
				except:
					symbol = ""
				list.append({"urlSafeEncodedStr":"abc","uId":item,"symbol":symbol,"dept":"Other","allocated":allocated,"incurance":incurance})
			totalAllocatedDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).aggregate(Sum('allocated'))
			totalActualDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).aggregate(Sum('incurance'))
			totalAllocated = totalAllocatedDetails["allocated__sum"]
			totalActual = totalActualDetails["incurance__sum"]
			if(totalAllocated!=None):
				totalAllocated = symbol+" "+str(totalAllocated)
			else:
				totalAllocated = ""
			if(totalActual!=None):
				totalActual = symbol+" "+str(totalActual)
			else:
				totalActual = ""
			
			return HttpResponse(json.dumps({"totalAllocated":totalAllocated,"totalActual":totalActual,"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = ProjectBudget.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		elif(data["action"]=="getChartData"):
			uId = data["project"]
			# ListDetails = ProjectBudget.objects.filter(project_id=uId,isActive=True).distinct('dept_id')
			labels = []
			actualdata = []
			targetdata = []
			ListDetails = ["Civil","Mechanical","Electrical","Utility"]
			for l in ListDetails:	
				allocateddetails = ProjectBudget.objects.filter(project_id=uId,isActive=True,dept__name=l).aggregate(Sum('allocated'))
				incurancedetails = ProjectBudget.objects.filter(project_id=uId,isActive=True,dept__name=l).aggregate(Sum('incurance'))
				if(allocateddetails!=None):
					allocated = allocateddetails["allocated__sum"]
				else:
					allocated = 0
				if(allocateddetails!=None):
					incurance = incurancedetails["incurance__sum"]
				else:
					incurance = 0
				targetdata.append(allocated)
				actualdata.append(incurance)
				
				labels.append(l)
			allocateddetails = ProjectBudget.objects.filter(project_id=uId,isActive=True).exclude(dept__name__in=ListDetails).aggregate(Sum('allocated'))
			incurancedetails = ProjectBudget.objects.filter(project_id=uId,isActive=True).exclude(dept__name__in=ListDetails).aggregate(Sum('incurance'))
			if(allocateddetails!=None):
				allocated = allocateddetails["allocated__sum"]
			else:
				allocated = 0
			if(allocateddetails!=None):
				incurance = incurancedetails["incurance__sum"]
			else:
				incurance = 0
			actualdata.append(incurance)
			targetdata.append(allocated)
			labels.append("Other")
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","labels":labels,"actualdata":actualdata,"targetdata":targetdata}),content_type="application/json")
		elif(data["action"]=="deptSearch"):
			project = data["project"]
			dept = data["dept"]
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			otherList = ["Civil","Mechanical","Electrical","Utility"]
			if(dept!="abc"):
				totalCount = ProjectBudget.objects.filter(project_id=project,dept_id=dept,isActive=True).count()
				ListDetails = ProjectBudget.objects.filter(project_id=project,dept_id=dept,isActive=True).order_by('-id')
			else:
				totalCount = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=otherList).count()
				ListDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=otherList).order_by('-id')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(system__name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			symbol = ""
			for item in ListDetails:
				symbol = ""
				urlSafeEncodedBytes = base64.urlsafe_b64encode(str(item.dept_id).encode("utf-8"))
				urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
				try:
					symbol = item.project.currency.symbol
				except:
					symbol = ""
				allocated = symbol+" "+str(item.allocated)
				incurance = symbol+" "+str(item.incurance)
				list.append({"urlSafeEncodedStr":urlSafeEncodedStr,"uId":item.uId,"symbol":symbol,"system":item.system.name,"dept":item.dept.name,"allocated":allocated,"incurance":incurance})
			
			if(dept!="abc"):
				totalAllocatedDetails = ProjectBudget.objects.filter(project_id=project,dept_id=dept,isActive=True).aggregate(Sum('allocated'))
				totalActualDetails = ProjectBudget.objects.filter(project_id=project,dept_id=dept,isActive=True).aggregate(Sum('incurance'))
			else:
				totalAllocatedDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=otherList).aggregate(Sum('allocated'))
				totalActualDetails = ProjectBudget.objects.filter(project_id=project,isActive=True).exclude(dept__name__in=otherList).aggregate(Sum('incurance'))
			totalAllocated = totalAllocatedDetails["allocated__sum"]
			totalActual = totalActualDetails["incurance__sum"]
			if(totalAllocated!=None):
				totalAllocated = symbol+" "+str(totalAllocated)
			else:
				totalAllocated = ""
			if(totalActual!=None):
				totalActual = symbol+" "+str(totalActual)
			else:
				totalActual = ""
			return HttpResponse(json.dumps({"totalAllocated":totalAllocated,"totalActual":totalActual,"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="deptGetChartData"):
			uId = data["project"]
			dept = data["dept"]
			otherList = ["Civil","Mechanical","Electrical","Utility"]
			if(dept!="abc"):
				ListDetails = ProjectBudget.objects.filter(project_id=uId,dept_id=dept,isActive=True).order_by('id')
			else:
				ListDetails = ProjectBudget.objects.filter(project_id=uId,isActive=True).exclude(dept__name__in=otherList).order_by('id')
			labels = []
			actualdata = []
			targetdata = []
			for l in ListDetails:
				labels.append(l.system.name)
				actualdata.append(l.incurance)
				targetdata.append(l.allocated)
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","labels":labels,"actualdata":actualdata,"targetdata":targetdata}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
def ganttChart(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	project = Project.objects.get(uId=uId1)
	data = {"uId":uId,"project":project}
	return render(request,'admin_templates/project/ganttView.html',{"data":data})
	
def departmentwiseBudget(request,uId,proId):
	if(uId!="abc"):
		uId1 = base64.b64decode(uId).decode('utf-8')
	else:
		uId1 = "abc"
	proId1 = base64.b64decode(proId).decode('utf-8')
	urlSafeEncodedBytes = base64.urlsafe_b64encode(str(proId1).encode("utf-8"))
	dept = department.objects.filter(isActive=True)
	uId = str(urlSafeEncodedBytes, "utf-8")
	data = {"deptId":uId1,"project":proId1,"uId":uId,"dept":dept}
	return render(request,'admin_templates/project/departmentwiseBudget.html',{"data":data})