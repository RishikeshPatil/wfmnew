# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
import json
from django.contrib.auth import logout,authenticate, login
from django.contrib.auth import models
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
import string
# from apollodev.settings import DEBUG
# from rest_framework_jwt.utils import jwt_payload_handler
# from rest_framework_jwt.utils import jwt_response_payload_handler
# from rest_framework_jwt.utils import jwt_encode_handler
import random
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core import serializers
from django.apps import apps
import re
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login
from rest_framework.generics import CreateAPIView,ListAPIView,RetrieveAPIView
from base64 import b64decode
from django.core.files.base import ContentFile
from rest_framework.settings import api_settings
from django.contrib.auth import authenticate
from django.conf import settings
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import django
import datetime
import ast
from datetime import datetime,timedelta
from django.contrib import messages
import os
from django.utils.crypto import get_random_string
from email.header  import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
import smtplib
from getpass  import getpass
import smtplib
import copy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.db.models import Q
from django.db.models import Sum
import calendar
from io import BytesIO
import xlsxwriter
import pyexcel
import xlwt
from master.models import country,State,City,role,UOM,TaskCategory
from projects.models import MilestoneMaster,TaskMaster,MilestoneTaskMapping,Project,ProjectMilestone,ProjectMilestoneTask,ProjectMembers
from userManagements.models import UserMapping
import base64



#--------------------Convert date in DB format------------------#
def getDatefromDateString(dateString):
	date = None
	if(dateString != ""):
		date = datetime.strptime(dateString, "%d/%m/%Y")
		return date
	elif(dateString == 'undefined'):
		return date
	else:
		return date
#---------------------Convert date in UI format----------------------#
def getDateStringfromDate(dateString):
	if(dateString == "" or dateString == None or dateString == "Null"):
		return ""
	else:
		date = dateString.strftime("%d/%m/%Y")
		return date
# Create your views here.

#-------------------load business unit page----------------------------
@csrf_exempt
def businessUnit(request):
	con = country.objects.filter(id=100)
	state = State.objects.filter(country_id=100)
	data = {"con":con,"state":state}
	return render(request,'admin_templates/business_unit/business-unit.html',{"data":data})
	
#-------------------load currency page----------------------------
@csrf_exempt
def currency(request):
	return render(request,'admin_templates/masters/currency.html')
	
#---------------Business unit Api---------
@api_view(['POST'])
def currencyApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = Currency.objects.get(uId=data["uId"])
				if(details.currency!=data["currency"]):
					if(Currency.objects.filter(currency__iexact = data["currency"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Currency"}),content_type="application/json")
			except:
				if(Currency.objects.filter(currency__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Currency"}),content_type="application/json")
				details = Currency()
			details.name = data["name"]
			details.symbol = data["symbol"]
			details.currency = data["currency"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"currency":details.currency}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = Currency.objects.get(uId=uId)
			obj = {"symbol":p.symbol,"name":p.name,"currency":p.currency}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = Currency.objects.filter(isActive=True).count()
			ListDetails = Currency.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(currency__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"currency":item.currency,"symbol":item.symbol})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = Currency.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")	
	
#------------------get all city by state----------------------

@api_view(['POST'])
def getCityByState(request):
	try:
		data = request.data
		state = data["state"]
		city = []
		cityDetails = City.objects.filter(state_id=state)
		for c in cityDetails:
			city.append({"id":c.uId,"name":c.name})
		return HttpResponse(json.dumps({"status":"Success","responsecode":200,"city":city}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":500,"error":str(e)}),content_type="application/json")
		
#---------------Business unit Api---------
@api_view(['POST'])
def BusinessUnitApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = BusinessUnit.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(BusinessUnit.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Business Unit Name"}),content_type="application/json")
			except:
				if(BusinessUnit.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Business Unit Name"}),content_type="application/json")
				details = BusinessUnit()
			details.name = data["name"]
			details.location = data["location"]
			details.address = data["address"]
			details.country_id = data["country"]
			details.state_id = data["state"]
			details.city_id = data["city"]
			details.pincode = data["pincode"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = BusinessUnit.objects.get(uId=uId)
			cityList = []
			cityDetails = City.objects.filter(state_id=p.state_id)
			for c in cityDetails:
				cityList.append({"id":c.uId,"name":c.name})
			obj = {"cityList":cityList,"name":p.name,"location":p.location,"address":p.address,"country":p.country_id,"state":p.state_id,"city":p.city_id,"pincode":p.pincode}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = BusinessUnit.objects.filter(isActive=True).count()
			ListDetails = BusinessUnit.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"address":item.address,"location":item.location,"state":item.state.name,"city":item.city.name,"pincode":item.pincode})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = BusinessUnit.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
#-------------------load milestone master page----------------		
@csrf_exempt
def milestoneMaster(request):
	tasks = TaskMaster.objects.filter(isActive=True)
	data = {"tasks":tasks}
	# return render(request,'admin_templates/milestone/milestoneMaster.html',{"data":data})
	return HttpResponse('admin_templates/milestone/milestoneMaster.html',{"data":data})
		
#---------------milestone master Api---------
@api_view(['POST'])
def milestoneMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = MilestoneMaster.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(MilestoneMaster.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Milestone Name"}),content_type="application/json")
			except:
				details = MilestoneMaster()
				if(MilestoneMaster.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Milestone Name"}),content_type="application/json")
			details.name = data["name"]
			details.isDefault = False
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = MilestoneMaster.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = MilestoneMaster.objects.filter(isActive=True).count()
			ListDetails = MilestoneMaster.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"isDefault":item.isDefault})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = MilestoneMaster.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		elif(data["action"]=="getTask"):
			taskList = []
			tasks = TaskMaster.objects.filter(isActive=True)
			for t in tasks:
				mm = MilestoneTaskMapping.objects.filter(milestone_id = data["uId"],task_id = t.uId,isActive=True).count()
				if(mm>0):
					selectFlag = True
				else:
					selectFlag = False
				taskList.append({"uId":t.uId,"name":t.name,"selectFlag":selectFlag})
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","taskList":taskList}),content_type="application/json")
		elif(data["action"]=="saveTaskMapping"):
			uId = data["uId"]
			tmArr = json.loads(data["tmArr"])
			tmArr1 = json.loads(data["tmArr1"])
			for t in tmArr:
				try:
					mm = MilestoneTaskMapping.objects.get(milestone_id = uId,task_id = t)
				except:
					mm = MilestoneTaskMapping()
				mm.milestone_id = uId
				mm.task_id = t
				mm.isActive = True
				mm.save()
			mmde = MilestoneTaskMapping.objects.filter(milestone_id = uId,task_id__in = tmArr1)
			mmdec = MilestoneTaskMapping.objects.filter(milestone_id = uId,task_id__in = tmArr1).count()
			list = []
			for m in mmde:
				list.append(m.uId)
				m.isActive = False
				m.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","mmdec":mmdec,"list":list,"tmArr1":tmArr1}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
#----------------- excel export milestone master -----------------			
@api_view(['POST'])
def getAllMilestoneExcel(request):
	try:
		milestones = MilestoneMaster.objects.filter(isActive=True)
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output)
		worksheet = workbook.add_worksheet("Dispatch")
		# bold = workbook.add_format({'bold': True})
		worksheet.set_row(0, 25)
		worksheet.set_column('A:B', 20)
		header_format = workbook.add_format({
		'border': 1,
		'bg_color': '#2c81c4',
		'bold': True,
		'text_wrap': True,
		'valign': 'vcenter',
		'indent': 1,
		'font_color':'white'
		})
		columns = [
			"Sr No",
			"Name",
		]
		row = 0
		srno = 1
		for i,elem in enumerate(columns):
			worksheet.write(row, i, elem, header_format)
		row += 1
		for item in milestones:
			worksheet.write(row, 0, srno)
			worksheet.write(row, 1, item.name)
			row += 1
			srno = srno+1
		workbook.close()
		output.seek(0)
		response = HttpResponse(output.read(), content_type="application/ms-excel")
		response['Content-Disposition'] = 'attachment; filename="MilestoneMaster.xlsx"'
		return response
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")

#------------------------ load task master page------------------
		
@csrf_exempt
def taskMaster(request):
	milestones = MilestoneMaster.objects.filter(isActive=True)
	data = {"milestones":milestones}
	# return render(request,'admin_templates/task/taskMaster.html',{"data":data})
	return HttpResponse('admin_templates/task/taskMaster.html',{"data":data})
		
#---------------Task Master Api---------
@api_view(['POST'])
def taskMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = TaskMaster.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(TaskMaster.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Task Name"}),content_type="application/json")
			except:
				details = TaskMaster()
				if(TaskMaster.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Task Name"}),content_type="application/json")
			details.name = data["name"]
			details.save()
			milestoneList = json.loads(data["milestoneList"])
			mmlist = MilestoneTaskMapping.objects.filter(task_id = details.uId)
			for m in mmlist:
				m.isActive = False
				m.save()
			for t in milestoneList:
				try:
					mm = MilestoneTaskMapping.objects.get(milestone_id = t,task_id = details.uId)
				except:
					mm = MilestoneTaskMapping()
				mm.milestone_id = t
				mm.task_id = details.uId
				mm.isActive = True
				mm.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = TaskMaster.objects.get(uId=uId)
			milestoneList = []
			mmlist = MilestoneTaskMapping.objects.filter(task_id = uId)
			for m in mmlist:
				milestoneList.append(m.milestone_id)
			obj = {"milestoneList":milestoneList,"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = TaskMaster.objects.filter(isActive=True).count()
			ListDetails = TaskMaster.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = TaskMaster.objects.get(uId=uId)
			p.isActive = False
			p.save()
			mmlist = MilestoneTaskMapping.objects.filter(task_id = uId)
			for m in mmlist:
				m.isActive = False
				m.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")

		
#----------------- excel export task master -----------------			

@api_view(['POST'])
def getAllTaskExcel(request):
	try:
		task = TaskMaster.objects.filter(isActive=True)
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output)
		worksheet = workbook.add_worksheet("Dispatch")
		# bold = workbook.add_format({'bold': True})
		worksheet.set_row(0, 25)
		worksheet.set_column('A:B', 20)
		header_format = workbook.add_format({
		'border': 1,
		'bg_color': '#2c81c4',
		'bold': True,
		'text_wrap': True,
		'valign': 'vcenter',
		'indent': 1,
		'font_color':'white'
		})
		columns = [
			"Sr No",
			"Name",
		]
		row = 0
		srno = 1
		for i,elem in enumerate(columns):
			worksheet.write(row, i, elem, header_format)
		row += 1
		for item in task:
			worksheet.write(row, 0, srno)
			worksheet.write(row, 1, item.name)
			row += 1
			srno = srno+1
		workbook.close()
		output.seek(0)
		response = HttpResponse(output.read(), content_type="application/ms-excel")
		response['Content-Disposition'] = 'attachment; filename="TaskMaster.xlsx"'
		return response
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")

#----------------- excel import milestone master -----------------		
@api_view(['POST'])
def importMilestoneMaster(request):
	try:
		if(request.FILES['myfile']):
			request.FILES['myfile'].save_to_database(name_columns_by_row=0,model=MilestoneMasterExcelTemp,mapdict=['name'])
			MilestoneMasterExcelTemp.objects.filter(name__isnull=True,isActive = True).delete()
			re = MilestoneMasterExcelTemp.objects.filter(isActive = True)
			for r in re:
				try:
					obj = MilestoneMaster.objects.get(name=r.name,isActive=True)
				except:
					obj = MilestoneMaster()
				try:
					obj.name = r.name
					obj.isDefault = False
					obj.save()
					r.delete()
				except:
					r.isActive = False
					r.save()
			return HttpResponse(json.dumps({"status" : "Success", "responceCode":"200"}), content_type="application/json")
		else:
			return HttpResponse(json.dumps({"status" : "Fail", "responceCode":"500"}), content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"Status" : "Fail", "responceCode":"500" ,"error":str(e) }), content_type="application/json")
		
	
#----------------- excel import task master -----------------	
@api_view(['POST'])
def importTaskMaster(request):
	try:
		if(request.FILES['myfile']):
			request.FILES['myfile'].save_to_database(name_columns_by_row=0,model=TaskMasterExcelTemp,mapdict=['name'])
			TaskMasterExcelTemp.objects.filter(name__isnull=True,isActive = True).delete()
			re = TaskMasterExcelTemp.objects.filter(isActive = True)
			for r in re:
				try:
					obj = TaskMaster.objects.get(name=r.name,isActive=True)
				except:
					obj = TaskMaster()
				try:
					obj.name = r.name
					obj.save()
					r.delete()
				except:
					r.isActive = False
					r.save()
			return HttpResponse(json.dumps({"status" : "Success", "responceCode":"200"}), content_type="application/json")
		else:
			return HttpResponse(json.dumps({"status" : "Fail", "responceCode":"500"}), content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"Status" : "Fail", "responceCode":"500" ,"error":str(e) }), content_type="application/json")
		
		
#------------------------ load task master page------------------
		
@csrf_exempt
def roleMaster(request):
	# return render(request,'admin_templates/masters/roleMaster.html')
	return HttpResponse('admin_templates/masters/roleMaster.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def roleMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = role.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(role.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Role Name"}),content_type="application/json")
			except:
				details = role()
				if(role.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Role Name"}),content_type="application/json")
			details.name = data["name"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = role.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = role.objects.filter(isActive=True).count()
			ListDetails = role.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = role.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#------------------------ load task master page------------------
		
@csrf_exempt
def buildingMaster(request):
	return render(request,'admin_templates/building/buildingMaster.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def buildingMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = building.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(building.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Building Name"}),content_type="application/json")
			except:
				details = building()
				if(building.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Building Name"}),content_type="application/json")
			details.name = data["name"]
			details.code = data["code"]
			# details.sequence = data["sequence"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = building.objects.get(uId=uId)
			obj = {"name":p.name,"code":p.code,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = building.objects.filter(isActive=True).count()
			ListDetails = building.objects.filter(isActive=True).order_by('name')
			
			if(searchVal != ""):
				ListDetails = ListDetails.filter(Q(name__icontains=searchVal) | Q(code__icontains=searchVal))
				
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"code":item.code})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = building.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#------------------------ load task master page------------------
		
@csrf_exempt
def subbuildingMaster(request):
	b = building.objects.filter(isActive=True)
	data = {"b":b}
	return render(request,'admin_templates/building/subbuilding.html',{"data":data})
		
#---------------Task Master Api---------
@api_view(['POST'])
def subbuildingMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = subbuilding.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(subbuilding.objects.filter(name__iexact = data["name"],building_id = data["building"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Sub-Building Name"}),content_type="application/json")
			except:
				if(subbuilding.objects.filter(name__iexact = data["name"],building_id = data["building"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Sub-Building Name"}),content_type="application/json")
				details = subbuilding()
			details.name = data["name"]
			details.building_id = data["building"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = subbuilding.objects.get(uId=uId)
			obj = {"name":p.name,"building":p.building_id,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = subbuilding.objects.filter(isActive=True).count()
			ListDetails = subbuilding.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"building":item.building.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = subbuilding.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#------------------------ load task master page------------------
		
@csrf_exempt
def uomMaster(request):
	# return render(request,'admin_templates/masters/uom.html')
	return HttpResponse('admin_templates/masters/uom.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def uomMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = UOM.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(UOM.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate UOM Name"}),content_type="application/json")
			except:
				details = UOM()
				if(UOM.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate UOM Name"}),content_type="application/json")
			details.name = data["name"]
			details.description = data["description"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = UOM.objects.get(uId=uId)
			obj = {"name":p.name,"description":p.description,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = UOM.objects.filter(isActive=True).count()
			ListDetails = UOM.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"description":item.description})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = UOM.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#------------------------ load task master page------------------
		
@csrf_exempt
def departmentMaster(request):
	tow = TypeOfWork.objects.filter(isActive=True)
	data = {"tow":tow}
	return render(request,'admin_templates/masters/department.html',{"data":data})
		
#---------------Task Master Api---------
@api_view(['POST'])
def departmentMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = department.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(department.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Department Name"}),content_type="application/json")
			except:
				details = department()
				if(department.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Department Name"}),content_type="application/json")
			details.name = data["name"]
			# details.description = data["description"]
			details.save()
			if(data["action"]=="edit"):
				dm = department_typoofwork_mapping.objects.filter(isActive=True,department_id = details.uId)
				for d in dm:
					d.isActive = False
					d.save()
			for t in json.loads(data["tow"]):
				try:
					dwm = department_typoofwork_mapping.objects.get(department_id = details.uId,workType_id=t)
				except:
					dwm = department_typoofwork_mapping()
				dwm.department_id = details.uId
				dwm.workType_id = t
				dwm.isActive = True
				dwm.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = department.objects.get(uId=uId)
			towm = []
			dm = department_typoofwork_mapping.objects.filter(isActive=True,department_id = p.uId)
			for d in dm:
				towm.append(d.workType_id)
			obj = {"name":p.name,"uId":p.uId,"towm":towm}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = department.objects.filter(isActive=True).count()
			ListDetails = department.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = department.objects.get(uId=uId)
			p.isActive = False
			p.save()
			dm = department_typoofwork_mapping.objects.filter(isActive=True,department_id = p.uId)
			for d in dm:
				d.isActive = False
				d.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
#------------------------ load task master page------------------
		
@csrf_exempt
def utilitiesSystemCategory(request):
	return render(request,'admin_templates/masters/utilitiescategory.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def UtilitiesSystemCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = UtilitiesSystemCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(UtilitiesSystemCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Utilities System Category Name"}),content_type="application/json")
			except:
				if(UtilitiesSystemCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Utilities System Category Name"}),content_type="application/json")
				details = UtilitiesSystemCategory()
			details.name = data["name"]
			# details.description = data["description"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = UtilitiesSystemCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = UtilitiesSystemCategory.objects.filter(isActive=True).count()
			ListDetails = UtilitiesSystemCategory.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = UtilitiesSystemCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")		

#------------------------ load task master page------------------
		
@csrf_exempt
def utilitiesSystem(request):
	category = UtilitiesSystemCategory.objects.filter(isActive=True)
	data = {"category":category}
	return render(request,'admin_templates/masters/utilities.html',{"data":data})
		
#---------------Task Master Api---------
@api_view(['POST'])
def UtilitiesSystemApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = UtilitiesSystem.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(UtilitiesSystem.objects.filter(name__iexact = data["name"],category_id = data["category"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Utilities System Name"}),content_type="application/json")
			except:
				if(UtilitiesSystem.objects.filter(name__iexact = data["name"],category_id = data["category"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Role Name"}),content_type="application/json")
				details = UtilitiesSystem()
			details.name = data["name"]
			details.category_id = data["category"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = UtilitiesSystem.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId,"category":p.category_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = UtilitiesSystem.objects.filter(isActive=True).count()
			ListDetails = UtilitiesSystem.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"category":item.category.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = UtilitiesSystem.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")	
		

#------------------------ load task master page------------------
		
@csrf_exempt
def electricalSystemCategory(request):
	return render(request,'admin_templates/masters/electricalcategory.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def ElectricalSystemCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = ElectricalSystemCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(ElectricalSystemCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Electrical System Category Name"}),content_type="application/json")
			except:
				if(ElectricalSystemCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Electrical System Category Name"}),content_type="application/json")
				details = ElectricalSystemCategory()
			details.name = data["name"]
			# details.description = data["description"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = ElectricalSystemCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = ElectricalSystemCategory.objects.filter(isActive=True).count()
			ListDetails = ElectricalSystemCategory.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = ElectricalSystemCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")		

#------------------------ load task master page------------------
		
@csrf_exempt
def electricalSystem(request):
	category = ElectricalSystemCategory.objects.filter(isActive=True)
	data = {"category":category}
	return render(request,'admin_templates/masters/electrical.html',{"data":data})
		
#---------------Task Master Api---------
@api_view(['POST'])
def ElectricalSystemApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = ElectricalSystem.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(ElectricalSystem.objects.filter(name__iexact = data["name"],category_id = data["category"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Electrical System Name"}),content_type="application/json")
			except:
				if(ElectricalSystem.objects.filter(name__iexact = data["name"],category_id = data["category"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Electrical System Name"}),content_type="application/json")
				details = ElectricalSystem()
			details.name = data["name"]
			details.category_id = data["category"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = ElectricalSystem.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId,"category":p.category_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = ElectricalSystem.objects.filter(isActive=True).count()
			ListDetails = ElectricalSystem.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"category":item.category.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = ElectricalSystem.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	

#------------------------ load task master page------------------
@csrf_exempt
def statutoryCategory(request):
	return render(request,'admin_templates/masters/statutoryCategory.html')
		
#---------------Task Master Api---------
@api_view(['POST'])
def statutoryCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = StatutoryCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(StatutoryCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Statutory Category Name"}),content_type="application/json")
			except:
				if(StatutoryCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Statutory Category Name"}),content_type="application/json")
				details = StatutoryCategory()
			details.name = data["name"]
			# details.description = data["description"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = StatutoryCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = StatutoryCategory.objects.filter(isActive=True).count()
			ListDetails = StatutoryCategory.objects.filter(isActive=True).order_by('-name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = StatutoryCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")	
	
#------------------------ load task master page------------------
@csrf_exempt
def equipmentCategory(request):
	return render(request,'admin_templates/masters/equipmentCategory.html')
	
#---------------Task Master Api---------
@api_view(['POST'])
def equipmentCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = EquipmentCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(EquipmentCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Category Name"}),content_type="application/json")
			except:
				if(EquipmentCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Category Name"}),content_type="application/json")
				details = EquipmentCategory()
			details.name = data["name"]
			# details.description = data["description"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = EquipmentCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = EquipmentCategory.objects.filter(isActive=True).count()
			ListDetails = EquipmentCategory.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = EquipmentCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	
#------------------------ load task master page------------------
@csrf_exempt
def equipmentSubCategory(request):
	category = EquipmentCategory.objects.filter(isActive=True)
	data = {"category":category}
	return render(request,'admin_templates/masters/equipmentSubCategory.html',{"data":data})
	
#---------------Task Master Api---------
@api_view(['POST'])
def equipmentSubCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = EquipmentSubCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(EquipmentSubCategory.objects.filter(name__iexact = data["name"],category_id	= data["category"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Sub Category Name"}),content_type="application/json")
			except:
				if(EquipmentSubCategory.objects.filter(name__iexact = data["name"],category_id	= data["category"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Sub Category Name"}),content_type="application/json")
				details = EquipmentSubCategory()
			details.name = data["name"]
			details.category_id	= data["category"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = EquipmentSubCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId,"category":p.category_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = EquipmentSubCategory.objects.filter(isActive=True).count()
			ListDetails = EquipmentSubCategory.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"category":item.category.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = EquipmentSubCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	
#------------------------ load task master page------------------
@csrf_exempt
def equipmentMaster(request):
	category = EquipmentCategory.objects.filter(isActive=True)
	# buildings = building.objects.filter(isActive=True)
	systemType = SystemType.objects.filter(isActive=True)
	data = {"category":category,"systemType":systemType}
	return render(request,'admin_templates/masters/equipment.html',{"data":data})
	
#---------------Task Master Api---------
@api_view(['POST'])
def equipmentMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = Equipment.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(Equipment.objects.filter(name__iexact = data["name"],category_id	= data["category"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Name"}),content_type="application/json")
			except:
				if(Equipment.objects.filter(name__iexact = data["name"],category_id	= data["category"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Equipment Name"}),content_type="application/json")
				details = Equipment()
			details.name = data["name"]
			details.category_id	= data["category"]
			try:
				if(data["subcategory"]!="" and data["subcategory"]!="Null" and data["subcategory"]!=None):
					details.subcategory_id	= data["subcategory"]
				else:
					pass
			except:
				pass
			# try:
				# if(data["building"]!="" and data["building"]!="Null" and data["building"]!=None):
					# details.building_id	= data["building"]
				# else:
					# pass
			# except:
				# pass
			try:
				if(data["systemType"]!="" and data["systemType"]!="Null" and data["systemType"]!=None):
					details.systemType_id	= data["systemType"]
				else:
					pass
			except:
				pass
			if(data["postToDashboard"]=="true"):
				details.postToDashboard	= True
			else:
				details.postToDashboard = False
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			slist = []
			p = Equipment.objects.get(uId=uId)
			subcategory = EquipmentSubCategory.objects.filter(category_id=p.category_id)
			for s in subcategory:
				slist.append({"uId":s.uId,"name":s.name})
			# try:
				# subcat = p.subcategory_id
			# except:
				# subcat = ""
			obj = {"systemType":p.systemType_id,"postToDashboard":p.postToDashboard,"name":p.name,"uId":p.uId,"category":p.category_id,"subcategory":p.subcategory_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj,"slist":slist}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			systemType = request.POST['systemType']
			totalCount = Equipment.objects.filter(isActive=True).count()
			ListDetails = Equipment.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			if(systemType != ""):
				ListDetails = ListDetails.filter(systemType_id=systemType)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				try:
					subcat = item.subcategory.name
				except:
					subcat = ""
				# try:
					# bui = item.building.name
				# except:
					# bui = ""
				try:
					systemtype = item.systemType.name
				except:
					systemtype = ""
				list.append({"systemtype":systemtype,"postToDashboard":item.postToDashboard,"uId":item.uId,"name":item.name,"category":item.category.name,"subcategory":subcat})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = Equipment.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	
@api_view(['POST'])
def getEquipmentMasterInExcel(request):
	try:
		searchVal = request.POST['search']
		systemType = request.POST['systemType']
		ListDetails = Equipment.objects.filter(isActive=True).order_by('-id')
		if(searchVal != ""):
			ListDetails = ListDetails.filter(name__icontains=searchVal)
		if(systemType != ""):
			ListDetails = ListDetails.filter(systemType_id=systemType)
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output)
		worksheet = workbook.add_worksheet()
		header_format = workbook.add_format({
		'border': 1,
		'bg_color': '#2c81c4',
		'bold': True,
		'text_wrap': True,
		'valign': 'vcenter',
		'indent': 1,
		'font_color':'white'
		})
		worksheet.set_row(0, 35)
		worksheet.set_column('A1:F1', 20)
		worksheet.write('A1', "Sr. No", header_format)
		worksheet.write('B1', "Name", header_format)
		worksheet.write('C1', "System Type", header_format)
		worksheet.write('D1', "Category", header_format)
		worksheet.write('E1', "Sub Category", header_format)
		worksheet.write('F1', "Post To Dashboard", header_format)
		srNo = 1
		col = 2
		for item in ListDetails:
			colstr = str(col)
			try:
				subcat = item.subcategory.name
			except:
				subcat = ""
			try:
				systemtype = item.systemType.name
			except:
				systemtype = ""
			try:
				category = item.category.name
			except:
				category = ""
			if(item.postToDashboard == True):
				postToDashboard = "Y"
			else:
				postToDashboard = "N"
			worksheet.write('A'+colstr,srNo)
			worksheet.write('B'+colstr,item.name)
			worksheet.write('C'+colstr,systemtype)
			worksheet.write('D'+colstr,category)
			worksheet.write('E'+colstr,subcat)
			worksheet.write('F'+colstr,postToDashboard)
			col = col + 1
			srNo = srNo + 1
		workbook.close()
		output.seek(0)
		response = HttpResponse(output.read(),content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="StatutoryApproval.xlsx"'
		return response
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json") 
	
@api_view(['POST'])
def getSubcategory(request):
	try:
		data = request.data
		slist = []
		subcategory = EquipmentSubCategory.objects.filter(category_id=data["category"])
		for s in subcategory:
			slist.append({"uId":s.uId,"name":s.name})
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","slist":slist}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json") 
	
#------------------------ load type of work page------------------
		
@csrf_exempt
def typeOfWork(request):
	return render(request,'admin_templates/masters/typeOfWork.html')
		
#---------------type Of Work Api---------
@api_view(['POST'])
def typeOfWorkApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = TypeOfWork.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(TypeOfWork.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Type Of Work Name"}),content_type="application/json")
			except:
				details = TypeOfWork()
				if(TypeOfWork.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Type Of Work Name"}),content_type="application/json")
			details.name = data["name"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = TypeOfWork.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = TypeOfWork.objects.filter(isActive=True).count()
			ListDetails = TypeOfWork.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = TypeOfWork.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
	
@csrf_exempt
def buildingLayout(request):
	# b = building.objects.filter(isActive=True)
	# data = {"b":b}
	return render(request,'admin_templates/building/buildingLayout.html')
	
@api_view(['POST'])
def getSubbuildingByBuilding(request):
	try:
		data = request.data
		list = []
		listdetails = subbuilding.objects.filter(building_id = data["uId"])
		for s in listdetails:
			list.append({"uId":s.uId,"name":s.name})
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","list":list}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
@api_view(['POST'])
def getUtilitiesByCategory(request):
	try:
		data = request.data
		list = []
		listdetails = UtilitiesSystem.objects.filter(category_id = data["uId"])
		for s in listdetails:
			list.append({"uId":s.uId,"name":s.name})
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","list":list}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#-------------------load holiday master page----------------		
@csrf_exempt
def holidayMaster(request):
	return render(request,'admin_templates/masters/holiday.html')
	
#---------------type Of Work Api---------
@api_view(['POST'])
def holidayApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = Holiday.objects.get(uId=data["uId"])
			except:
				details = Holiday()
			details.name = data["name"]
			details.date = getDatefromDateString(data["date"])
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = Holiday.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId,"date":getDateStringfromDate(p.date)}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = Holiday.objects.filter(isActive=True).count()
			ListDetails = Holiday.objects.filter(isActive=True).order_by('-id')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				try:
					dayname = (item.date).strftime('%A')
				except Exception as e:
					dayname = ""
				list.append({"uId":item.uId,"dayname":dayname,"name":item.name,"date":getDateStringfromDate(item.date)})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = Holiday.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#-------------------load Task Category page----------------		
@csrf_exempt
def taskCategory(request):
	# return render(request,'admin_templates/masters/taskCategory.html')
	return HttpResponse('admin_templates/masters/taskCategory.html')
	
#---------------type Of Work Api---------
@api_view(['POST'])
def taskCategoryApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = TaskCategory.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(TaskCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Task Category Name"}),content_type="application/json")
			except:
				if(TaskCategory.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Task Category Name"}),content_type="application/json")
				details = TaskCategory()
			details.name = data["name"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = TaskCategory.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = TaskCategory.objects.filter(isActive=True).count()
			ListDetails = TaskCategory.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = TaskCategory.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
		
#-------------------load Email Template List Page----------------		
@csrf_exempt
def emailTemplate(request):
	return render(request,'admin_templates/emailTemplate/templateList.html')
	
@csrf_exempt
def addTemplate(request):
	roles = role.objects.filter(isActive = True)
	types = EmailTemplateType.objects.filter(isActive=True)
	data = {"roles":roles,"types":types}
	return render(request,'admin_templates/emailTemplate/addTemplate.html',{"data":data})
	
@csrf_exempt
def editTemplate(request,uId):
	uId1 = base64.b64decode(uId).decode('utf-8')
	roles = role.objects.filter(isActive = True)
	types = EmailTemplateType.objects.filter(isActive=True)
	et = EmailTemplate.objects.get(id=uId1)
	etr = EmailTemplateRoleMapping.objects.filter(isActive=True,template_id=et.id).values_list("role_id",flat=True)
	data = {"roles":roles,"types":types,"template":et,"selectedRoles":etr}
	return render(request,'admin_templates/emailTemplate/editTemplate.html',{"data":data})
	
@api_view(['POST'])
def getFieldsByTemplateType(request):
	try:
		data = request.data
		type = EmailTemplateType.objects.get(uId = data["type"])
		name = type.name
		fields = []
		if(name=="Project Creation" or name=="Project Closed"):
			fields = ["projectName","description","projectType","businessUnit","startDate","endDate","projectManager","projectHead","leadCivil","leadElectrical","leadUtility","projectMembers"]
		elif(name=="Task Creation" or name=="Task Edit" or name=="Task Closed"):
			fields = ["projectName","milestoneName","taskName","plannedStartDate","plannedEndDate","actualStartDate","actualEndDate","completionPercentage","healthCheck"]
		elif(name=="Milestone Closed"):
			fields = ["projectName","milestoneName","description","startDate","endDate"]
		elif(name=="Forgot Password"):
			fields = ["password","firstName","lastName"]
		return HttpResponse(json.dumps({"status":"Success","responsecode":"200","fields":fields}),content_type="application/json") 
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json") 
	
@api_view(['POST'])
def emailTemplateApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				et = EmailTemplate.objects.get(id=data["id"])
				if(et.type_id!=data["type"]):
					if(EmailTemplate.objects.filter(type_id = data["type"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Email Template Type"}),content_type="application/json")
			except:
				if(EmailTemplate.objects.filter(type_id = data["type"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Email Template Type"}),content_type="application/json")
				et = EmailTemplate()
			et.type_id = data["type"]
			et.subject = data["subject"]
			et.body = data["body"]
			et.save()
			if(data["id"]!=""):
				etr = EmailTemplateRoleMapping.objects.filter(isActive=True,template_id=et.id)
				for e in etr:
					e.isActive = False
					e.save()
			roles = json.loads(data["roles"])
			for r in roles:
				try:
					etr = EmailTemplateRoleMapping.objects.get(role_id=r,template_id=et.id)
				except:
					etr = EmailTemplateRoleMapping()
				etr.role_id = r
				etr.template_id = et.id
				etr.isActive = True
				etr.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json") 
		elif(data["action"]=="delete"):
			et = EmailTemplate.objects.get(id=data["id"])
			et.isActive = False
			et.save()
			etr = EmailTemplateRoleMapping.objects.filter(isActive=True,template_id=et.id)
			for e in etr:
				e.isActive = False
				e.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json") 
		elif(data["action"]=="search" or data["action"]=="list"):
			draw = request.POST['draw']
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = EmailTemplate.objects.filter(isActive=True).count()
			ListDetails = EmailTemplate.objects.filter(isActive=True).order_by('-id')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(subject__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			templateList = []
			for p in ListDetails:
				urlSafeEncodedBytes = base64.urlsafe_b64encode(str(p.id).encode("utf-8"))
				urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
				templateList.append({"type":p.type.name,"subject":p.subject,"body":p.body,"id":p.id,"encodedId":urlSafeEncodedStr})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : templateList}), content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json") 

def sendMailFunction(typeName,uId):
	# try:
		templateType = EmailTemplateType.objects.filter(name = typeName).order_by("-id")[0]
		et = EmailTemplate.objects.filter(type_id=templateType.uId).order_by("-id")[0]
		roles = []
		etr = EmailTemplateRoleMapping.objects.filter(isActive=True,template_id=et.id)
		for e in etr:
			roles.append(e.role_id)
		receiver_email = []
		html = et.body
		subject = et.subject
		name = templateType.name
		fields = []
		if(name=="Project Creation" or name=="Project Closed"):
			project = Project.objects.get(uId = uId)
			try:
				userMap = UserMapping.objects.get(user_id=project.projectManager_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
				html = html.replace("$$projectManager", str(project.projectManager.first_name)+" "+str(project.projectManager.last_name))
				subject = subject.replace("$$projectManager", str(project.projectManager.first_name)+" "+str(project.projectManager.last_name))
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=project.projectHead_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
				html = html.replace("$$projectHead", str(project.projectHead.first_name)+" "+str(project.projectHead.last_name))
				subject = subject.replace("$$projectHead", str(project.projectHead.first_name)+" "+str(project.projectHead.last_name))
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=project.leadCivil_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
				html = html.replace("$$leadCivil", str(project.leadCivil.first_name)+" "+str(project.leadCivil.last_name))
				subject = subject.replace("$$leadCivil", str(project.leadCivil.first_name)+" "+str(project.leadCivil.last_name))
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=project.leadElectrical_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
				html = html.replace("$$leadElectrical", str(project.leadElectrical.first_name)+" "+str(project.leadElectrical.last_name))
				subject = subject.replace("$$leadElectrical", str(project.leadElectrical.first_name)+" "+str(project.leadElectrical.last_name))
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=project.leadUtility_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
				html = html.replace("$$leadUtility", str(project.leadUtility.first_name)+" "+str(project.leadUtility.last_name))
				subject = subject.replace("$$leadUtility", str(project.leadUtility.first_name)+" "+str(project.leadUtility.last_name))
			except Exception as e:
				pass
			projectMembers = ProjectMembers.objects.filter(project_id = project.uId)
			projectMembersNames = ""
			for pm in projectMembers:
				try:
					userMap = UserMapping.objects.get(user_id=pm.user_id)
					if(userMap.role_id in roles):
						receiver_email.append(userMap.user.username)
				except Exception as e:
					pass
				projectMembersNames = projectMembersNames + str(pm.user.first_name)+" "+str(pm.user.last_name)+", "
			try:
				projectMembersNames = projectMembersNames[:-2]
				html = html.replace("$$projectMembers", projectMembersNames)
				subject = subject.replace("$$projectMembers", projectMembersNames)
			except Exception as e:
				pass
			try:
				userMaps = UserMapping.objects.filter(role__name__in=["Project Chief","System Admin"])
				for userMap in userMaps:
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				html = html.replace("$$projectName", str(project.name))
				subject = subject.replace("$$projectName", str(project.name))
			except Exception as e:
				pass
			try:
				html = html.replace("$$description", str(project.description))
				subject = subject.replace("$$description", str(project.description))
			except Exception as e:
				pass
			try:
				html = html.replace("$$projectType", str(project.projectType.name))
				subject = subject.replace("$$projectType", str(project.projectType.name))
			except Exception as e:
				pass
			try:
				html = html.replace("$$businessUnit", str(project.businessUnit.name))
				subject = subject.replace("$$businessUnit", str(project.businessUnit.name))
			except Exception as e:
				pass
			try:
				html = html.replace("$$startDate", getDateStringfromDate(project.startDate))
				subject = subject.replace("$$startDate", getDateStringfromDate(project.startDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$endDate", getDateStringfromDate(project.endDate))
				subject = subject.replace("$$endDate", getDateStringfromDate(project.endDate))
			except Exception as e:
				pass
		elif(name=="Task Creation" or name=="Task Edit" or name=="Task Closed"):
			task = ProjectMilestoneTask.objects.get(uId = uId)
			try:
				userMap = UserMapping.objects.get(user_id=task.project.projectManager_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			# try:
			# 	userMap = UserMapping.objects.get(user_id=task.project.projectHead_id)
			# 	if(userMap.role_id in roles):
			# 		receiver_email.append(userMap.user.username)
			# except Exception as e:
			# 	pass
			try:
				userMap = UserMapping.objects.get(user_id=task.project.leadCivil_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=task.project.leadElectrical_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=task.project.leadUtility_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			projectMembers = ProjectMembers.objects.filter(project_id = task.project.uId)
			for pm in projectMembers:
				try:
					userMap = UserMapping.objects.get(user_id=pm.user_id)
					# if(userMap.role_id in roles):
					# 	receiver_email.append(userMap.user.username)
					if(department_typoofwork_mapping.objects.filter(department_id=userMap.department_id,workType_id=task.workType_id,isActive=True).count()>0):
						receiver_email.append(userMap.user.username)
				except Exception as e:
					pass
			try:
				html = html.replace("$$taskName", str(task.taskname))
				subject = subject.replace("$$taskName", str(task.taskname))
			except Exception as e:
				pass
			try:
				html = html.replace("$$projectName", str(task.project.name))
				subject = subject.replace("$$projectName", str(task.project.name))
			except Exception as e:
				pass
			try:
				html = html.replace("$$milestoneName", str(task.milestone.milestoneName))
				subject = subject.replace("$$milestoneName", str(task.milestone.milestoneName))
			except Exception as e:
				pass
			try:
				html = html.replace("$$plannedStartDate", getDateStringfromDate(task.plannedStartDate))
				subject = subject.replace("$$plannedStartDate", getDateStringfromDate(task.plannedStartDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$plannedEndDate", getDateStringfromDate(task.plannedEndDate))
				subject = subject.replace("$$plannedEndDate", getDateStringfromDate(task.plannedEndDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$actualStartDate", getDateStringfromDate(task.actualStartDate))
				subject = subject.replace("$$actualStartDate", getDateStringfromDate(task.actualStartDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$actualEndDate", getDateStringfromDate(task.actualEndDate))
				subject = subject.replace("$$actualEndDate", getDateStringfromDate(task.actualEndDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$completionPercentage", str(task.completionPercentage))
				subject = subject.replace("$$completionPercentage", str(task.completionPercentage))
			except Exception as e:
				pass
			try:
				html = html.replace("$$healthCheck", str(task.healthCheck))
				subject = subject.replace("$$healthCheck", str(task.healthCheck))
			except Exception as e:
				pass
		elif(name=="Milestone Closed"):
			milestone = ProjectMilestone.objects.get(uId = uId)
			try:
				userMap = UserMapping.objects.get(user_id=milestone.project.projectManager_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=milestone.project.projectHead_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=milestone.project.leadCivil_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=milestone.project.leadElectrical_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			try:
				userMap = UserMapping.objects.get(user_id=milestone.project.leadUtility_id)
				if(userMap.role_id in roles):
					receiver_email.append(userMap.user.username)
			except Exception as e:
				pass
			projectMembers = ProjectMembers.objects.filter(project_id = milestone.project.uId)
			for pm in projectMembers:
				try:
					userMap = UserMapping.objects.get(user_id=pm.user_id)
					if(userMap.role_id in roles):
						receiver_email.append(userMap.user.username)
				except Exception as e:
					pass
			try:
				html = html.replace("$$projectName", str(milestone.project.name))
				subject = subject.replace("$$projectName", str(milestone.project.name))
			except Exception as e:
				pass
			try:
				html = html.replace("$$milestoneName", str(milestone.milestoneName))
				subject = subject.replace("$$milestoneName", str(milestone.milestoneName))
			except Exception as e:
				pass
			try:
				html = html.replace("$$description", str(milestone.description))
				subject = subject.replace("$$description", str(milestone.description))
			except Exception as e:
				pass
			try:
				html = html.replace("$$startDate", getDateStringfromDate(milestone.startDate))
				subject = subject.replace("$$startDate", getDateStringfromDate(milestone.startDate))
			except Exception as e:
				pass
			try:
				html = html.replace("$$endDate", getDateStringfromDate(milestone.endDate))
				subject = subject.replace("$$endDate", getDateStringfromDate(milestone.endDate))
			except Exception as e:
				pass
		if(len(receiver_email)>0):
			sender_email = "technicioustech@gmail.com"
			sender_password = "Technicious-kop@0406"
			smtpHost = "smtp.gmail.com"
			smtpPort = "587"
			msg = MIMEMultipart('alternative')
			msg['From'] = 'Apollo <'+sender_email+'>'
			msg['To'] =  ','.join(receiver_email)
			msg['Subject'] = subject
			part = MIMEText(html, 'html')
			msg.attach(part)
			smtpconf = smtpHost+":"+smtpPort
			mailServer = smtplib.SMTP(smtpconf)
			mailServer.starttls()
			mailServer.login(sender_email,sender_password)
			mailServer.sendmail(sender_email,receiver_email, msg.as_string())
			mailServer.close()
			return "success"
		else:
			return "No recipient"
	# except Exception as e:
	# 	return str(e)

def sendMailFunctionPassword(typeName,userId,password):
	# try:
		templateType = EmailTemplateType.objects.filter(name = typeName).order_by("-id")[0]
		et = EmailTemplate.objects.filter(type_id=templateType.uId).order_by("-id")[0]
		receiver_email = []
		html = et.body
		subject = et.subject
		fields = []
		user = User.objects.get(id=userId)
		receiver_email.append(user.username)
		try:
			userMaps = UserMapping.objects.filter(role__name__in=["System Admin"])
			for userMap in userMaps:
				receiver_email.append(userMap.user.username)
		except Exception as e:
			pass
		try:
			html = html.replace("$$password", str(password))
			subject = subject.replace("$$password", str(password))
		except Exception as e:
			pass
		try:
			html = html.replace("$$firstName", str(user.first_name))
			subject = subject.replace("$$firstName", str(user.first_name))
		except Exception as e:
			pass
		try:
			html = html.replace("$$lastName", str(user.last_name))
			subject = subject.replace("$$lastName", str(user.last_name))
		except Exception as e:
			pass
		if(len(receiver_email)>0):
			sender_email = "technicioustech@gmail.com"
			sender_password = "Technicious-kop@0406"
			smtpHost = "smtp.gmail.com"
			smtpPort = "587"
			msg = MIMEMultipart('alternative')
			msg['From'] = 'Apollo <'+sender_email+'>'
			msg['To'] =  ','.join(receiver_email)
			msg['Subject'] = subject
			part = MIMEText(html, 'html')
			msg.attach(part)
			smtpconf = smtpHost+":"+smtpPort
			mailServer = smtplib.SMTP(smtpconf)
			mailServer.starttls()
			mailServer.login(sender_email,sender_password)
			mailServer.sendmail(sender_email,receiver_email, msg.as_string())
			mailServer.close()
			return "success"
		else:
			return "No recipient"
	# except Exception as e:
	# 	return str(e)

@api_view(['GET'])
def testApi(request):
	res = sendMailFunction("Milestone Closed","PMId-240")
	return HttpResponse(json.dumps({"res": res}), content_type="application/json")
	
	
#-------------------load milestone master page----------------		
@csrf_exempt
def shipmentLocation(request):
	return render(request,'admin_templates/masters/shipmentLocation.html')
		
#---------------ShipmentLocationApi Api---------
@api_view(['POST'])
def ShipmentLocationApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = ShipmentLocation.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(ShipmentLocation.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Shipment Location Name"}),content_type="application/json")
			except:
				if(ShipmentLocation.objects.filter(name__iexact = data["name"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate Shipment Location Name"}),content_type="application/json")
				details = ShipmentLocation()
			details.name = data["name"]
			details.duration = data["duration"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = ShipmentLocation.objects.get(uId=uId)
			obj = {"name":p.name,"duration":p.duration,"uId":p.uId}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = ShipmentLocation.objects.filter(isActive=True).count()
			ListDetails = ShipmentLocation.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"duration":item.duration})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = ShipmentLocation.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")
		
#-------------------load budget system page----------------------------
@csrf_exempt
def budgetSystemMaster(request):
	dept = department.objects.filter(isActive=True)
	data = {"dept":dept}
	return render(request,'admin_templates/masters/budgetSystemMaster.html',{"data":data})
	
#---------------budget System Master Api---------
@api_view(['POST'])
def budgetSystemMasterApi(request):
	try:
		data = request.data
		if(data["action"]=="create" or data["action"]=="edit"):
			try:
				details = BudgetSystem.objects.get(uId=data["uId"])
				if(details.name!=data["name"]):
					if(BudgetSystem.objects.filter(name__iexact = data["name"],dept_id = data["dept"],isActive=True).count()>0):
						return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate System name"}),content_type="application/json")
			except:
				if(BudgetSystem.objects.filter(name__iexact = data["name"],dept_id = data["dept"],isActive=True).count()>0):
					return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":"Duplicate System name"}),content_type="application/json")
				details = BudgetSystem()
			details.name = data["name"]
			details.dept_id = data["dept"]
			details.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","uId":details.uId,"name":details.name}),content_type="application/json")
		elif(data["action"]=="viewById"):
			uId = data["uId"]
			p = BudgetSystem.objects.get(uId=uId)
			obj = {"name":p.name,"uId":p.uId,"dept":p.dept_id}
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","obj":obj}),content_type="application/json")
		elif(data["action"]=="list"):
			draw = request.POST['draw']
			list = []
			start = int(request.POST['start'])
			length = int(request.POST['length'])
			end = start + length
			searchVal = request.POST['search[value]']
			totalCount = BudgetSystem.objects.filter(isActive=True).count()
			ListDetails = BudgetSystem.objects.filter(isActive=True).order_by('name')
			if(searchVal != ""):
				ListDetails = ListDetails.filter(name__icontains=searchVal)
			count = len(ListDetails)
			ListDetails = ListDetails[start:end]
			for item in ListDetails:
				list.append({"uId":item.uId,"name":item.name,"dept":item.dept.name})
			return HttpResponse(json.dumps({"sEcho": draw, "iTotalDisplayRecords": count, "iTotalRecords": totalCount,"aaData" : list}), content_type="application/json")
		elif(data["action"]=="delete"):
			uId = data["uId"]
			p = BudgetSystem.objects.get(uId=uId)
			p.isActive = False
			p.save()
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200"}),content_type="application/json")
		elif(data["action"]=="getSystemByDepartment"):
			dept = data["dept"]
			systemList = []
			ListDetails = BudgetSystem.objects.filter(isActive=True,dept_id=dept).order_by('name')
			for l in ListDetails:
				systemList.append({"uId":l.uId,"name":l.name})
			return HttpResponse(json.dumps({"status":"Success","responsecode":"200","systemList":systemList}),content_type="application/json") 
	except Exception as e:
		return HttpResponse(json.dumps({"status":"Fail","responsecode":"500","error":str(e)}),content_type="application/json")