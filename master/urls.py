from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings
from master.views import getCityByState,milestoneMaster,milestoneMasterApi,taskMaster,taskMasterApi,roleMaster,roleMasterApi,uomMasterApi,uomMaster,taskCategory,taskCategoryApi
urlpatterns = [
 url(r'^getCityByState/$',getCityByState, name= 'getCityByState'),
 url(r'^milestoneMaster/$',milestoneMaster, name= 'milestoneMaster'),
 url(r'^milestoneMasterApi/$',milestoneMasterApi, name= 'milestoneMasterApi'),
 url(r'^taskMaster/$',taskMaster, name= 'taskMaster'),
 url(r'^taskMasterApi/$',taskMasterApi, name= 'taskMasterApi'),
 url(r'^roleMaster/$',roleMaster, name= 'roleMaster'),
 url(r'^roleMasterApi/$',roleMasterApi, name= 'roleMasterApi'),
 url(r'^uomMaster/$',uomMaster, name= 'uomMaster'),
 url(r'^uomMasterApi/$',uomMasterApi, name= 'uomMasterApi'),
 url(r'^taskCategory/$',taskCategory, name= 'taskCategory'),
 url(r'^taskCategoryApi/$',taskCategoryApi, name= 'taskCategoryApi'),
]