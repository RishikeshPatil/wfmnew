from django.db import models
# from django.contrib.auth.models import User,Group

class TeamFunctionUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getTeamFunctionUUID():
	try:
		uuid = TeamFunctionUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = TeamFunctionUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'TFId-'+ str(uuid.uuidNumber)
	return getUUIDId

class TeamFunction(models.Model):
	uId = models.CharField(default=getTeamFunctionUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name

class ProjectManagementModeUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getProjectManagementModeUUID():
	try:
		uuid = ProjectManagementModeUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = ProjectManagementModeUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PMMId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectManagementMode(models.Model):
	uId = models.CharField(default=getProjectManagementModeUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name

class PrivacyUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getPrivacyUUID():
	try:
		uuid = PrivacyUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = PrivacyUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PRIId-'+ str(uuid.uuidNumber)
	return getUUIDId

class Privacy(models.Model):
	uId = models.IntegerField()
	name =models.TextField(null=True, blank=True)
	def __str__(self):
		return self.name

class PriorityUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getPriorityUUID():
	try:
		uuid = PriorityUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = PriorityUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PRIOId-'+ str(uuid.uuidNumber)
	return getUUIDId

class Priority(models.Model):
	uId = models.IntegerField()
	name =models.TextField(null=True, blank=True)
	def __str__(self):
		return self.name
class TypeOfTaskUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getTypeOfTaskUUID():
	try:
		uuid = TypeOfTaskUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = TypeOfTaskUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'WId-'+ str(uuid.uuidNumber)
	return getUUIDId

class TypeOfTask(models.Model):
	uId = models.CharField(default=getTypeOfTaskUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name

#-------------------------------------Employee Master-----------------------------	
class EmployeeUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getEmployeeUUID():
	try:
		uuid = EmployeeUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = EmployeeUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getawId = 'EmpId'+ str(uuid.uuidNumber)
	return getawId
	
	
def getPhotoPath(self, filename):
	directory =settings.MEDIA_ROOT+"/"+"employee/"+self.uId
	if not os.path.exists(directory):
		os.makedirs(directory)
	full_path = str(directory)+"/%s" %(filename)
	return full_path
	
class Employee(models.Model):
	uId = models.TextField(default=getEmployeeUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	address = models.TextField(null=True, blank=True)
	pan = models.TextField(null=True, blank=True)
	dob = models.DateField(null=True, blank=True)
	dol = models.DateField(null=True, blank=True)
	doj = models.DateField(null=True, blank=True)
	reporting = models.TextField(null=True, blank=True)
	mobile = models.TextField(null=True, blank=True)
	email = models.TextField(null=True, blank=True)
	password = models.TextField(null=True, blank=True)
	photo = models.FileField(upload_to=getPhotoPath,null=True,blank=True,max_length=500)
	isActive = models.BooleanField(default=True, blank=True)
	isSuperAdmin = models.BooleanField(default=False, blank=True)
	def __str__(self):
		return self.name
		
#-------------------------------------Employee Master-----------------------------	
class EmployeeMapUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getEmployeeMapUUID():
	try:
		uuid = EmployeeMapUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = EmployeeMapUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getawId = 'EUMId'+ str(uuid.uuidNumber)
	return getawId

#--------------------------Country master ---------------------------------------#
class CountryUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getCountryUUID():
	try:
		uuid = CountryUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = CountryUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getCountryListUUIDId = 'Country'+ str(uuid.uuidNumber)
	return getCountryListUUIDId

class country(models.Model):
	uId = models.TextField(default=getCountryUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	def __str__(self):
		return self.name
		
#---------------------------States Master------------------------------------#
class State(models.Model):
	uId = models.TextField(max_length=200,unique=True)
	name = models.TextField(null=True,max_length=200)
	country = models.ForeignKey(country,on_delete=models.SET_NULL,blank = True, null = True)
	isActive = models.BooleanField(default=True)
	def __str__(self):
		return self.name

#---------------------------City Master -------------------------------#
 
class City(models.Model):
	uId = models.TextField(max_length=200,unique=True)
	name = models.TextField(null = True,max_length=200)
	state = models.ForeignKey(State,on_delete=models.SET_NULL, blank = True, null = True)
	isActive = models.BooleanField(default=True)
	def __str__(self):
		return self.name

#-------------------Role Master-------------------------#

class PartyUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getPartyUUID():
	try:
		uuid = PartyUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = PartyUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getparId = 'PARID'+ str(uuid.uuidNumber)
	return getparId
	
class Party(models.Model):
	uId = models.TextField(default=getPartyUUID, max_length=100, unique=True)
	# group = models.ForeignKey(Group,on_delete=models.SET_NULL,to_field="uId", blank = True, null = True)
	name = models.TextField(null=True, blank=True)
	address = models.TextField(null=True, blank=True)
	city = models.ForeignKey(City, on_delete=models.SET_NULL,blank = True, null = True)
	cityname = models.TextField(null=True, blank=True)
	state = models.ForeignKey(State, on_delete=models.SET_NULL,blank = True, null = True)
	country = models.ForeignKey(country, on_delete=models.SET_NULL,blank = True, null = True)
	pincode = models.TextField(null=True, blank=True)
	email = models.TextField(null=True, blank=True)
	alternateEmail = models.TextField(null=True, blank=True)
	mobileNo = models.TextField(null=True, blank=True)
	alternateMobileNo = models.TextField(null=True, blank=True)
	pan = models.TextField(null=True, blank=True)
	customerId = models.TextField(null=True, blank=True)
	businessName = models.TextField(null=True, blank=True)
	dob = models.DateField(null=True, blank=True)
	telephoneNo = models.TextField(null=True, blank=True)
	note = models.TextField(null=True, blank=True)
	tbcode = models.TextField(null=True, blank=True)
	gstcode = models.TextField(null=True, blank=True)
	partyType = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True, blank=True)
	# client = models.ForeignKey(Client,on_delete=models.SET_NULL,to_field="uId",blank = True, null = True)
	def __str__(self):
		return self.name


#-------------------Role Master-------------------------

class roleUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getRoleUUID():
	try:
		uuid = roleUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = roleUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'RoleId-'+ str(uuid.uuidNumber)
	return getUUIDId

class role(models.Model):
	uId = models.CharField(default=getRoleUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
#-------------------UOM Master-------------------------

class uomUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getUomUUID():
	try:
		uuid = uomUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = uomUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'UOMId-'+ str(uuid.uuidNumber)
	return getUUIDId

class UOM(models.Model):
	uId = models.CharField(default=getUomUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
		
		
#-------------------ProjectType Master-------------------------

class ProjectTypeUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getProjectTypeUUID():
	try:
		uuid = ProjectTypeUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = ProjectTypeUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PTId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectType(models.Model):
	uId = models.CharField(default=getProjectTypeUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
		
		
#-------------------ProjectStatus Master-------------------------

class ProjectStatusUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getProjectStatusUUID():
	try:
		uuid = ProjectStatusUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = ProjectStatusUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'PSId-'+ str(uuid.uuidNumber)
	return getUUIDId

class ProjectStatus(models.Model):
	uId = models.CharField(default=getProjectStatusUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name

#-------------------TaskStatus Master-------------------------

class TaskStatusUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getTaskStatusUUID():
	try:
		uuid = TaskStatusUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = TaskStatusUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'TSId-'+ str(uuid.uuidNumber)
	return getUUIDId

class TaskStatus(models.Model):
	uId = models.CharField(default=getTaskStatusUUID, max_length=100, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name
	
class TaskCategoryUUID(models.Model):
	uuidNumber = models.BigIntegerField(default=0)
	
def getTaskCategoryUUID():
	try:
		uuid = TaskCategoryUUID.objects.latest('uuidNumber')
	except Exception as e:
		uuid = TaskCategoryUUID()
		uuid.save()
	uuid.uuidNumber = uuid.uuidNumber+1
	uuid.save()
	getUUIDId = 'TCId-'+ str(uuid.uuidNumber)
	return getUUIDId
	
class TaskCategory(models.Model):
	uId = models.CharField(default=getTaskCategoryUUID, max_length=10, unique=True)
	name = models.TextField(null=True, blank=True)
	isActive = models.BooleanField(default=True,null=True,blank=True)
	def __str__(self):
		return self.name