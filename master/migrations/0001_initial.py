# Generated by Django 2.2.11 on 2020-04-10 10:20

from django.db import migrations, models
import django.db.models.deletion
import master.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.TextField(max_length=200, unique=True)),
                ('name', models.TextField(max_length=200, null=True)),
                ('isActive', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.TextField(default=master.models.getCountryUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CountryUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.TextField(default=master.models.getEmployeeUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('address', models.TextField(blank=True, null=True)),
                ('pan', models.TextField(blank=True, null=True)),
                ('dob', models.DateField(blank=True, null=True)),
                ('dol', models.DateField(blank=True, null=True)),
                ('doj', models.DateField(blank=True, null=True)),
                ('reporting', models.TextField(blank=True, null=True)),
                ('mobile', models.TextField(blank=True, null=True)),
                ('email', models.TextField(blank=True, null=True)),
                ('password', models.TextField(blank=True, null=True)),
                ('photo', models.FileField(blank=True, max_length=500, null=True, upload_to=master.models.getPhotoPath)),
                ('isActive', models.BooleanField(blank=True, default=True)),
                ('isSuperAdmin', models.BooleanField(blank=True, default=False)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeMapUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='PartyUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Privacy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectManagementMode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getProjectManagementModeUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectManagementModeUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getProjectStatusUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectStatusUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getProjectTypeUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectTypeUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getRoleUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='roleUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TaskCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getTaskCategoryUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskCategoryUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TaskStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getTaskStatusUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskStatusUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TeamFunction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getTeamFunctionUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TeamFunctionUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TypeOfTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getTypeOfTaskUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TypeOfTaskUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='UOM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.CharField(default=master.models.getUomUUID, max_length=10, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='uomUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.TextField(max_length=200, unique=True)),
                ('name', models.TextField(max_length=200, null=True)),
                ('isActive', models.BooleanField(default=True)),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='master.country')),
            ],
        ),
        migrations.CreateModel(
            name='Party',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uId', models.TextField(default=master.models.getPartyUUID, max_length=100, unique=True)),
                ('name', models.TextField(blank=True, null=True)),
                ('address', models.TextField(blank=True, null=True)),
                ('cityname', models.TextField(blank=True, null=True)),
                ('pincode', models.TextField(blank=True, null=True)),
                ('email', models.TextField(blank=True, null=True)),
                ('alternateEmail', models.TextField(blank=True, null=True)),
                ('mobileNo', models.TextField(blank=True, null=True)),
                ('alternateMobileNo', models.TextField(blank=True, null=True)),
                ('pan', models.TextField(blank=True, null=True)),
                ('customerId', models.TextField(blank=True, null=True)),
                ('businessName', models.TextField(blank=True, null=True)),
                ('dob', models.DateField(blank=True, null=True)),
                ('telephoneNo', models.TextField(blank=True, null=True)),
                ('note', models.TextField(blank=True, null=True)),
                ('tbcode', models.TextField(blank=True, null=True)),
                ('gstcode', models.TextField(blank=True, null=True)),
                ('partyType', models.TextField(blank=True, null=True)),
                ('isActive', models.BooleanField(blank=True, default=True)),
                ('city', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='master.City')),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='master.country')),
                ('state', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='master.State')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='master.State'),
        ),
    ]
