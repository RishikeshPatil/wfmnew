# Generated by Django 2.2.11 on 2020-04-10 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0002_priority'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriorityUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='PrivacyUUID',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuidNumber', models.BigIntegerField(default=0)),
            ],
        ),
        migrations.AddField(
            model_name='priority',
            name='uId',
            field=models.CharField(default=0, max_length=10),
        ),
        migrations.AddField(
            model_name='privacy',
            name='uId',
            field=models.CharField(default=0, max_length=10),
        ),
    ]
