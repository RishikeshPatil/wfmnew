from django.contrib import admin
from master.models import Employee,role,UOM,country,ProjectManagementMode,TeamFunction,State,City,ProjectType,ProjectStatus,TypeOfTask,Privacy,Priority,TaskStatus
# Register your models here.
admin.site.register(role)
admin.site.register(UOM)
admin.site.register(country)
admin.site.register(State)
admin.site.register(City)
admin.site.register(ProjectType)
admin.site.register(ProjectStatus)
admin.site.register(TypeOfTask)
admin.site.register(Privacy)
admin.site.register(Priority)
admin.site.register(TeamFunction)
admin.site.register(ProjectManagementMode)
admin.site.register(Employee)
admin.site.register(TaskStatus)
